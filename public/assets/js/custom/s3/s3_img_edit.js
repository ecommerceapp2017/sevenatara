$(document).ready(function() {
    var userId = Cookies.get('userId');
    var appUrl = Cookies.get('appUrl');
    var btnUpload = jQuery('#edit_we_pic'); //button
    var status = jQuery('#edit_we_status'); //status
    var _token = $("input[name=_token]").val();
    new AjaxUpload(btnUpload, {
        action: 'userImage',
        name: 'uploadfile',
        '1': '1',
        onSubmit: function(file, ext) {
            if (!(ext && /^(jpg|jpeg|gif|png)$/.test(ext))) {
                // extension is not allowed 
                status.text('Only JPG or GIF or PNG files are allowed');
                return false;
            }
            status.text('Uploading...');
        },
        onComplete: function(file, response) {
            console.log(response)
            status.text('');
            var resp = response.split('"');
            console.log(resp)
            if (resp[5] == 1) {
                var img = resp[9];
                $(".edit_weImg").attr('src', img);
                $("#edit_weImgTxt").val(img);
            } else {
                alert(resp[9]);

            }
        }
    });
});