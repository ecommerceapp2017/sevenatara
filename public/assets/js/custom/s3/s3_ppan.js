$(document).ready(function() {


    var userId = Cookies.get('userId');
    var appUrl = Cookies.get('appUrl');
    var btnUpload = jQuery('#ppan_pic');
    var status = jQuery('#ppan_status');
    var _token = $("input[name=_token]").val();
    new AjaxUpload(btnUpload, {
        action: 'uploadPan',
        name: 'uploadfile',
        '1': '1',
        onSubmit: function(file, ext) {
            if (!(ext && /^(jpg|jpeg|gif|png)$/.test(ext))) {
                // extension is not allowed 
                status.text('Only JPG or GIF or PNG files are allowed');
                return false;
            }
            status.text('Uploading...');
        },
        onComplete: function(file, response) {
            //On completion clear the status
            console.log(response)
            status.text('');
            //split the response
            var resp = response.split('"');
            console.log(resp)
                //Add uploaded file to list
            if (resp[5] == 1) {
                var img = resp[9];
                $("#ppanUrl").val(img)
                uploadImage(img, userId)

                $(".ppanImg").attr('src', img);
            } else {
                //jQuery('#imgErr').text(resp[9]);
                //alert('Fail', resp[9]);
                alert(resp[9]);

            }
        }
    });


    function uploadImage(img, userId) {
        $.post(appUrl + "uploadPPanImg", { _token: _token, id: userId, p_pan_image: img })
            .done(function(data) {
                if (data.success == 1) {
                    //alert('Success', data.message);
                } else {
                    alert('Fail', data.message);
                }
            });
    }

    // function alert(title, message) {
    //     if (title == 'Success') {
    //         alert(message)
    //     } else {
    //         alert(message)
    //     }
    // }
});