<?php

if (isset($_POST) && !empty($_FILES) && !empty($_FILES['uploadfile']['name'])) {
// print_r($_POST);
// print_r($_FILES);
//upload file formats
    $valid_file_formats = array("jpg", "png", "gif", "bmp", "jpeg", "PNG", "JPG", "JPEG", "GIF", "BMP");

    function get_file_extension($file_name) {
        return substr(strrchr($file_name, '.'), 1);
    }

    $filename = $_FILES['uploadfile']['name'];
    $tmp_filename = $_FILES['uploadfile']['tmp_name'];
    $filetype = strtolower($_FILES['uploadfile']['type']);
//get file extenstion to verify the format
    $extension = get_file_extension($filename);
    $randomStr = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);
    if (in_array($extension, $valid_file_formats)) {
        //include config.php
        include_once "config.php";
        $header = array('Content-Type' => $filetype);
        $new_file_name = "juice/" . time() . $randomStr . "." . $extension;
        if ($s3->putObject(S3::inputFile($tmp_filename), $bucket_name, $new_file_name, S3::ACL_PUBLIC_READ, array(), $header)) {
            $img = "http://" . $bucket_name . ".s3.amazonaws.com/" . $new_file_name;

            $arr = array("success" => 1, "image_url" => $img);
            $v = json_encode($arr, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
            echo $v;
            exit;
        } else {
            print_r($arr);
            $arr = array('success' => 0, 'message' => "Upload Failed");
            echo JSON_UNESCAPED_SLASHES($arr, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
        }
    } else {
        $arr = array('success' => 0, 'message' => "Not a Valid Format");
        echo json_encode($arr, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
    }
} else {
    $arr = array('success' => 0, 'message' => "Please Select a File..");
    echo json_encode($arr, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
}
?>