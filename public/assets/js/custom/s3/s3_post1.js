$(document).ready(function() {


    var userId = Cookies.get('userId');
    var appUrl = Cookies.get('appUrl');
    var btnUpload = jQuery('.cat_pic_1');
    var status = jQuery('#u_p_p_s_1');
    var _token = $("input[name=_token]").val();
    new AjaxUpload(btnUpload, {
        action: appUrl + 'productImage',
        name: 'uploadfile',
        '1': '1',
        onSubmit: function(file, ext) {
            if (!(ext && /^(jpg|jpeg|gif|png)$/.test(ext))) {
                // extension is not allowed 
                status.text('Only JPG or GIF or PNG files are allowed');
                return false;
            }
            status.text('Uploading...');
        },
        onComplete: function(file, response) {
            //On completion clear the status
            console.log(response)
            status.text('');
            //split the response
            var resp = response.split('"');
            console.log(resp)
                //Add uploaded file to list
            if (resp[5] == 1) {
                var img = resp[9];

                $(".u_p_p_i_1").attr('src', img);
                $("#u_p_p_im_1").val(img);
            } else {
                //jQuery('#imgErr').text(resp[9]);
                alert(resp[9]);

            }
        }
    });



    // function alert(title, message) {
    //     if (title == 'Success') {
    //         alert(message)
    //     } else {
    //         alert(message)
    //     }
    // }
});