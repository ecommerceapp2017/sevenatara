$(document).ready(function () {

//$('#mobile').mask('(+1) 999-999-9999');
//$('#phone').mask('(+1) 99999999999');
    $('#price').mask('99.99');
    $('.ui-dialog-titlebar-close').html('<sup>x</sup>');

    // var url = window.location.href;     // Returns full URL
    // if(url.indexOf("localhost:8080") > -1) 
    // {
    //   var mode = "local8080";
    // } else if (url.indexOf("localhost") > -1) 
    // {
    //   var mode = 'local';
    // } else if (url.indexOf("54.82.161.147") > -1) 
    // {
    //   var mode = 'production';
    // }
    //  else if (url.indexOf("order.greenhoppingapp.com") > -1) 
    // {
    //   var mode = 'productiondns';
    // }
    // else {
    //   var mode = 'live';
    // }
    // console.log(mode);

    // if (mode == 'local') 
    // {
    //   var ghDomain = 'http://localhost/greenhopping';
    // } 
    // else if (mode == 'local8080') 
    // {
    //   var ghDomain = 'http://localhost:8080/greenhopping-web-portal';
    // }
    // else if (mode == 'production') 
    // {
    //   var ghDomain = 'http://54.82.161.147';
    // }
    // else if (mode == 'production') 
    // {
    //   var ghDomain = 'http://order.greenhoppingapp.com';
    // } 
    // else 
    // {
    //     var ghDomain = 'http://52.91.246.152';
    // }
    var url = window.location.href;
    var ghDomain = window.location.href;
    var ghDomain = "https://order.greenhoppingapp.com/";
    
    console.log("+++")
    console.log()
    console.log("---")

    $.cookie('ghDomain', ghDomain, { expires: 7, path: '/' });
    $('.btn-danger').html('Clear')
      .click(function (event) {
        $(".form-control").val('');
        $('#addStore').find("input[type=text],input[type=email],input[type=password], textarea").val("");
        event.preventDefault();
    });

    var _token = $("input[name=_token]").val();
    $("#adminLogin").click(function (event) {
        loginAdmin();
    });


//Triggers

    $("#createStore").click(function (event) {
        createStore();
    });




// Functions

    function loginAdmin() {
        if (!$("#adminLoginForm").valid()) {
            event.preventDefault();
            return false;
        } else
        {
            ("#adminLoginForm").submit();
        }
        return false;
    }

// Create Store
    function createStore() {
        ("#addStore").submit();
        return false
        if (!$("#addStore").valid()) {
            event.preventDefault();
            return false;
        } else
        {
            ("#addStore").submit();
        }
        return false;
    }

// Validations
    //Admin Login
    $("#adminLoginForm").validate({
        errorPlacement: function (error, element) {
            var name = $(element).attr("name");
            error.appendTo($("#adminLogin-" + name));
        },
        rules: {
            email: {
                minlength: 5,
                required: true,
                email: true
            },
            password: {
                required: true,
                minlength: 5,
                maxlength: 100
            }
        }
    });
    //Add Store
    $("#addStores").validate({
        errorPlacement: function (error, element) {
            var name = $(element).attr("name");
            console.log(name);
            error.appendTo($("#addStore-" + name));
        },
        rules: {
            name: {
                minlength: 2,
                maxlength: 25,
                required: true,
            },
            email: {
                minlength: 6,
                maxlength: 35,
                required: true,
            },
            address: {
                minlength: 6,
                maxlength: 35,
                required: true,
            },
            pincode: {
                minlength: 4,
                maxlength: 10,
                number: true,
                required: true,
            }
        }
    });




});
