$(document).ready(function() {


    var userId = Cookies.get('userId');
    var appUrl = Cookies.get('appUrl');
    var btnUpload = jQuery('#poi_pic');
    var status = jQuery('#poi_status');
    var _token = $("input[name=_token]").val();
    new AjaxUpload(btnUpload, {
        action: 'uploadPoi',
        name: 'uploadfile',
        '1': '1',
        onSubmit: function(file, ext) {
            if (!(ext && /^(jpg|jpeg|gif|png)$/.test(ext))) {
                // extension is not allowed 
                status.text('Only JPG or GIF or PNG files are allowed');
                return false;
            }
            status.text('Uploading...');
        },
        onComplete: function(file, response) {
            //On completion clear the status
            console.log(response)
            status.text('');
            //split the response
            var resp = response.split('"');
            console.log(resp)
                //Add uploaded file to list
            if (resp[5] == 1) {
                var img = resp[9];
                $("#poiUrl").val(img)
                uploadImage(img, userId)

                $(".poiImg").attr('src', img);
            } else {
                //jQuery('#imgErr').text(resp[9]);
                //alert('Fail', resp[9]);
                alert(resp[9]);

            }
        }
    });


    function uploadImage(img, userId) {
        $.post(appUrl + "uploadPoiImg", { _token: _token, id: userId, poi_img: img })
            .done(function(data) {
                if (data.success == 1) {
                    //alert('Success', data.message);
                } else {
                    alert('Fail', data.message);
                }
            });
    }

    // function alert(title, message) {
    //     if (title == 'Success') {
    //         alert(message)
    //     } else {
    //         alert(message)
    //     }
    // }
});