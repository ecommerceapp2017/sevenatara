$(document).ready(function() {


    var userId = Cookies.get('userId');
    var appUrl = Cookies.get('appUrl');
    var btnUpload = jQuery('#aadhar_pic');
    var status = jQuery('#aadhar_status');
    var _token = $("input[name=_token]").val();
    new AjaxUpload(btnUpload, {
        action: 'uploadAadhar',
        name: 'uploadfile',
        '1': '1',
        onSubmit: function(file, ext) {
            if (!(ext && /^(jpg|jpeg|gif|png)$/.test(ext))) {
                // extension is not allowed 
                status.text('Only JPG or GIF or PNG files are allowed');
                return false;
            }
            status.text('Uploading...');
        },
        onComplete: function(file, response) {
            //On completion clear the status
            console.log(response)
            status.text('');
            //split the response
            var resp = response.split('"');
            console.log(resp)
                //Add uploaded file to list
            if (resp[5] == 1) {
                var img = resp[9];

                uploadImage(img, userId)

                $(".aadharImg").attr('src', img);
            } else {
                //jQuery('#imgErr').text(resp[9]);
                alert('Fail', resp[9]);

            }
        }
    });

    function uploadImage(img, userId) {
        $.post(appUrl + "updateAadhar", { _token: _token, id: userId, img: img })
            .done(function(data) {
                if (data.success == 1) {
                    //alert('Success', data.message);
                } else {
                    alert('Fail', data.message);
                }
            });
    }

    function alert(title, message) {
        if (title == 'Success') {
            $('body').xmalert({
                x: 'left',
                y: 'bottom',
                xOffset: 30,
                yOffset: 30,
                alertSpacing: 40,
                lifetime: 3000,
                fadeDelay: 0.3,
                template: 'messageSuccess',
                title: title,
                paragraph: message,
            });
        } else {
            $('body').xmalert({
                x: 'left',
                y: 'bottom',
                xOffset: 30,
                yOffset: 30,
                alertSpacing: 40,
                lifetime: 6000,
                fadeDelay: 0.3,
                template: 'messageError',
                title: title,
                paragraph: message,
            });
        }
    }
});