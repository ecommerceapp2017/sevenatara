$(document).ready(function() {
    var userId = Cookies.get('userId');
    var appUrl = Cookies.get('appUrl');
    var btnUpload = jQuery('#we_pic_1'); //button
    var status = jQuery('#we_1_status'); //status
    var _token = $("input[name=_token]").val();
    new AjaxUpload(btnUpload, {
        action: 'userImage',
        name: 'uploadfile',
        '1': '1',
        onSubmit: function(file, ext) {
            if (!(ext && /^(jpg|jpeg|gif|png)$/.test(ext))) {
                // extension is not allowed 
                status.text('Only JPG or GIF or PNG files are allowed');
                return false;
            }
            status.text('Uploading...');
        },
        onComplete: function(file, response) {
            console.log(response)
            status.text('');
            var resp = response.split('"');
            console.log(resp)
            if (resp[5] == 1) {
                var img = resp[9];
                $(".weImg_1").attr('src', img);
                $("#weImg_1_Txt").val(img);
            } else {
                alert(resp[9]);

            }
        }
    });
});