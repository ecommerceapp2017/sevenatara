$(document).ready(function() {
    var appUrl = Cookies.get("appUrl");
    $(document).on("click", ".loginUser", function() {
        event.preventDefault();
        console.log('clicked')
        var _token = $("input[name=_token]").val();
        var selection = 'login';
        var email = $("#" + selection + "email").val();
        var password = $("#" + selection + "password").val();
        var error = '';
        $(".err").html('')
        if (email == '') {
            $("#" + selection + "erroremail").html('Email is required');
            error = 'email';
        }
        if (password == '') {
            $("#" + selection + "errorpassword").html('Password is required');
            error = 'password';
        }
        if (error != '') {
            return;
        }
        $.post(appUrl + "loginUser", { _token: _token, email: email, password: password })
            .done(function(data) {
                if (data.success == 1) {
                    Cookies.set("userId", data.data.id)
                    location.replace(appUrl);
                } else if (data.success == 0) {
                    $("#loginerroremail").html(data.message);
                } else if (data.success == 2) {
                    $("#loginerrorpassword").html(data.message);
                } else {
                    console.log(data)
                }
            });
    });


    $(document).on("click", ".registerUser", function() {
        event.preventDefault();
        console.log('clicked')
        var _token = $("input[name=_token]").val();
        var selection = 'register';
        var phone = $("#" + selection + "phone").val();
        var email = $("#" + selection + "email").val();
        var password = $("#" + selection + "password").val();
        var referal_code = $("#referal_code").val();
        var error = '';
        $(".err").html('')
        if (email == '') {
            $("#" + selection + "erroremail").html('Email is required');
            error = 'email';
        }
        if (password == '') {
            $("#" + selection + "errorpassword").html('Password is required');
            error = 'password';
        }
        if (phone == '') {
            $("#" + selection + "errorphone").html('Phone is required');
            error = 'password';
        }
        if (error != '') {
            return;
        }
        $.post(appUrl + "registerUser", { _token: _token, phone: phone, email: email, password: password, referal_code: referal_code })
            .done(function(data) {
                if (data.success == 1) {
                    console.log(data)
                    hideforOTP(1);
                    backGroundOTP(data.id);
                    //$(".otp").val(data.otp);
                    $("#registeredID").val(data.id);
                } else if (data.success == 0) {
                    if (data.error.phone) {
                        $("#registererrorphone").html(data.error.phone);
                    }
                    if (data.error.email) {
                        $("#registererroremail").html(data.error.email);
                    }

                } else if (data.success == 2) {
                    $("#registererrorreferal_code").html(data.error);
                } else {
                    console.log(data)
                }
            });
    });

    $(document).on("click", ".otpUser", function() {
        event.preventDefault();
        console.log('clicked')
        var _token = $("input[name=_token]").val();
        var selection = 'register';
        var otp = $("#referal_code").val();
        var registeredID = $("#registeredID").val();
        var error = '';
        $(".err").html('')
        if (otp == '') {
            $("#" + selection + "errorotp").html('OTP is required');
            error = 'otp';
        }
        if (registeredID == '') {
            $("#registererrorotp").html('Internal Error');
            error = 'otp';
        }
        if (error != '') {
            return;
        }
        $.post(appUrl + "otpVerify", { _token: _token, id: registeredID, otp: otp })
            .done(function(data) {
                if (data.success == 1) {
                    hideforOTP(2);
                } else if (data.success == 0) {
                    $("#registererrorotp").html(data.message);
                } else {
                    console.log(data)
                }
            });
    });


    $(document).on("click", ".resetPassword", function() {
        event.preventDefault();
        console.log('clicked')
        var _token = $("input[name=_token]").val();
        var selection = 'login';
        var email = $("#" + selection + "email").val();
        var error = '';
        $(".err").html('')
        if (email == '') {
            $("#" + selection + "erroremail").html('Email is required');
            error = 'email';
        }
        if (error != '') {
            return;
        }


        $.post(appUrl + "resetPassword", { _token: _token, email: email })
            .done(function(data) {
                if (data.success == 1) {
                    alert("Password sent to mail");
                    showLoginArea();
                } else {
                    console.log(data.data);
                }
            });
    });

    $(document).on("click", ".forgotButton", function() {
        showResetArea();
    });

    $(document).on("click", ".loginButton", function() {
        showLoginArea();
    });
    $(".loginButton").hide();
    $(".resetPassword").hide();


    function showResetArea() {
        $(".loginTitle").html('Reset Password');
        $(".loginPassword").hide();
        $(".err").html('');
        $(".loginButton").show();
        $(".resetPassword").show();
        $("#loginAction").removeClass();
        $("#loginAction").addClass("form-group");
        $("#loginAction").addClass("resetPassword");
        $("#forgotAction").removeClass();
        $("#forgotAction").addClass("pull-right");
        $("#forgotAction").addClass("loginButton");
        $("#forgotAction").html("Back to Login");
        $(".loginActionText").html("Reset");
    }

    function showLoginArea() {
        $(".loginTitle").html('Create an account');
        $(".loginPassword").show();
        $(".err").html('');
        $(".loginButton").show();
        $(".resetPassword").show();
        $("#loginAction").removeClass();
        $("#loginAction").addClass("form-group");
        $("#loginAction").addClass("loginUser");
        $("#forgotAction").removeClass();
        $("#forgotAction").addClass("pull-right");
        $("#forgotAction").addClass("forgotButton");
        $("#forgotAction").html("Back to Login");
        $(".loginActionText").html("Login");
    }

    function hideforOTP(type) {
        $(".err").html('');
        if (type == '1') {
            $("#registerEmailGroup").hide();
            $("#registerPhoneGroup").hide();
            $("#registerPasswordGroup").hide();
            $("#registerAction").removeClass();
            $("#ref_title").html("Enter OTP");
            $("#referal_code").attr("placeholder", "Enter OTP");
            $("#registerAction").addClass("btn btn-default round btn-md otpUser");
            $("#registerAction").html("Submit OTP");
        } else {
            // $('body').xmalert({
            //     x: 'left',
            //     y: 'bottom',
            //     xOffset: 30,
            //     yOffset: 30,
            //     alertSpacing: 40,
            //     lifetime: 6000,
            //     fadeDelay: 0.3,
            //     template: 'messageSuccess',
            //     title: 'OTP Verification Success',
            //     paragraph: 'Your OTP is Verified. You can proceed to Login now',
            // });
            $("#referal_code").val("");
            $("#registeremail").val("");
            $("#registerphone").val("");
            $("#registerpassword").val("");
            alert("OTP Verification Success");
            $("#registerEmailGroup").show();
            $("#registerPhoneGroup").show();
            $("#registerPasswordGroup").show();
            $("#registerAction").removeClass();
            $("#ref_title").html("Referal Code");
            $("#referal_code").attr("placeholder", "Referal Code");
            $("#registerAction").addClass("btn btn-default round btn-md otpUser");
            $("#registerAction").html("Submit OTP");
        }
    }

    function backGroundOTP(id) {
        var _token = $("input[name=_token]").val();
        $.post(appUrl + "otpProcess", { id: id, _token: _token })
            .done(function(data) {
                console.log(data)
            });
    }
    $(document).on("click", ".contactUs", function() {
        event.preventDefault();
        console.log('clicked')
        var _token = $("input[name=_token]").val();
        var name = $("#name").val();
        var email = $("#email").val();
        var message = $("#message").val();
        var error = '';
        $(".err").html('')
        if (name == '') {
            $("#name_err").html('Name is required');
            error = 'email';
        }
        if (email == '') {
            $("#email_err").html('Email is required');
            error = 'email';
        }
        if (message == '') {
            $("#message_err").html('Message is required');
            error = 'email';
        }
        if (error != '') {
            return;
        }
        $.post(appUrl + "contactUs", { _token: _token, name: name, email: email, message: message })
            .done(function(data) {
                if (data.success == 1) {
                    $(".success").html('<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Form submitted succesfully !</strong></div>');
                    setTimeout(function() {
                        $(".success").hide();
                    }, 2000);
                } else if (data.success == 0) {
                    //$("#loginerroremail").html(data.message);
                } else if (data.success == 2) {
                    //$("#loginerrorpassword").html(data.message);
                } else {
                    console.log(data)
                }
            });
    });
});