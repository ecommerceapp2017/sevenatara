$(document).ready(function() {
    var url = window.location.href; // Returns full URL
    if (url.indexOf("localhost") > -1) {
        var mode = 'local';
    } else {
        var mode = 'live';
    }

    if (mode == 'local') {
        var appUrl = 'http://localhost/project/7A/sevenatara/';
        Cookies.set('appUrl', appUrl, { expires: 7 });
    } else {
        var appUrl = 'http://sevenatara.com/beta/';
        Cookies.set('appUrl', appUrl, { expires: 7 });
    }

    var img = 0;

    $(document).on("click", ".img", function() {
        var id = this.id;
        var i = id.substring(4);
        img = i;
        var src = $('.' + img).val();
        console.log(img);
        makeActive(img);
        $(".product-img-detail").attr('src', src);
    });
    $(document).on("click", ".carousel-control", function() {
        var dir = this.id;
        if (dir == 'left') {

            if (img == 0) {
                img = 0;
            } else {
                img--
            }
        } else {
            if (img == 5) {
                img = 5;
            } else {
                img++;
            }
        }
        var src = $('.' + img).val();
        console.log(img);
        makeActive(img);
        $(".product-img-detail").attr('src', src);
    });

    function makeActive(id) {
        for (var index = 1; index < 6; index++) {
            if (index == id) {
                $("#img_" + index).removeClass();
            } else {
                $("#img_" + index).removeClass();
                $("#img_" + index).addClass('img active');
            }

        }
    }

    $(document).on("click", ".addCart", function() {
        var str = this.id;
        var res = str.split("|");
        var user = res[0];
        var product = res[1];
        var count = $("#single-item").prop("value");
        updateCart(user, product, count);
    });

    $(document).on("click", ".deleteCart", function() {
        var d = $(this).attr("data-id")
        $.get(appUrl + "deleteCart/" + this.id)
            .done(function(data) {
                if (data.success == 1) {
                    alert(data.message);
                    if (data.count == 0) {
                        $('#checkOut').hide();
                    }
                    $(".trr_" + d).hide();
                    // setTimeout(
                    //     function() {
                    //         location.reload();
                    //     }, 1000);
                } else {
                    alert(data.message);
                }
            });
    });

    $(".qty-item").change(function() {
        var price = this.id;
        price = $("#pri_" + price).html();
        var qty = $("#" + this.id).prop("value");
        var tot = price * qty;
        $("#tot_" + this.id).html('Rs.' + tot);
        $(".to_" + this.id).val(tot);

        var userId = Cookies.get('userId');
        var product_id = this.id;
        var count = qty;
        updateCart(userId, product_id, qty)
            //updateTotal();
    });

    $(".qty-item-checkout").change(function() {
        var price = this.id;
        price = $("#pri_" + price).html();
        var qty = $("#" + this.id).prop("value");
        var tot = price * qty;
        $("#tot_" + this.id).html('Rs.' + tot);
        $(".to_" + this.id).val(tot);

        var userId = Cookies.get('userId');
        var product_id = this.id;
        var count = qty;
        updateCart(userId, product_id, qty)
        updateTotal();
    });

    function updateTotal() {
        var total = 0;
        $('.tt').each(function(i, obj) {
            var oo = obj;
            var sub = $(oo).val()
            total = Number(total) + Number(sub);
        });
        console.log(total)
        total = Math.round(total);
        $(".sub_total_all").html('Rs.' + total)
        $(".sub_total_hide_all").val(total)
            //alert(total)
            //$('#razor').html('<form action="/purchase" method="POST"> <script src = "https://checkout.razorpay.com/v1/checkout.js"data - key = "rzp_test_m9r9OoPz5KBcJ0"data - amount = "9999" data - buttontext = "Pay with Razorpay"data - name = "Seven Atara" data - description = "Website Purchase" data - image = "https://your-awesome-site.com/your_logo.jpg" data - prefill.name = "<?php echo Auth::user()->first_name ?> <?php echo Auth::user()->last_name ?>                     data - prefill.email = "<?php echo Auth::user()->email?>" data - theme.color = "#14e8b9"id = "paymentWidget" ></script> <input type = "hidden" value = "Hidden Element"name = "hidden" ></form>')
            //$('#paymentWidget').attr('data-amount', total * 100)
            //$("#frame").attr("src", "http://localhost/test/raz.php?rs=900");
            //$("#content").load("http://localhost/test/raz.php?rs=900");//
        localStorage.setItem('amt', total * 100)
            //document.getElementById("paymentWidget").attr("data-amount") = total
    }


    $(document).on("click", "#payBtn", function() {
        var options = {
            "key": "rzp_test_m9r9OoPz5KBcJ0",
            "amount": localStorage.getItem("amt"),
            "name": "Seven Atara",
            "description": "Purchase on Website",
            "image": "/your_logo.png",
            "handler": function(response) {
                //alert(response.razorpay_payment_id);
                placeOrder(response.razorpay_payment_id)
            },
            "prefill": {
                "name": localStorage.getItem("name"),
                "email": localStorage.getItem("email")
            },
            "notes": {
                "address": "Hello World"
            },
            "theme": {
                "color": "#14e8b9"
            }
        };
        var rzp1 = new Razorpay(options);
        rzp1.open();
    });

    var options = {
        "key": "rzp_test_m9r9OoPz5KBcJ0",
        "amount": localStorage.getItem("amt"),
        "name": "Seven Atara",
        "description": "Purchase on Website",
        "image": "/your_logo.png",
        "handler": function(response) {
            placeOrder(response.razorpay_payment_id)
                //alert(response.razorpay_payment_id);
        },
        "prefill": {
            "name": localStorage.getItem("name"),
            "email": localStorage.getItem("email")
        },
        "notes": {
            "address": "Hello World"
        },
        "theme": {
            "color": "#14e8b9"
        }
    };


    var rzp1 = new Razorpay(options);
    document.getElementById('rzp-button1').onclick = function(e) {
        rzp1.open();
        e.preventDefault();
    }


    function updateCart(user, product, count) {
        $.get(appUrl + "addCart/" + user + "/" + product + "/" + count)
            .done(function(data) {
                if (data.success == 1) {
                    alert(data.message);
                    $('.cart-val').html(data.count)
                    // setTimeout(
                    //     function() {
                    //         location.reload();
                    //     }, 1000);
                } else {
                    alert(data.message);
                }
            });
    }

    $(document).on("click", ".couponCode", function() {
        setTimeout(
            function() {
                alert('Invalid Coupon Code')
            }, 1000);
    });

    $(document).on("click", "#submitPayment", function() {
        //Address Check
        var error = '';
        $(".error").html('');
        var first_name = $("#first_name").val();
        if (first_name == '') {
            error = 'name'
            $("#first_name_error").html('First Name is required')
        }
        var last_name = $("#last_name").val();
        if (last_name == '') {
            error = 'name'
            $("#last_name_error").html('Last Name is required')
        }
        var email = $("#email").val();
        if (email == '') {
            error = 'name'
            $("#email_error").html('Email is required')
        }
        var email = $("#email").val();
        if (email == '') {
            error = 'name'
            $("#email_error").html('Email is required')
        }
        var phone = $("#phone").val();
        if (email == '') {
            error = 'name'
            $("#email_error").html('Phone is required')
        }
        var address = $("#address").val();
        if (address == '') {
            error = 'name'
            $("#address_error").html('Address is required')
        }

        if (error != '') {
            $("#bil-info").trigger("click");
            return;
        } else {
            $(".error").html('');
            firePayment();
        }
        //Cart Check
    });

    function firePayment() {
        $("#payBtn").trigger('click');
        //placeOrder();
    }

    function placeOrder(raz_id) {
        var userId = Cookies.get('userId');
        $.get(appUrl + "placeOrder/" + userId + "/" + raz_id)
            .done(function(data) {
                if (data.success == 1) {
                    $(".col-sm-9").html('<h1 style="color:green">Payment done Success !</h1><br> You will be redirected to home page in 3 seconds')
                        //alert(data.message);
                        //$(".trr_" + d).hide();
                    console.log(data);
                    // setTimeout(
                    //     function() {
                    //         window.location.href = appUrl + '/my-orders';
                    //     }, 3000);
                } else {
                    alert(data.message);
                }
            });
    }





});
