$(document).ready(function() {
    var appUrl = Cookies.get('appUrl');
    var userId = Cookies.get('userId');
    //$(".userImg").attr('src', Cookies.get('fbImg'));
    var _token = $("input[name=_token]").val();

    $(document).on("click", "#updateProfile", function() {
        event.preventDefault();
        var firstname = $("#firstname").val();
        var lastname = $("#lastname").val();
        var address = $("#address").val();
        var lat = $("#lat").val();
        var lng = $("#lng").val();
        $(".error").html("");
        var error = '';
        if (firstname == '') {
            $("#first_name_error").html("First Name should not be empty");
            error += 'First Name should not be empty'
        }
        if (lastname == '') {
            $("#last_name_error").html("Last Name should not be empty");
            error += 'Last Name should not be empty'
        }
        if (address == '') {
            $("#address_error").html("Address should not be empty");
            error += '<br>Address should not be empty'
        }
        if (lat == '') {
            $("#address_error").html("Please use Google Autocomplete");
            error += '<br>Address should not be empty'
        }
        if (lng == '') {
            $("#address_error").html("Please use Google Autocomplete");
            error += '<br>Address should not be empty'
        }
        if (error != '') {
            //alert('Error', error);
            return false;
        }
        $.post(appUrl + "updateProfile", { _token: _token, firstname: firstname, lastname: lastname, id: userId, address: address, lat : lat, lng : lng })
            .done(function(data) {
                if (data.success == 1) {
                    window.location = "upgrade";
                } else {
                    console.log(data.data);
                }
            });
    });

    $(document).on("click", "#updatePasswords", function() {
        event.preventDefault();
        var old_pwd = $("#old_pwds").val();
        var new_pwd = $("#new_pwds").val();
        var error = '';
        if (old_pwd == '') {
            error += 'Old Password not be empty'
        }
        if (new_pwd == '') {
            error += '<br>New Password should not be empty'
        }
        if (error != '') {
            alert('Error', error);
            return false;
        }
        var _token = $("input[name=_token]").val();
        $.post(appUrl + "updatePassword", { _token: _token, old_password: old_pwd, new_password: new_pwd, id: userId })
            .done(function(data) {
                if (data.success == 1) {
                    alert('Success', data.message);
                } else {
                    alert('Error', data.message);
                }
            });
    });


    $(document).on("click", "#uploadImage", function() {
        v
        event.preventDefault();
        console.log('start')
    });

    //Common

    $(window).keydown(function(event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            if (event.target.form.id == 'updatePassword') {
                $(".updatePassword").trigger("click");
            } else {
                $(".updateProfile").trigger("click");
            }
        }
    });

    // function alert(title, message) {
    //     if (title == 'Success') {
    //         alert(message)
    //     } else {
    //         alert(message)
    //     }
    // }

    $(document).on("click", ".upgradeButton", function() {
        var id = this.id.split("_");
        $(".commonForm").slideToggle();
        $(".frmElement").hide();
        formElements(id[1]);
        $(".mainBox").slideToggle();
    });

    $(document).on("click", ".closeForm", function() {
        $(".mainBox").slideToggle();
        $(".commonForm").slideToggle();
    });

    function formElements(form) {
        if (form == 'buyer') {
            $(".formTitle").html('Upgrade as Buyer');
            formElementsSub(form);
        } else if (form == 'broker') {
            $(".formTitle").html('Upgrade as Broker');
            formElementsSub(form);
        } else if (form == 'lessee') {
            $(".formTitle").html('Upgrade as Lessee');
            formElementsSub(form);
        } else if (form == 'lessor') {
            $(".formTitle").html('Upgrade as Lesssor');
            formElementsSub(form);
        } else if (form == 'seeker') {
            $(".formTitle").html('Upgrade as Seeker');
            formElementsSub(form);
        } else if (form == 'seller') {
            $(".formTitle").html('Upgrade as Seller');
            formElementsSub(form);
        } else {
            removeCl('actionButton')
        }
    }

    function formElementsSub(form) {
        removeCl('actionButton')
        $("#actionButton").addClass(form + 'btn');
        $(".referalCode").show();
    }

    function removeCl(id) {
        $("#" + id).removeClass();
        $("#" + id).addClass('button');
        $("#" + id).addClass('big');
        $("#" + id).addClass('dark');
    }

    $(document).on("click", ".buyerbtn", function() {
        var referal_code = $("#referal_code").val();
        var checkBox = checkbox();
        if (checkBox == '1') {
            $.post(appUrl + "upgradeBuyer", { _token: _token, id: userId, referal_code: referal_code })
                .done(function(data) {
                    if (data.success == 1) {
                        alert('Success', data.message);
                        upgradeBtn('btn_buyer');
                    } else {
                        console.log(data.data);
                    }
                });
        } else {
            alert("Error", checkBox)
        }
    });
    $(document).on("click", ".brokerbtn", function() {
        var referal_code = $("#referal_code").val();
        var checkBox = checkbox();
        if (checkBox == '1') {
            $.post(appUrl + "upgradeBroker", { _token: _token, id: userId, referal_code: referal_code })
                .done(function(data) {
                    if (data.success == 1) {
                        alert('Success', data.message);
                        upgradeBtn('btn_broker');
                    } else {
                        alert('Error', data.message);
                    }
                });
        } else {
            alert("Error", checkBox)
        }
    });
    $(document).on("click", ".lesseebtn", function() {
        var referal_code = $("#referal_code").val();
        var checkBox = checkbox();
        if (checkBox == '1') {
            $.post(appUrl + "upgradeLessee", { _token: _token, id: userId, referal_code: referal_code })
                .done(function(data) {
                    if (data.success == 1) {
                        alert('Success', data.message);
                        upgradeBtn('btn_lessee');
                    } else {
                        alert('Error', data.message);
                    }
                });
        } else {
            alert("Error", checkBox)
        }
    });
    $(document).on("click", ".lessorbtn", function() {
        var referal_code = $("#ref_code").val();
        var aadharUrl = $("#aadharUrl").val();
        if (aadharUrl == '') {
            alert('Aadhar Image is required');
        } else {
            var checkBox = checkboxx();
            if (checkBox == '1') {
                $.post(appUrl + "upgradeLessor", { _token: _token, id: userId, referal_code: referal_code })
                    .done(function(data) {
                        if (data.success == 1) {
                            //alert('Success', data.message);
                            alert(data.message);
                            location.reload();
                            upgradeBtn('btn_lessor');
                            upgradeBtn('btn_seller');
                        } else {
                            alert(data.message);
                        }
                    });
            } else {
                //alert("Error", checkBox)
                alert(checkBox)
            }
        }
    });
    $(document).on("click", ".seekerbtn", function() {
        var referal_code = $("#referal_code").val();
        var checkBox = checkbox();
        if (checkBox == '1') {
            $.post(appUrl + "upgradeSeeker", { _token: _token, id: userId, referal_code: referal_code })
                .done(function(data) {
                    if (data.success == 1) {
                        alert('Success', data.message);
                        upgradeBtn('btn_seeker');
                    } else {
                        alert('Error', data.message);
                    }
                });
        } else {
            alert("Error", checkBox)
        }
    });

    $(document).on("click", ".sellerbtn", function() {
        var aadharUrl = $("#aadharUrl").val();
        if (aadharUrl == '') {
            alert('Error', 'Aadhar Image is required');
        } else {
            var referal_code = $("#referal_code").val();
            var checkBox = checkbox();
            if (checkBox == '1') {
                $.post(appUrl + "upgradeSeller", { _token: _token, id: userId, referal_code: referal_code })
                    .done(function(data) {
                        if (data.success == 1) {
                            alert('Success', data.message);
                            upgradeBtn('btn_lessor');
                            upgradeBtn('btn_seller');
                        } else {
                            alert('Error', data.message);
                        }
                    });
            } else {
                alert("Error", checkBox)
            }
        }
    });

    $(document).on("click", "#aadharButton", function() {
        var referal_code = $("#referal_code").val();
        $(".err").html('');
        if (referal_code == '') {
            $("#aadharError").html("Enter your Aadhar Card Number")
            alert("Enter your Aadhar Card Number");
        } else {
            var checkBox = checkbox();
            if (checkBox == '1') {
                $.post(appUrl + "linkAadharNumber", { _token: _token, id: userId, aadhar_number: referal_code })
                    .done(function(data) {
                        if (data.success == 1) {
                            alert(data.message);
                            location.reload();
                            // setTimeout(
                            //     function() {
                            //         location.reload();
                            //     }, 2000);
                        } else {
                            alert(data.message);
                        }
                    });
            } else {
                alert(checkBox);
            }
        }
    });


    function upgradeBtn(btn) {
        $("#" + btn).removeClass();
        $("#" + btn).addClass('button');
        $("#" + btn).addClass('primary');
        $("#" + btn).addClass('upgradeButton');
        $("#" + btn).html('Upgraded');
        $(".closeForm").trigger("click");
    }

    function checkbox() {
        var check;
        if (show_balance.checked) {
            return '1';
        } else {
            return 'Please check the Terms and Conditions';
        }
    }

    function checkboxx() {
        var check;
        if (show_bal.checked) {
            return '1';
        } else {
            return 'Please check the Terms and Conditions';
        }
    }
    $("#uploadForm").hide();
    $(document).on("change", "#sampleData", function() {
        var selection = this.value;
        if (selection == 'Yes') {
            $("#uploadForm").show();
        } else {
            $("#uploadForm").hide();
        }
    });

    $(document).on("change", "#p_cat_type", function() {
        var selection = this.value;
        $.post(appUrl + "getSubCategory", { id: selection })
            .done(function(data) {
                if (data.success == 1) {
                    $("#p_sb_type").empty();
                    $.each(data.sub, function(key, value) {
                        $("#p_sb_type").append(new Option(value.name, value.id));
                    });
                } else {
                    console.log("Error")
                }
            });
    });

    $(".err").hide();
    $(document).on("click", "#updateProduct", function() {
        $(".err").html('');
        var cat = $("#p_cat_type option:selected").val();
        if (cat == '' || cat == undefined) {
            error = 'name';
            $("#p_cat_type_err").html('Category should not be empty');
            $("#p_cat_type_err").show();
//            p_sb_type_err
        }
        var sub = $("#p_sb_type option:selected").val();
        if (sub == '' || sub == undefined) {
            error = 'name';
            $("#p_sb_type_err").html('Sub Category should not be empty');
            $("#p_sb_type_err").show();
        }
        var type = $("input[name=p_p_type]:checked").val();
        var p_p_name = $("#p_p_name").val();
        var error = '';
        if (p_p_name == '') {
            error = 'name';
            $("#p_p_name_err").html('Name should not be empty');
            $("#p_p_name_err").show();
        }
        var p_p_length = $("#p_p_length").val();
        if (p_p_length == '') {
            error = 'name';
            $("#p_p_length_err").html('Length should not be empty');
            $("#p_p_length_err").show();
        }
        var p_p_breadth = $("#p_p_breadth").val();
        if (p_p_breadth == '') {
            error = 'name';
            $("#p_p_breadth_err").html('Breadth should not be empty');
            $("#p_p_breadth_err").show();
        }
        var p_p_height = $("#p_p_height").val();
        if (p_p_height == '') {
            error = 'name';
            $("#p_p_height_err").html('Height should not be empty');
            $("#p_p_height_err").show();
        }
        var p_p_weight = $("#p_p_weight").val();
        if (p_p_weight == '') {
            error = 'name';
            $("#p_p_weight_err").html('Weight should not be empty');
            $("#p_p_weight_err").show();
        }
        var p_p_price = $("#p_p_price").val();
        if (p_p_price == '') {
            error = 'name';
            $("#p_p_price_err").html('Price should not be empty');
            $("#p_p_price_err").show();
        }
        var p_p_unit_type = $("#p_p_unit_type option:selected").val();
        var p_p_unit_others = $("#p_p_others").val();
        if (p_p_unit_type == '36') {
            if (p_p_unit_others == '') {
                $("#p_p_others_err").html("Units should not be empty");
                $("#p_p_others_err").show();
            }
        }
        var p_p_gst = $("#p_p_gst option:selected").val();
        var p_p_supply_capacity = $("#p_p_supply_capacity").val();
        if (p_p_supply_capacity == '') {
            error = 'name';
            $("#p_p_supply_capacity_err").html('Supply Capacity should not be empty');
            $("#p_p_supply_capacity_err").show();
        }

        var ar = $("input[name='color[]']:checked")
        var p_p_color = '';
        for (var index = 0; index < ar.length; index++) {
            p_p_color = p_p_color + ar[index].value + ',';
        }

        if (p_p_color == '') {
            error = 'name';
            $("#p_p_color_err").html('Color should not be empty');
            $("#p_p_color_err").show();
        }
        var p_p_variety = $("#p_p_variety").val();
        if (p_p_variety == '') {
            error = 'name';
            $("#p_p_variety_err").html('Variety should not be empty');
            $("#p_p_variety_err").show();
        }
        var p_p_material_type = $("#p_p_material_type").val();
        if (p_p_material_type == '') {
            error = 'name';
            $("#p_p_material_type_err").html('Material Type should not be empty');
            $("#p_p_material_type_err").show();
        }
        var p_p_description = $("#p_p_description").val();
        if (p_p_description == '') {
            error = 'name';
            $("#p_p_description_err").html('Product Description should not be empty');
            $("#p_p_description_err").show();
        }
        var p_p_moq = $("#p_p_moq").val();
        if (p_p_moq == '') {
            //Check Logic Part as example given
            error = 'name';
            $("#p_p_moq_err").html('Minimum Order Quantity should not be empty');
            $("#p_p_moq_err").show();
        }
        var p_p_returns = $("#p_p_returns").val();
        if (p_p_returns == '') {
            error = 'name';
            $("#p_p_returns_err").html('Return Quantity should not be empty');
            $("#p_p_returns_err").show();
        }
        var img = $("#u_p_p_im").val();
        if (img == '') {
            error = 'img';
            $("#u_p_p_im_err").html('Product Image should not be empty');
            $("#u_p_p_im_err").show();
        }
        var p_p_sample = $("input[name=p_p_sample]:checked").val();
        // var p_p_gurantee = $("input[name=p_p_gurantee]:checked").val();
        // var p_p_warranty = $("input[name=p_p_warranty]:checked").val();
        if (error != '') {
          console.log(console.error(););
            return false;
        }

        $(".err").html('');
        $(".err").hide();

        var pid = $("#pid").val();
        //gurantee: p_p_gurantee, warranty: p_p_warranty,img: img,
        $.post(appUrl + "updateProduct", { id: userId, pid: pid, type: type, cat: cat, sub: sub,  name: p_p_name, price: p_p_price, length : p_p_length, breadth : p_p_breadth , height : p_p_height, weight : p_p_weight, unit: p_p_unit_type, unit_others: p_p_unit_others, gst: p_p_gst, supply_capacity: p_p_supply_capacity, color: p_p_color, variety: p_p_variety, material_type: p_p_material_type, description: p_p_description, moq: p_p_moq, returns: p_p_returns, sample: p_p_sample,
        })
            .done(function(data) {
                if (data.success == 1) {
                    alert(data.message);
                } else {
                    alert(data.message);
                }
            });
    });



    //$(".war_pic").hide();
    $('input[type=radio][name=p_p_warranty]').change(function() {
        if (this.value == 1) {
            $(".war_pic").show();
        } else {
            $(".war_pic").hide();

        }
    });

    //$(".gua_pic").hide();
    $('input[type=radio][name=p_p_gurantee]').change(function() {
        if (this.value == 1) {
            $(".gua_pic").show();
        } else {
            $(".gua_pic").hide();

        }
    });


    //Changing unit type accordingly
    var unit = $("#p_p_unit_type option:selected").html();
    $("#othersTab").hide()
    $(".unitChosen").html(unit);
    $(document).on("change", "#p_p_unit_type", function() {
        var unit = $("#p_p_unit_type option:selected").html();
        console.log(unit);
        if (unit == 'Others') {
            $("#othersTab").show()
        } else {
            $("#othersTab").hide()
        }
        $(".unitChosen").html(unit);
    });

    $('#assorted').change(function() {
        if ($(this).is(":checked")) {
            $('.oth_col').prop('checked', false);
        } else {
            console.log('assorted')
        }
    });

    $('.oth_col').change(function() {
        if ($(this).is(":checked")) {
            $('.un_col').prop('checked', false);
        }
    });
    var warranty_data_status = 0;
    $('.warranty_data').change(function() {
        var state =  $(this)[0].id;
        if(state=='warranty_yes'){
            warranty_data_status = 1;
            $(".war_pic").show();
        }else{
          $(".war_pic").hide();
            warranty_data_status = 0;
        }
    });
    var guarantee_data_status = 0;
    $('.guarantee_data').change(function() {
        var state =  $(this)[0].id;
        if(state=='guarantee_yes'){
            guarantee_data_status = 1;
            $(".gua_pic").show();
        }else{
          $(".gua_pic").hide();
            guarantee_data_status = 0;
        }
    });

    $(document).on("click", "#postProduct", function() {
        $(".err").html('');
        var cat = $("#p_cat_type option:selected").val();
        if (cat == '' || cat == undefined) {
            error = 'name';
            $("#p_cat_type_err").html('Category should not be empty');
            $("#p_cat_type_err").show();
            p_sb_type_err
        }
        var sub = $("#p_sb_type option:selected").val();
        if (sub == '' || sub == undefined) {
            error = 'name';
            $("#p_sb_type_err").html('Sub Category should not be empty');
            $("#p_sb_type_err").show();
        }
        var type = $("input[name=p_p_type]:checked").val();
        var p_p_name = $("#p_p_name").val();
        var error = '';
        if (p_p_name == '') {
            error = 'name';
            $("#p_p_name_err").html('Name should not be empty');
            $("#p_p_name_err").show();
        }
        var p_p_price = $("#p_p_price").val();
        if (p_p_price == '') {
            error = 'name';
            $("#p_p_price_err").html('Price should not be empty');
            $("#p_p_price_err").show();
        }
        var p_p_unit_type = $("#p_p_unit_type option:selected").val();
        var p_p_unit_others = $("#p_p_others").val();
        if (p_p_unit_type == '36') {
            if (p_p_unit_others == '') {
                $("#p_p_others_err").html("Units (others) should not be empty");
                $("#p_p_others_err").show();
            }
        }
        var p_p_gst = $("#p_p_gst option:selected").val();
        var p_p_supply_capacity = $("#p_p_supply_capacity").val();
        if (p_p_supply_capacity == '') {
            error = 'name';
            $("#p_p_supply_capacity_err").html('Supply Capacity should not be empty');
            $("#p_p_supply_capacity_err").show();
        }
        //var p_p_color = $("#p_p_color").val();
        var ar = $("input[name='color[]']:checked")
        var p_p_color = '';
        for (var index = 0; index < ar.length; index++) {
            p_p_color = p_p_color + ar[index].value + ',';
        }
        if (p_p_color == '') {
            error = 'name';
            $("#p_p_color_err").html('Color should not be empty');
            $("#p_p_color_err").show();
        }
        var p_p_variety = $("#p_p_variety").val();
        if (p_p_variety == '') {
            error = 'name';
            $("#p_p_variety_err").html('Variety should not be empty');
            $("#p_p_variety_err").show();
        }
        var p_p_material_type = $("#p_p_material_type").val();
        if (p_p_material_type == '') {
            error = 'name';
            $("#p_p_material_type_err").html('Material Type should not be empty');
            $("#p_p_material_type_err").show();
        }
        var p_p_description = $("#p_p_description").val();
        if (p_p_description == '') {
            error = 'name';
            $("#p_p_description_err").html('Product Description should not be empty');
            $("#p_p_description_err").show();
        }
        var p_p_moq = $("#p_p_moq").val();
        if (p_p_moq == '') {
            error = 'name';
            $("#p_p_moq_err").html('Minimum Order Quantity should not be empty');
            $("#p_p_moq_err").show();
        }
        var p_p_returns = $("#p_p_returns").val();
        if (p_p_returns == '') {
            error = 'name';
            $("#p_p_returns_err").html('Return Quantity should not be empty');
            $("#p_p_returns_err").show();
        }

        if (p_p_returns > p_p_moq) {
            error = 'name';
            $("#p_p_r_m").html('Return Quantity should not less than MOQ');
            $("#p_p_r_m").show();
            error = "d"
        }
        var img = $("#u_p_p_im").val();
        if (img == '') {
            error = 'img';
            $("#u_p_p_im_err").html('Product Image 1 should not be empty');
            $("#u_p_p_im_err").show();
        }
        var img1 = $("#u_p_p_im_1").val();
        if (img1 == '') {
            error = 'img';
            $("#u_p_p_im_1_err").html('Product Image 2 should not be empty');
            $("#u_p_p_im_1_err").show();
        }
        var img2 = $("#u_p_p_im_2").val();
        if (img2 == '') {
            error = 'img';
            $("#u_p_p_im_2_err").html('Product Image 3 should not be empty');
            $("#u_p_p_im_2_err").show();
        }
        var img3 = $("#u_p_p_im_3").val();
        if (img3 == '') {
            error = 'img';
            $("#u_p_p_im_3_err").html('Product Image 4 should not be empty');
            $("#u_p_p_im_3_err").show();
        }
        var img4 = $("#u_p_p_im_4").val();
        if (img4 == '') {
            error = 'img';
            $("#u_p_p_im_4_err").html('Product Image 5 should not be empty');
            $("#u_p_p_im_4_err").show();
        }
        var p_p_sample = $("input[name=p_p_sample]:checked").val();
        // var p_p_gurantee = $("input[name=p_p_gurantee]:checked").val();
        // var p_p_warranty = $("input[name=p_p_warranty]:checked").val();
        var p_p_gurantee = $("#gurantee").val();
        var p_p_warranty = $("#warantee").val();
        if(warranty_data_status==0){
          p_p_warranty = '';
        }else{
          if (p_p_warranty == '') {
              error = 'img';
              $("#warantee_err").html('Warranty Image should not be empty');
              $("#warantee_err").show();
          }
        }
        if(guarantee_data_status==0){
          p_p_gurantee = '';
        }else{
          if (p_p_gurantee == '') {
              error = 'img';
              $("#gurantee_err").html('Gurantee Image should not be empty');
              $("#gurantee_err").show();
          }
        }



        var p_p_length = $("#p_p_length").val();
        if (p_p_length == '') {
            error = 'name';
            $("#p_p_length_err").html('Length should not be empty');
            $("#p_p_length_err").show();
        }
        var p_p_breadth = $("#p_p_breadth").val();
        if (p_p_breadth == '') {
            error = 'name';
            $("#p_p_breadth_err").html('Breadth should not be empty');
            $("#p_p_breadth_err").show();
        }
        var p_p_height = $("#p_p_height").val();
        if (p_p_height == '') {
            error = 'name';
            $("#p_p_height_err").html('Height should not be empty');
            $("#p_p_height_err").show();
        }
        var p_p_weight = $("#p_p_weight").val();
        if (p_p_weight == '') {
            error = 'name';
            $("#p_p_weight_err").html('Weight should not be empty');
            $("#p_p_weight_err").show();
        }


        if (error != '') {
            return false;
        }

        $(".err").html('');
        $(".err").hide();

        $.post(appUrl + "postProduct", { id: userId, type: type, cat: cat, sub: sub, img: img, img1: img1, img2: img2, img3: img3, img4: img4, name: p_p_name, price: p_p_price, length :  p_p_length, weight : p_p_weight, height : p_p_height , breadth : p_p_breadth, unit: p_p_unit_type, unit_others: p_p_unit_others, gst: p_p_gst, supply_capacity: p_p_supply_capacity, color: p_p_color, variety: p_p_variety, material_type: p_p_material_type, description: p_p_description, moq: p_p_moq, returns: p_p_returns, sample: p_p_sample, gurantee: p_p_gurantee, warranty: p_p_warranty })
            .done(function(data) {
                if (data.success == 1) {
                    alert(data.message);
                    setTimeout(
                        function() {
                            //location.reload();
                        }, 1000);
                } else {
                    alert(data.message);
                }
            });
    });

    $(".formCompany").hide();
    $(".formIndividual").show();
    $('input[type=radio][name=p_p_type]').change(function() {
        if (this.value == 1) {
            $(".formCompany").hide();
            $(".formIndividual").show();
        } else {
            $(".formCompany").show();
            $(".formIndividual").hide();

        }
    });

    //postServiceDetailIndividual
    $(document).on("click", "#postServiceDetailIndividual", function() {
        $(".err").html('');
        var cat = $("#p_si_type option:selected").val();
        var p_si_name = $("#p_si_name").val();
        var error = '';
        if (p_si_name == '') {
            error = 'name1';
            $("#p_si_name_err").html('Name should not be empty');
            $("#p_si_name_err").show();
        }

        var p_si_address = $("#p_si_address").val();
        if (p_si_address == '') {
            error = 'name2';
            $("#p_si_address_err").html('Address should not be empty');
            $("#p_si_address_err").show();
        }

        var p_si_cert = $("#p_si_cert").val();
        if (p_si_cert == '') {
            error = 'name2';
            $("#p_si_cert_err").html('Certificate should not be empty');
            $("#p_si_cert_err").show();
        }

        var p_si_machine = $("#p_si_machine option:selected").val();


        console.log(error);
        if (error != '') {
            return false;
        }

        $(".err").html('');
        $(".err").hide();

        $.post(appUrl + "postServiceDetail", { id: userId, c_type: 1, cat: cat, name: p_si_name, address: p_si_address, certificate: p_si_cert, machine: p_si_machine })
            .done(function(data) {
                console.log(data);
                if (data.success == 1) {
                    alert(data.message);
                    setTimeout(
                        function() {
                            location.reload();
                        }, 1000);
                } else {
                    alert(data.message);
                }
            });
    });

    //postServiceDetailCompany
    $(document).on("click", "#postServiceDetailCompany", function() {
        $(".err").html('');
        var cat = $("#p_s_type option:selected").val();
        var name = $("#p_s_name").val();
        var error = '';
        if (name == '') {
            error = 'name1';
            $("#p_s_name_err").html('Name should not be empty');
            $("#p_s_name_err").show();
        }

        var gstin = $("#p_s_gstin").val();
        if (gstin == '') {
            error = 'name1';
            $("#p_s_gstin_err").html('GSTIN should not be empty');
            $("#p_s_gstin_err").show();
        }

        var address = $("#p_s_address").val();
        if (address == '') {
            error = 'name1';
            $("#p_s_address_err").html('Address should not be empty');
            $("#p_s_address_err").show();
        }

        var country = $("#p_s_country option:selected").val();
        var state = $("#p_s_state option:selected").val();
        var city = $("#p_s_city option:selected").val();
        var service = $("#p_s_service_city option:selected").val();

        var b_address = $("#p_s_b_address").val();
        if (b_address == '') {
            error = 'name1';
            $("#p_s_b_address_err").html('Business Address should not be empty');
            $("#p_s_b_address_err").show();
        }

        var certificate = $("#p_s_cert").val();
        if (certificate == '') {
            error = 'name1';
            $("#p_s_cert_err").html('Certificate should not be empty');
            $("#p_s_cert_err").show();
        }

        var tan = $("#p_s_tan").val();
        if (tan == '') {
            error = 'name1';
            $("#p_s_tan_err").html('TAN Number should not be empty');
            $("#p_s_tan_err").show();
        }

        var pan = $("#p_s_pan").val();
        if (pan == '') {
            error = 'name1';
            $("#p_s_pan_err").html('PAN Number should not be empty');
            $("#p_s_pan_err").show();
        }

        var aadhar = $("#p_s_aadhar").val();
        if (aadhar == '') {
            error = 'name1';
            $("#p_s_aadhar_err").html('Aadhar Number should not be empty');
            $("#p_s_aadhar_err").show();
        }

        console.log(error);
        if (error != '') {
            return false;
        }

        $(".err").html('');
        $(".err").hide();

        $.post(appUrl + "postServiceDetail", { id: userId, c_type: 2, cat: cat, gstin: gstin, name: name, address: address, country: country, state: state, city: city, service: service, b_address: b_address, certificate: certificate, tan: tan, pan: pan, aadhar: aadhar })
            .done(function(data) {
                console.log(data);
                if (data.success == 1) {
                    alert(data.message);
                    setTimeout(
                        function() {
                            location.reload();
                        }, 1000);
                } else {
                    alert(data.message);
                }
            });
    });

    //postService
    $(document).on("click", "#postService", function() {
        $(".err").html('');
        var error = '';
        var s_name = $("#s_name").val();
        if (s_name == '') {
            error = 'name1';
            $("#s_name_err").html('Name should not be empty');
            $("#s_name_err").show();
        }

        var s_desc = $("#s_desc").val();
        if (s_desc == '') {
            error = 'name1';
            $("#s_desc_err").html('Description should not be empty');
            $("#s_desc_err").show();
        }

        var s_hour = $("#s_hour").val();
        if (s_hour == '') {
            error = 'name1';
            $("#s_hour_err").html('Hour should not be empty');
            $("#s_hour_err").show();
        }

        var s_day = $("#s_day").val();
        if (s_day == '') {
            error = 'name1';
            $("#s_day_err").html('Day should not be empty');
            $("#s_day_err").show();
        }

        var s_week = $("#s_week").val();
        if (s_week == '') {
            error = 'name1';
            $("#s_week_err").html('Week should not be empty');
            $("#s_week_err").show();
        }

        var s_month = $("#s_month").val();
        if (s_month == '') {
            error = 'name1';
            $("#s_month_err").html('Month should not be empty');
            $("#s_month_err").show();
        }

        var s_year = $("#s_year").val();
        if (s_year == '') {
            error = 'name1';
            $("#s_year_err").html('Year should not be empty');
            $("#s_year_err").show();
        }



        console.log(error);
        if (error != '') {
            return false;
        }

        $(".err").html('');
        $(".err").hide();

        $.post(appUrl + "postService", { id: userId, name: s_name, description: s_desc, hour: s_hour, day: s_day, week: s_week, month: s_month, year: s_year })
            .done(function(data) {
                console.log(data);
                if (data.success == 1) {
                    alert(data.message);
                } else {
                    alert(data.message);
                }
            });
    });

    //postService
    $(document).on("click", "#updateService", function() {
        $(".err").html('');
        var error = '';
        var s_name = $("#s_name").val();
        if (s_name == '') {
            error = 'name1';
            $("#s_name_err").html('Name should not be empty');
            $("#s_name_err").show();
        }

        var s_desc = $("#s_desc").val();
        if (s_desc == '') {
            error = 'name1';
            $("#s_desc_err").html('Description should not be empty');
            $("#s_desc_err").show();
        }

        var s_hour = $("#s_hour").val();
        if (s_hour == '') {
            error = 'name1';
            $("#s_hour_err").html('Hour should not be empty');
            $("#s_hour_err").show();
        }

        var s_day = $("#s_day").val();
        if (s_day == '') {
            error = 'name1';
            $("#s_day_err").html('Day should not be empty');
            $("#s_day_err").show();
        }

        var s_week = $("#s_week").val();
        if (s_week == '') {
            error = 'name1';
            $("#s_week_err").html('Week should not be empty');
            $("#s_week_err").show();
        }

        var s_month = $("#s_month").val();
        if (s_month == '') {
            error = 'name1';
            $("#s_month_err").html('Month should not be empty');
            $("#s_month_err").show();
        }

        var s_year = $("#s_year").val();
        if (s_year == '') {
            error = 'name1';
            $("#s_year_err").html('Year should not be empty');
            $("#s_year_err").show();
        }



        console.log(error);
        if (error != '') {
            return false;
        }

        $(".err").html('');
        $(".err").hide();
        var sid = $("#sids").val();
        $.post(appUrl + "updateService", { id: userId, sid: sid, name: s_name, description: s_desc, hour: s_hour, day: s_day, week: s_week, month: s_month, year: s_year })
            .done(function(data) {
                console.log(data);
                if (data.success == 1) {
                    alert(data.message);
                } else {
                    alert(data.message);
                }
            });
    });

    $(document).on("click", "#saveBusiness", function() {
        var error = '';
        $(".error").html('');
        //var b_type = $('input[name=b_type]:checked').val();
        var name = $("#b_name").val();
        if (name == '') {
            $("#b_name_error").html('Business name should not be empty');
            var error = 'name';
        }
        var gstin = $("#b_gstin").val();
        if (gstin == '') {
            $("#b_gstin_error").html('GSTIN should not be empty');
            var error = 'name';
        }
        var address = $("#b_address").val();
        if (address == '') {
            $("#b_address_error").html('Address should not be empty');
            var error = 'name';
        }
        var lat = $("#b_lat").val();
        if (lat == '') {
            $("#b_address_error").html('Please use Google Autocomplete');
            var error = 'lat';
        }
        var lng = $("#b_lng").val();
        if (lng == '') {
            $("#b_address_error").html('Please use Google Autocomplete');
            var error = 'lng';
        }
        var pan_no = $("#b_pan_no").val();
        if (pan_no == '') {
            $("#b_pan_no_error").html('PAN Number should not be empty');
            var error = 'name';
        }
        var tan_no = $("#b_tan_no").val();
        if (tan_no == '') {
            $("#b_tan_no_error").html('TAN Number should not be empty');
            var error = 'name';
        }
        var aadharUrl = $("#aadharUrl").val();
        if (aadharUrl == '') {
            $("#aadharUrlHTML").html('Aadhar Image should not be empty');
            var error = 'name';
        }
        var panUrl = $("#panUrl").val();
        if (panUrl == '') {
            $("#panUrlHTML").html('PAN Image should not be empty');
            var error = 'name';
        }
        var digUrl = $("#digUrl").val();
        if (digUrl == '') {
            $("#digUrlHTML").html('Digital Signature Image should not be empty');
            var error = 'name';
        }
        if (error != '') {
            return;
        } else {
            $(".error").html('');
        }
        $.post(appUrl + "updateBusinessDetails", { _token: _token, id: userId, name: name, gstin: gstin, address: address, lat : lat, lng : lng, pan_no: pan_no, tan_no: tan_no })
            .done(function(data) {
                if (data.success == 1) {
                    alert('Success', data.message);
                    setTimeout(
                        function() {
                            location.reload();
                        }, 1000);
                } else {
                    alert('Error', data.message);
                }
            });
    });

    $(document).on("click", "#saveBank", function() {
        var error = '';
        $(".error").html('');
        //var b_type = $('input[name=b_type]:checked').val();
        var name = $("#c_name").val();
        if (name == '') {
            $("#c_name_error").html('Account holder name should not be empty');
            var error = 'name';
        }
        var number = $("#c_no").val();
        if (number == '') {
            $("#c_no_error").html('Account Number should not be empty');
            var error = 'name';
        }
        var bank_name = $("#c_b_name").val();
        if (bank_name == '') {
            $("#c_b_name_error").html('Bank Name should not be empty');
            var error = 'name';
        }
        var ifsc = $("#c_ifsc").val();
        if (ifsc == '') {
            $("#c_ifsc_error").html('IFSC Number should not be empty');
            var error = 'name';
        }
        var p_pan = $("#c_p_pan").val();
        if (p_pan == '') {
            $("#c_p_pan_error").html('Personal Pan Number should not be empty');
            var error = 'name';
        }
        var address = $("#c_address").val();
        if (address == '') {
            $("#c_address_error").html('Address should not be empty');
            var error = 'name';
        }
        var ppanUrl = $("#ppanUrl").val();
        if (ppanUrl == '') {
            $("#ppanUrlHTML").html('Personal Pan Card Image should not be empty');
            var error = 'name';
        }
        var addrUrl = $("#addrUrl").val();
        if (addrUrl == '') {
            $("#addrUrlHTML").html('Address Proof Image should not be empty');
            var error = 'name';
        }
        var ccUrl = $("#ccUrl").val();
        if (ccUrl == '') {
            $("#ccUrlHTML").html('Cancelled Cheque Image should not be empty');
            var error = 'name';
        }

        var type = $("#c_btype option:selected").val();

        if (error != '') {
            return;
        } else {
            $(".error").html('');
        }
        $.post(appUrl + "updateBankDetails", { _token: _token, id: userId, name: name, number: number, bank_name: bank_name, ifsc: ifsc, p_pan: p_pan, address: address, type: type })
            .done(function(data) {
                if (data.success == 1) {
                    alert('Success', data.message);
                    setTimeout(
                        function() {
                            window.location = "post";
                        }, 2000);
                } else {
                    alert('Error', data.message);
                }
            });
    });


    //Workman page


    $(document).on("click", "#addWorkMan", function() {
        $(".error").html('');
        $(".err").html('');
        var name = $("#workman_name").val();
        var aadhar = $("#weImgTxt").val();
        var license = $("#weImg_1_Txt").val();
        var err = '';
        if (name == '') {
            err = 'name';
            $("#workman_name_error").html('Workman name should not be empty');
        }
        if (aadhar == '') {
            err = 'aadhar';
            $("#we_error").html("Aadhar Image should not be empty")
        }
        if (license == '') {
            err = 'aadhar';
            $("#we_1_error").html("License Image should not be empty")
        }
        if (err != '') {
            return;
        } else {
            $.post(appUrl + "addWorkMan", { _token: _token, id: userId, name: name, aadhar: aadhar, license: license })
                .done(function(data) {
                    if (data.success == 1) {
                        alert(data.message);
                        setTimeout(
                            function() {
                                location.reload();
                            }, 1000);
                    } else {
                        console.log(data.data);
                    }
                });
        }
    });

    $(document).on("click", ".edit_workman", function() {
        var id = this.id;
        $("#updateId").val(id);
        var name = $("#" + id + "_name").val();
        $("#edit_workman_name").val(name);
        var img = $("#" + id + "_aadhar").val();
        $(".edit_weImg").attr('src', img);
        var lic = $("#" + id + "_license").val();
        $("#edit_weImgTxt").val(img);
        $("#edit_weImg_1_Txt").val(lic);
        console.log(lic);
        $(".edit_weImg_1").attr('src', lic);

    });
    $(document).on("click", "#updateWorkMan", function() {
        var id = $("#updateId").val();
        $(".error").html('');
        $(".err").html('');
        var name = $("#edit_workman_name").val();
        var aadhar = $("#edit_weImgTxt").val();
        var license = $("#edit_weImg_1_Txt").val();
        var err = '';
        if (name == '') {
            err = 'name';
            $("#workman_name_error").html('Workman name should not be empty');
        }
        if (aadhar == '') {
            err = 'aadhar';
            $("#edit_we_error").html("Aadhar Image should not be empty")
        }
        if (license == '') {
            err = 'aadhar';
            $("#edit_we_1_error").html("License Image should not be empty")
        }
        if (err != '') {
            return;
        } else {
            $.post(appUrl + "updateWorkMan", { _token: _token, id: id, name: name, aadhar: aadhar, license: license })
                .done(function(data) {
                    if (data.success == 1) {
                        alert(data.message);
                        setTimeout(
                            function() {
                                location.reload();
                            }, 1000);
                    } else {
                        console.log(data.data);
                    }
                });
        }

    });

    $("#passwordSection").removeClass();
    $("#passwordSection").addClass('col-md-12');
    $("#passwordSection").hide();

    $(document).on("click", "#showPasswordScreen", function() {
        $("#profileSection").toggle('slow');
        $("#passwordSection").toggle('slow');
    });


    $(document).on("click", "#updatePassword", function() {
        $('.error').html('');
        var old = $("#oldPassword").val();
        var newP = $("#newPassword").val();
        var confirmP = $("#confirmPassword").val();
        var err = ''
        if (old == '') {
            err = 'old';
            $("#oldPassword_error").html('Old password should not be empty')
        }
        if (newP == '') {
            err = 'old';
            $("#newPassword_error").html('New password should not be empty')
        }
        if (confirmP == '') {
            err = 'old';
            $("#confirmPassword_error").html('Confirm Password should not be empty')
        }

        if (newP == confirmP) {

        } else {
            err = 'old';
            $("#confirmPassword_error").html('New Password and Confirm Password are not same')
        }
        if (err != '') {
            return;
        } else {
            $('.error').html('');
            $.post(appUrl + "updatePassword", { _token: _token, new_password: newP, old_password: old, id: userId })
                .done(function(data) {
                    if (data.success == 1) {
                        alert(data.message);
                        setTimeout(
                            function() {
                                location.reload();
                            }, 1000);
                    } else {
                        alert(data.message);
                    }
                });
        }
    });

    $(document).on("click", ".viewOrderDetail", function() {
        $('.orderDetailLoad').show();
        $('.orderDetailBody').hide();
        var id = this.id;
        $.post(appUrl + "viewOrderDetail", { _token: _token, id: userId, product_id: id })
            .done(function(data) {
                if (data.success == 1) {
                    $('.orderDetailLoad').hide();
                    $('.orderDetailBody').show();
                    $('.o_id').html(data.order.id)
                    $('.o_date').html(data.order.created_at)
                    $('.o_ref').html(data.order.razor_key)
                        //$("#order_table > tbody").html('');
                    $("#oBody").empty();
                    //$("#order_table tbody").remove();
                    //$("#order_table tbody").append('<tbody>');
                    var count = 0;
                    $.each(data.order_detail, function(key, value) {
                        count++;
                        //$('')
                        $('#order_table tbody').append('<tr><td>' + count + '</td><td>' + value.product_name + '</td><td>Rs.' + value.price_individual + '</td><td>' + value.count + '</td><td>Rs.' + value.price_total + '</td></tr>');
                    });
                    //$("#order_table tbody").append('</tbody>');
                    $('#order_table tbody').append('<tr><td></td><td></td><td></td><td><b>Total:</b></td><td><b>Rs.' + data.order.price + '</b></td></tr>');
                } else {
                    alert(data.message);
                }
            });
    });


});
