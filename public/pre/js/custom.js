$( document ).ready(function() {

     var url = window.location.href; // Returns full URL
    if (url.indexOf("localhost") > -1) {
        var mode = 'local';
    } else {
        var mode = 'live';
    }
    if (mode == 'local') {
        var sUrl = 'http://localhost/project/7A/sevenatara/';
        localStorage.setItem("sUrl", sUrl);
    } else {
        var sUrl = 'https://sevenatara.com/';
        localStorage.setItem("sUrl", sUrl);
    }

    $("#otp").hide();
    $("#otp_done").html('');
    var otpNo = '';
    var refNo = '';

    $(".oldfrm").show();

    $("#newfrm").removeClass();
    $("#newfrm").addClass('book-agileinfo-form');
    $("#newfrm").hide();
    

    $( "#getOTP" ).click(function() {
        var phone = $("#phone").val();
        if(phone==""){
            error='name'
            $("#phone_error").html('Mobile Number is required');            
        }else{
            if(phone.length==10){
                $("#phone_error").html('');
                $("#otp").show();
                $("#getOTP").hide();
                
                $.post( sUrl+"getOTP_P", { phone : phone })
                    .done(function( data ) {
                        console.log(data);
                        if(data.success==0){
                            $("#phone_error").html(data.message);
                            $("#otp").val('');
                            $("#otp").hide();
                            $("#getOTP").show();
                        }else{
                            localStorage.setItem('otp',data.otp);
                            setTimeout(function() {
                                $("#otp_done").html("");
                            }, 2000);
                            $("#otp_error").html("");
                            $("#otp_done").html('OTP Sent success');
                            $('#phone').attr("disabled","disabled") 
                        }
                });

            }else{
                $("#phone_error").html('Phone Number should be 10 number');            
            }
            
        }
    });
    $( ".submitForm" ).click(function() {
        
        $("#otp_done").html('');

        var first_name = $("#first_name").val();
        var last_name = $("#last_name").val();
        var email = $("#email").val();
        var phone = $("#phone").val();
        var otp = $("#otp").val();
        var password = $("#password").val();
        var confirm_password = $("#confirm_password").val();
        var referal_code = $("#referal_code").val();
        var error = "";
        $(".error").html("");
        if(first_name==''){
            error='first_name'
            $("#first_name_error").html('Enter First name');
        }
        if(last_name==''){
            error='last_name'
            $("#last_name_error").html('Enter Last name');
        }
        if(email==''){
            error='email'
            $("#email_error").html('Enter Email');
        }
        if(phone==''){
            error='phone'
            $("#phone_error").html('Enter Mobile number');
        }
        if(otp==''){
            error='otp'
            $("#otp_error").html('Enter OTP');
        }
        if(password==''){
            error='password'
            $("#password_error").html('Enter Password');
        }
        if(password!=confirm_password){
            error='password_confirm_password'
            $("#confirm_password_error").html('Password and Confirm Password should be same');
        }
        if(confirm_password==''){
            error='confirm_password'
            $("#confirm_password_error").html('Enter Confirm password');
        }
        if(error==''){
        $(".error").html("");
        var otpNos = localStorage.getItem('otp');
        console.log('checking '+otp+' with '+otpNos);
        if(otp == otpNos){
            $.post( sUrl+"preRegister", { first_name : first_name, last_name : last_name, email : email, phone : phone, password : password,referal_code: referal_code })
                .done(function( data ) {
                    console.log(data);
                    if(data.success==0){
                        if(data.error.email[0]){
                            $("#email_error").html('Email already exists');
                        }
                    }else if(data.success==2){
                        $("#referal_code_error").html('Invalid Referal Code');
                    }
                    else{
                        
                        localStorage.setItem('us_id',data.id);

                        $(".oldfrm").hide();
                        $("#howwework").trigger( "click" );
                        $("#understand").show();
                        //$(".newfrm").show();
                        $(".code").html('Referal Code : '+data.code);
                        $(".codes").html('<span style="color:white!important;" class="cp">Coupon Code</span> : <span class="cps" style="color:#FFD402 !important">'+data.codes+'</span>');
                        $(".codes").hide();
                        $("#myInput").val(data.code);
                        $(".topMsg").html("Pre-Registered Successfully!");
                        $('.soc-msg').html('Share your Referal Code');
                        $("#fb-link").attr("href", "https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fwww.sevenatara.com%2Freferal%2F"+data.code+"%2F&hashtag=%23sevenatara");
                        $("#tw-link").attr("href", "https://twitter.com/home?status=Pre%20register%20at%20Seven%20Atara%0Ahttp%3A//www.sevenatara.com/referal/"+data.code);
                        $("#tw-link").attr("href", "https://twitter.com/home?status=Pre%20register%20at%20Seven%20Atara%0Ahttp%3A//www.sevenatara.com/referal/"+data.code);
                        $("#li-link").attr("href", "https://www.linkedin.com/shareArticle?mini=true&url=http%3A//sevenatara.com/referal/"+data.code+"&title=Seven%20Atara%20Pre%20Registration&summary=&source=");
                        $("#wa-link").attr("href", "whatsapp://send?text=Pre Register at Seven Atara http://www.sevenatara.com/referal/"+data.code);

                        $("#fb-link").removeClass();
                        $("#tw-link").removeClass();
                        $("#li-link").removeClass();
                        $("#wa-link").removeClass();

                        $("#fb-link").addClass("fb-link shr");
                        $("#tw-link").addClass("tw-link shr");
                        $("#li-link").addClass("li-link shr");
                        $("#wa-link").addClass("wa-link shr");


                        $(".book-appointments").hide();
                        $(".abt-us").hide();
                        $(".book-appointment").hide();
                        $(".soc-msg").html('Claim your Coupon code by Sharing');

                        // $( ".left-agileits-w3layouts-img" ).clone().appendTo( ".main-agile-sectns" );
                        // $( ".left-agileits-w3layouts-img" ).hide();


                    }
            });
        }else{
            $("#otp_error").html('Invalid OTP');
        }
        }else{
            console.log('Error ',error);
            return false;
        }

    });
    $("#understand").hide();
    $(".topTxt").hide();
    

    $( "#understand" ).click(function() {
        $("#understand").hide();
        $("#newfrm").show();
        $(".topTxt").show();
    });

    


$( ".copyCode" ).click(function() {
    //function copyCode(){
        var copyText = document.getElementById("myInput");
        //copyText = refNo;
        copyText.select();
        document.execCommand("Copy");
        alert("Referal code copied !");
    });

    $(document).on("click",".shr",function() {
        $(".topTxt").hide();
        $(".codes").show();
        $(".code").show();
         var id = localStorage.getItem('us_id');
        $.get( sUrl+"sendMail/"+id)
            .done(function( datas ) {
        });


        $(".book-appointments").show();
        $(".abt-us").show();
        $(".book-appointment").show();

        $(".soc-msg").html('')
        
    });


$(".initModal").trigger( "click" );




var url = $(location).attr('href'),
parts = url.split("/"),
page = parts[parts.length-2];

if(page=='referal'){
code = parts[parts.length-1];
$.get( sUrl+"verify/"+code)
    .done(function( datas ) {
        if(datas.success==0){
            window.location.replace(sUrl);
        }else{
            window.location.replace(sUrl+"#"+code);
        }
});

}

if(window.location.hash) {
  var hash = window.location.hash;
  hash = hash.substring(1);
  $("#referal_code").val(hash);

}

});