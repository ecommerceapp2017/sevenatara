<?php
namespace App;
use Auth;
use App\Order;
use App\OrderDetail;
?>
@extends('layout.public')
@section('content')
<?php
if(Auth::check()){

}else{
$url = $_SERVER['HTTP_HOST'];
if($_SERVER['HTTP_HOST']=='localhost'){
    $path = 'applocal';
}else{
    $path = 'applive';
}
$url = Config::get('CONST.'.$path.'.url');
header('Location: '.$url);
exit;
}
?>

                    <div class="col-sm-9">
                        <div class="row">
                            <div class="col-sm-12 text-left">
                            <h1>My Orders</h1>
                            <div class="table-responsive">    
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th>Order Id</th>
                                                <th>Payment Referenc Code</th>
                                                <th>Total</th>
                                                <th>Date</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php $i = 0; ?>
                                        @foreach($data as $item)
                                        <?php $i++ ?>
                                        <tr>
                                            <td>{{$i}}</td>
                                            <td>#{{$item->id}}</td>
                                            <td>{{$item->razor_key}}</td>
                                            <td>Rs.{{$item->price}}</td>
                                            <td>{{$item->created_at}}</td>
                                            <td><span class="label label-primary">Order placed</span></td>
                                            <td><button class='btn btn-primary viewOrderDetail' data-toggle="modal" data-target="#myModal" id='{{$item->id}}'>View details</button></td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                            </div>
                    </div><!-- end col -->
                </div><!-- end row -->                
            </div><!-- end container -->
        </section>
        <!-- end section -->



<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Order Details</h4>
      </div>
      <div class="modal-body">
        <p class='orderDetailLoad'>Loading Order Details...</p>
        <p class='orderDetailBody'>
            <div class='row'>
                <div class='col-md-12'><strong>
                    <p class='pull-left' class='o_id'>Order Id : #123</p>
                    <p class='pull-right' class='o_date'>Order Date : 12-09-1989<br>Order Payment Ref Id : <span class='o_ref'></span></p></strong>
                </div>
            </div>
                    <div class="table-responsive" id='order_table'>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>S.No</th>
                                <th>Name</th>
                                <th>Price</th>
                                <th>Quantity</th>
                                <th>Sub Total</th>
                            </tr>
                        </thead>
                        <!--<tbody>
                            <tr>
                                <td>1</td>
                                <td>Board</td>
                                <td>12</td>
                                <td>2</td>
                                <td>24</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td><b>Total : </b></td>
                                <td><b>24</b></td>
                            </tr>
                        </tbody>-->
                    </table>
                    </div>
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<script src="{{ asset('/').('public/assets/js/custom/user.js') }}"></script>
@stop