<!DOCTYPE HTML>
<html>

<head>
	<title>Seven Atara | Pre Registration</title>
	<!-- Meta tags -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="Seven Atara Pre Registration"/>
  <meta property="og:type"          content="website" />
  <meta property="og:title"         content="Seven Atara" />
  <meta property="og:description"   content="Seven Atara - Description" />
    <meta property="og:image"         content="http://sevenatara.com/public/assets/img/7a-logo.png" />
	<script type="application/x-javascript">
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<!-- //Meta tags -->
	<!-- Stylesheet -->
    <link href="{{ asset('/').('public/pre/css/style.css') }}" type="text/css" rel="stylesheet"/>
	<!-- //Stylesheet -->
	<!--fonts-->
	<link href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
	<link href="//fonts.googleapis.com/css?family=Montserrat+Alternates:200,400,500,600,700" rel="stylesheet">
	<!--//fonts-->
</head>

<body>
	<!--background-->
	<h1 class="hide"><img src="public/assets/img/7a-logo.png" class="img-rounded logo"> <span class="logo-txt">Seven Atara</span><span class='txt-white'> Pre Registration</span></h1>
	<img src="public/pre/images/dbb.png" class="img-rounded logo" style="    text-align: center;margin: auto;border-radius:0px;width: 100% !important;height: 160px !important;">
	<div class="bg-agile">
		
		<div class="book-appointment">
		  <?php
			if($data!='invalid'){
			?>
			<h2 class='topMsg'>Personal Details</h2>
			<div class="book-agileinfo-form oldfrm">
				<form action="#" method="post">
					<div class="main-agile-sectns">
						<div class="agileits-btm-spc form-text1">
							<input type="text" name="Name" id="first_name" placeholder="First Name"  minlength="3" maxlength="20">
							<span class="error" id="first_name_error"></span>
						</div>
						<div class="agileits-btm-spc form-text2">
							<input type="text" name="Phone no" id="last_name" placeholder="Last Name" minlength="3" maxlength="20">
							<span class="error" id="last_name_error"></span>
						</div>
					</div>
					<div class="main-agile-sectns">
						<div class="agileits-btm-spc form-text1">
							<input type="email" name="email" id="email" placeholder="Email Id" minlength="3" maxlength="30">
						<span class="error" id="email_error"></span>
						</div>
						<div class="agileits-btm-spc form-text2">
							<input type="text" name="Phone no" id="referal_code" placeholder="Referal Code" value="{{$data}}" minlength="6" maxlength="6">
							<span class="error" id="referal_code_error"></span>
						</div>
					</div>
					<div class="main-agile-sectns">
						<div class="agileits-btm-spc form-text1">
							<input name="Text" id="phone" type="tel" placeholder="Mobile Number" max="9999999999" minlength="10" maxlength="10">
							<span class="error" id="phone_error"></span>
						</div>
						<div class="agileits-btm-spc form-text2">
							<button type="button" class="btn btn-primary yellow btn-lg" id="getOTP">Get OTP</button>
							<input type="tel" id="otp" name="Time" placeholder="Enter OTP" max="999999" minlength="6" maxlength="6">
							<br><span class="error" id="otp_error"></span>
							<br><span class="success" id="otp_done"></span>
						</div>
					</div>
					<div class="main-agile-sectns">
						<div class="agileits-btm-spc form-text1">
							<input type="password" name="Pick-up Location" id="password" placeholder="Password"   minlength="6" maxlength="20">
							<span class="error" id="password_error"></span>
						</div>
						<div class="agileits-btm-spc form-text2">
							<input type="password" name="Drop-off Location" id="confirm_password" placeholder="Confirm password"  minlength="6" maxlength="20">
							<span class="error" id="confirm_password_error"></span>
						</div>
					</div>
					<input type="button" value="Submit" class="submitForm">
					<input type="reset" value="Reset">
					<div class="clear"></div>
				</form>
			</div>
			<div class="book-agileinfo-form hide" id="newfrm">
				<form action="#" method="post">				
					<div class="main-agile-sectns">
						<div class="agileits-btm-spc form-text2">
							<h3 class='code'></h3>
							<h3 class='codes'></h3>
							<input type="hidden" id="myInput">
						</div>
						<h3 style="color: #FFC107;">Use the share links on right side to Share your Referal Code </h3>
					</div>
					<div class="clear"></div>
				</form>
			</div>
			<?php
			}else{
			?>
			<div class="book-agileinfo-form">
				<form action="#" method="post">				
					<div class="main-agile-sectns">
						<div class="agileits-btm-spc form-text2">
							<h3 class='code'>Invalid Referal Code</h3>
							
						</div>
					</div>
					<div class="clear"></div>
				</form>
			</div>
			<?php
			}
			?>

		</div>

		<div class="left-agileits-w3layouts-img">
			<h3>Share via Social</h3>
			<ul>
				<li> 
					<a class="fb-link" href="https://www.facebook.com/7atara" target="_blank">
						<img src="{{ asset('/').('public/pre/images/icon/png/fb.png') }}" class="img-rounded social">
					</a>
					<a class="tw-link" href="https://twitter.com/seven_atara" target="_blank">
					 <img src="{{ asset('/').('public/pre/images/icon/png/twitter.png') }}" class="img-rounded social">
					</a>
				</li>
				<li> 
					<a class="li-link" href="https://www.linkedin.com/company/seven-atara/" target="_blank">
						<img src="{{ asset('/').('public/pre/images/icon/png/linkedin.png') }}" class="img-rounded social">
					</a>
					<a class="wa-link" href="https://www.instagram.com/sevenatara/" target="_blank">
						<img src="{{ asset('/').('public/pre/images/icon/png/whatsapp.png') }}" class="img-rounded social">
					</a>
					<br>
						<a class="wa-link" href="https://www.instagram.com/sevenatara/" target="_blank">
						<img src="{{ asset('/').('public/pre/images/icon/png/insta.png') }}" class="img-rounded social">
					</a>
					 </li>
			</ul>
		</div>
	</div>
	<br>
	<div class="ctr">
	<button type="button" class="btn btn-primary yellow btn-lg" data-toggle="modal" data-target="#aboutModal">ABOUT US</button>
	<button type="button" class="btn btn-primary yellow btn-lg" id='howwework' data-toggle="modal" data-target="#workModal">HOW WE WORK</button>
	<button type="button" class="btn btn-primary yellow btn-lg" data-toggle="modal" data-target="#contactModal">CONTACT US</button>
	</div>

	<!--copyright-->
	<!--//copyright-->
	<!-- Time -->
	<!--// Time -->
	<!-- Calendar -->
	
	<!-- //Calendar -->

	  <!-- Modal -->
  <div class="modal fade" id="aboutModal" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">
						<span class="txt-white">&times;</span>
						</button>
          <h4 class="modal-title">About Us</h4>
        </div>
        <div class="modal-body">
          
<h1 dir="ltr" style="text-align:center"><strong class="clr">CULTURE AND VALUES</strong></h1>

<p dir="ltr"><strong class="clr">7ATARA CULTURE IS ABOUT ADVOCATING TRADE AROUND THE WORLD.</strong></p>

<p dir="ltr">We inculcate an ecosystem where all people &ndash; consumers, merchants, third-party service providers and others &ndash; have an opportunity to flourish.</p>

<p dir="ltr">Our prosperity and progress is built on the spirit of entrepreneurship, innovation, and a staunch focus on meeting the needs of our customers.<br />
<br />
We believe that a strong sense of shared values enables us to maintain a common company culture and community, no matter how large we grow.</p>

<p dir="ltr"><strong class="clr">OUR FOCUS</strong></p>

<p dir="ltr">We build responsible and honest relationships with communication that pursue growth and learning.</p>

<p dir="ltr"><strong class="clr">OUR VALUES</strong></p>

<p dir="ltr">Passion is at the heart of our company and we move persistently to the core of innovation and to a better future. Our values include:</p>

<p dir="ltr"><strong class="clr">CUSTOMER &nbsp;</strong></p>

<p dir="ltr">Be our first priority. Committed in hearts and minds.</p>

<p dir="ltr"><strong class="clr">QUALITY</strong></p>

<p dir="ltr">What we do, we do well, and we strive to be the best.</p>

<p dir="ltr"><strong class="clr">EMBODIED CHANGES</strong></p>

<p dir="ltr">In this fast moving world, we must be consistent, innovative and ready to adapt to new business conditions in order to be persistent.</p>

<p dir="ltr"><strong class="clr">INTEGRITY</strong></p>

<p dir="ltr">We wish our people to be straight forward and open minded. Keep it simple!</p>

<p dir="ltr"><strong class="clr">PASSION</strong></p>

<p dir="ltr">We require our people to be bold and courageous through entrepreneurial risk to reach boundaries and experiments with consistent honesty and genuine towards the community and we believe in people, we never give up.</p>

<p dir="ltr"><strong class="clr">COMMITMENT</strong></p>

<p dir="ltr">We persuade our people to create a culture of warmth respect where everyone is welcome and determined with entrepreneurial spirit, cost consciousness which creates excellence.</p>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
	<!-- End Modal -->
	<!-- Modal -->
  <div class="modal fade" id="workModal" role="dialog" style="margin-top:-20px">
    <div class="modal-dialog modal-lg" style="width:98%">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">
						<span class="txt-white">&times;</span>
					</button>
          <h4 class="modal-title">How we work</h4>
        </div>
        <div class="modal-body">
          <img src="{{ asset('/').('public/pre/images/how-we-work.png') }}" style="height:750px;width:100%;" class="mid">
					<div style="text-align:center">
					<br>
					<button type="button" class="btn btn-success" id="understand" data-dismiss="modal">I understand</button>
					</div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
	<!-- End Modal -->
	<!-- Modal -->
  <div class="modal fade" id="contactModal" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">
						<span class="txt-white">&times;</span>
					</button>
          <h4 class="modal-title">Contact Us</h4>
        </div>
        <div class="modal-body">
          <b><p>
					<h3>Seven Atara</h3>
					Phone : 070106 09203<br>
					11-3/2, Street 3, Periyar Nagar, Koodal Nagar,<br>Madurai,<br>Tamil Nadu 625018<br>
					For Enquiry or Business please mail to 
						<a href="mailto:info@sevenatara.com?Subject=Hello" target="_top">info@sevenatara.com</a></p></b>
						<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3929.740301021703!2d78.097048!3d9.955552000000003!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xf662a7f366a198ad!2sSeven+Atara!5e0!3m2!1sen!2sus!4v1522216333620" width="500" height="450" frameborder="0" style="border:0;width:100%" allowfullscreen></iframe>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
	<!-- End Modal -->

	<button type="button" class="btn btn-primary yellow btn-lg initModal hide" data-toggle="modal" data-target="#initModal"></button>
	<div class="modal fade" id="initModal" role="dialog">
    <div class="modal-dialog modal-lg" style="width:90%">
      <!-- Modal content-->
      <div class="modal-content no-bg">
        <div class="modal-header no-bg">
          <button type="button" class="close" data-dismiss="modal" style="color: white;font-size: 40px;">&times;</button>
        </div>
        <div class="modal-body no-bg">
					<img src="{{ asset('/').('public/pre/images/adbanner.png') }}">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
	<!-- End Modal -->


<link href="{{ asset('/').('public/pre/css/bootstrap.min.css') }}" type="text/css" rel="stylesheet"/>
<script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
<script src="{{ asset('/').('public/pre/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('/').('public/pre/js/custom.js') }}"></script>


</body>

</html>