<!DOCTYPE HTML>
<html>

<head>
	<title>Seven Atara | Pre Registration</title>
	<!-- Meta tags -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="Money festival, intern from home, work from home, web develop, mobile app developer, web developer, full stack developer, fresher jobs, earn, earn easy,"/>
	<meta name="description" content="First ever one stop platform for goods and services. Refer and Earn!">
  <meta property="og:type"          content="website" />
  <meta property="og:title"         content="Seven Atara" />
  <meta property="og:description"   content="Seven Atara - First ever one stop platform for goods and services. Refer and Earn!" />
  <meta property="og:image"         content="http://sevenatara.com/public/assets/img/7a-logo.png" />
	<script type="application/x-javascript">
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<!-- //Meta tags -->
	<!-- Stylesheet -->
    <link href="{{ asset('/').('public/pre/css/style.css') }}" type="text/css" rel="stylesheet"/>
	<!-- //Stylesheet -->
	<!--fonts-->
	<link href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
	<link href="//fonts.googleapis.com/css?family=Montserrat+Alternates:200,400,500,600,700" rel="stylesheet">
	<!--//fonts-->
</head>

<body>
	<!--background-->
	<div class='ctr bg'>
		<div id="titles">
		<!--<img src="public/assets/img/7a-logo.png" class="img-rounded  logo">-->
		<img src="public/pre/images/new.png" class="img-rounded  logo">
    <div class='hide'>
			<h1 class="logo-txt txtclr">SEVEN ATARA</h1>
    		<h2 class="slogan txtclr"><i>Everything at Your EASE!</i></h2>
    </div>
		</div>

		<!--<span class="logo-txt clr">
			<span class='slogan'>Everything at Your EASE</span>
		</span>-->
	</div>
	<!--<h1 class="bg nomargin"><img src="public/assets/img/7a-logo.png" class="img-rounded  logo"> <span class="logo-txt">SEVEN ATARA</span><span class='txt-white'> PRE-REGISTRATION</span></h1>-->
	<!--<img src="public/pre/images/dbb.png" class="img-rounded logo" style="    text-align: center;margin: auto;border-radius:0px;width: 100% !important;height: 160px !important;">-->
	<div class="bg-agile">
		
		<div class="book-appointments">
		<div class="ctr">
		<button type="button" class="btn btn-primary yellow btn-lg s-btn" data-toggle="modal" data-target="#goodsModal">GOODS</button>
		<button type="button" class="btn btn-primary yellow btn-lg s-btn" data-toggle="modal" data-target="#serviceModal">SERVICES</button>
		<button type="button" class="btn btn-primary yellow btn-lg s-btn" data-toggle="modal" data-target="#aboutModal">ABOUT US</button><br>
		<button type="button" class="btn btn-primary yellow btn-lg s-btn" id='howwework' data-toggle="modal" data-target="#workModal">HOW WE WORK?</button><br>
		<button type="button" class="btn btn-primary yellow btn-lg s-btn" data-toggle="modal" data-target="#contactModal">CONTACT US</button>
		<button type="button" class="btn btn-primary yellow btn-lg s-btn" data-toggle="modal" data-target="#faqModal">FAQ</button><br>
		<button type="button" class="hide" data-toggle="modal" data-target="#ffModal">DD</button>
		
		</div>
		</div>

		<div class="book-appointment">
			<h2 class='topMsg'><b class="pre-txt">PRE-REGISTRATION</b></h2>
			<div class="book-agileinfo-form oldfrm">
				<form action="#" method="post">
					<div class="main-agile-sectns">
						<div class="agileits-btm-spc form-text1">
							<input type="text" name="Name" id="first_name" placeholder="First Name"  minlength="3" maxlength="20" autofocus>
							<span class="error" id="first_name_error"></span>
						</div>
						<div class="agileits-btm-spc form-text2">
							<input type="text" name="Phone no" id="last_name" placeholder="Last Name" minlength="3" maxlength="20">
							<span class="error" id="last_name_error"></span>
						</div>
					</div>
					<div class="main-agile-sectns">
						<div class="agileits-btm-spc form-text1">
							<input type="email" name="email" id="email" placeholder="Email Address" minlength="3" maxlength="30">
						<span class="error" id="email_error"></span>
						</div>
						<div class="agileits-btm-spc form-text2">
							<input type="text" name="Phone no" id="referal_code" placeholder="Referal Code (Optional)" minlength="6" maxlength="6">
							<span class="error" id="referal_code_error"></span>
						</div>
					</div>
					<div class="main-agile-sectns">
						<div class="agileits-btm-spc form-text1">
							<input name="Text" id="phone" type="tel" placeholder="Mobile Number" max="9999999999" minlength="10" maxlength="10">
							<span class="error" id="phone_error"></span>
						</div>
						<div class="agileits-btm-spc form-text2">
							<button type="button" class="btn btn-primary yellow btn-lg" id="getOTP">Get OTP</button>
							<input type="tel" id="otp" name="Time" placeholder="Enter OTP" max="999999" minlength="6" maxlength="6">
							<br><span class="error" id="otp_error"></span>
							<br><span class="success" id="otp_done"></span>
						</div>
					</div>
					<div class="main-agile-sectns">
						<div class="agileits-btm-spc form-text1">
							<input type="password" name="Pick-up Location" id="password" placeholder="Password"   minlength="6" maxlength="20">
							<span class="error" id="password_error"></span>
						</div>
						<div class="agileits-btm-spc form-text2">
							<input type="password" name="Drop-off Location" id="confirm_password" placeholder="Confirm password"  minlength="6" maxlength="20">
							<span class="error" id="confirm_password_error"></span>
						</div>
					</div>
					<button type="button" value="Submit" type="button" class="submitForm btn btn-primary btn-lg s-btn subbtn">Next</button>
					<br>
					<button type="reset" value="Submit" type="reset" class="btn btn-lg s-btn subbtn" style="background:white !important;color:black !important">
					<u>Reset</u>
					</button>
					
					<!--<button type="reset" value="Reset" class="btn btn-lg s-btn" style="background:white !important;color:black !important;text-align:center !important">
						<u>Reset</u>
					</button>-->
					<div class="clear"></div>
				</form>
			</div>
			<div class="book-agileinfo-form hide" id="newfrm">
				<form action="#" method="post">				
					<div class="main-agile-sectns">
						<div class="agileits-btm-spc form-text2">
							<h3 class='codes'></h3>
							<h3 class='code' style="width:200%"></h3>
<!-- start -->
<div style="width:200%;font-family: sans-serif;">
<h2 style="text-align:center;color:black"><strong><em>WHAT</em></strong> should I do with MY&nbsp;<strong><em>DISCOUNT COUPON CODE?</em></strong></h2>
<p style="text-align:center">You can <strong><em>GET DISCOUNTS</em></strong> on <strong><em>ANYTHING</em></strong> you <strong><em>BUY</em></strong> and/or on <strong><em>ANY SERVICES YOU AVAIL</em></strong>&nbsp;on <strong><em>SEVEN ATARA!</em></strong></p>
<p style="text-align:center">CONTACT US for clarity!</p>
</div>
<!-- end -->

							<input type="hidden" id="myInput">
						</div>
					</div>
					<div class="clear"></div>
				</form>
			</div>
		</div>
		
		<div class="left-agileits-w3layouts-img">
		<div class="topTxt">
		<h2 class="topMsgs" style="text-align:center">
		<b class="pre-txt">Refer via</b>
		</h2>
		</div>
			<ul class='ctr social-li' id='scl-li'>
				<li> 
					<a  id="fb-link" href="https://www.facebook.com/7atara" target="_blank" rel="noopener">
						<img src="{{ asset('/').('public/pre/images/icon/png/fb.png') }}" class="img-rounded social">
					</a>
					<a id="tw-link" href="https://twitter.com/seven_atara" target="_blank">
					 <img src="{{ asset('/').('public/pre/images/icon/png/twitter.png') }}" class="img-rounded social">
					</a>
					<a  id="li-link" href="https://www.linkedin.com/company/seven-atara/" target="_blank">
						<img src="{{ asset('/').('public/pre/images/icon/png/linkedin.png') }}" class="img-rounded social">
					</a>
					<a  id="wa-link" href="whatsapp://send?text=Pre Register at Seven Atara http://www.sevenatara.com" data-action="share/whatsapp/share">
						<img src="{{ asset('/').('public/pre/images/icon/png/whatsapp.png') }}" class="img-rounded social">
					</a>
						<a  id="in-link" href="https://www.instagram.com/sevenatara/" target="_blank">
						<img src="{{ asset('/').('public/pre/images/icon/png/insta.png') }}" class="img-rounded social">
					</a>
					
				</li>
				
			</ul>
			<div class="topTxt">
			<h2 class="topMsgs" style="text-align:center">
				<b class="pre-txt">to claim your discount coupon code</b>
			</h2>
			</div>
		</div>

	</div>
	<br>
	<div class="pull-left hide">
		<button type="button" class="btn btn-primary yellow btn-lg" data-toggle="modal" data-target="#aboutModal">ABOUT US</button>
		<button type="button" class="btn btn-primary yellow btn-lg" id='howwework' data-toggle="modal" data-target="#workModal">HOW WE WORK?</button>
		<button type="button" class="btn btn-primary yellow btn-lg" data-toggle="modal" data-target="#contactModal">CONTACT US</button>
	</div>
	<div class="pull-right">

	</div>

	<!--copyright-->
	<!--//copyright-->
	<!-- Time -->
	<!--// Time -->
	<!-- Calendar -->
	
	<!-- //Calendar -->

	  <!-- Modal -->
  <div class="modal fade" id="aboutModal" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">
						<span class="txt-white">&times;</span>
						</button>
          <h4 class="modal-title">About Us</h4>
        </div>
        <div class="modal-body">
<h1 dir="ltr" style="text-align:center"><strong class="clr">CULTURE AND VALUES</strong></h1>

<p dir="ltr"><strong class="clr">7ATARA CULTURE IS ABOUT ADVOCATING TRADE AROUND THE WORLD.</strong></p>

<p dir="ltr">We inculcate an ecosystem where all people &ndash; consumers, merchants, third-party service providers and others &ndash; have an opportunity to flourish.</p>

<p dir="ltr">Our prosperity and progress is built on the spirit of entrepreneurship, innovation, and a staunch focus on meeting the needs of our customers.<br />
<br />
We believe that a strong sense of shared values enables us to maintain a common company culture and community, no matter how large we grow.</p>

<p dir="ltr"><strong class="clr">OUR FOCUS</strong></p>

<p dir="ltr">We build responsible and honest relationships with communication that pursue growth and learning.</p>

<p dir="ltr"><strong class="clr">OUR VALUES</strong></p>

<p dir="ltr">Passion is at the heart of our company and we move persistently to the core of innovation and to a better future. Our values include:</p>

<p dir="ltr"><strong class="clr">CUSTOMER &nbsp;</strong></p>

<p dir="ltr">Be our first priority. Committed in hearts and minds.</p>

<p dir="ltr"><strong class="clr">QUALITY</strong></p>

<p dir="ltr">What we do, we do well, and we strive to be the best.</p>

<p dir="ltr"><strong class="clr">EMBODIED CHANGES</strong></p>

<p dir="ltr">In this fast moving world, we must be consistent, innovative and ready to adapt to new business conditions in order to be persistent.</p>

<p dir="ltr"><strong class="clr">INTEGRITY</strong></p>

<p dir="ltr">We wish our people to be straight forward and open minded. Keep it simple!</p>

<p dir="ltr"><strong class="clr">PASSION</strong></p>

<p dir="ltr">We require our people to be bold and courageous through entrepreneurial risk to reach boundaries and experiments with consistent honesty and genuine towards the community and we believe in people, we never give up.</p>

<p dir="ltr"><strong class="clr">COMMITMENT</strong></p>

<p dir="ltr">We persuade our people to create a culture of warmth respect where everyone is welcome and determined with entrepreneurial spirit, cost consciousness which creates excellence.</p>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
	<!-- End Modal -->
	<!-- Modal -->
  <div class="modal fade" id="workModal" role="dialog" style="margin-top:-20px">
    <div class="modal-dialog modal-lg" style="width:98%">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">
						<span class="txt-white">&times;</span>
					</button>
          <h4 class="modal-title hw-txt">Step - 2 out of 2</h4>
        </div>
        <div class="modal-body" style="background-image: url('public/pre/images/bg-v1.png');
				     background-repeat: no-repeat;
    background-size: cover;
    height: 100%;
				">
				<div class='hide'>
					<img src="{{ asset('/').('public/pre/images/how-we-work.png') }}" style="height:750px;width:100%;" class="mid">
				</div>
				<div>
					<!-- start -->
					<h2 style="text-align:center"><strong><em>HOW </em></strong><em>can I</em>&nbsp;<strong>EARN</strong> with the <strong><em>REFERRAL CODE</em></strong>?</h2>

<p style="text-align:center"><strong><em>REFER EVERYONE </em></strong>you know with <em><strong>YOUR</strong></em>&nbsp;<strong><em>REFERRAL CODE</em></strong><strong><em>!</em></strong></p>

<h2 style="text-align:center"><strong><em>WHY</em></strong> should I <strong><em>REFER</em></strong> <strong><em>EVERYONE</em></strong>?</h2>

<h2 style="text-align:center">You will <strong><em>EARN</em></strong> a <strong><em>COMMISSION </em></strong>on&nbsp;<em><strong>EVERY&nbsp;TRANSACTION</strong></em></h2>

<p style="text-align:center"><strong><em>(BUYING, SELLING, PROVIDING OF SERVICE &amp; USING OF SERVICES) OF THE PEOPLE&nbsp;</em></strong>who&nbsp;<em><strong>YOU REFERRED &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </strong></em></p>

<p style="text-align:center"><em><strong>SO</strong></em>&nbsp;<strong><em>TELL</em></strong> them <strong><em>ABOUT SEVEN ATARA</em></strong> and <strong><em>MAKE THEM PRE-REGISTER</em></strong> with your <strong><em>REFERRAL CODE</em></strong>.&nbsp;</p>

<h2 style="text-align:center"><strong><em>DISCOUNT&nbsp;COUPON CODE?</em></strong></h2>

<p style="text-align:center"><strong><em>CLOSE THIS POP-UP,&nbsp;REFER PEOPLE&nbsp;</em></strong>via all your <strong><em>SOCIAL NETWORK</em></strong> profiles &amp;&nbsp;grab&nbsp;<em><strong>YOUR DISCOUNT COUPON CODE</strong></em>!</p>

<p style="text-align:center">&nbsp;</p>

<p style="text-align:center">Doubts on how to do?</p>

<p style="text-align:center">Video Box!</p>
<div class="" style="text-align:center;font-size: 24px;">Step - 2 out of 2</div>
					<!-- end -->
				</div>
					<div style="text-align:center">
					<br>
					<button type="button" class="btn btn-success" id="understand" data-dismiss="modal">I understand</button>
					</div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
	<!-- End Modal -->
	<!-- Modal -->
  <div class="modal fade" id="ffModal" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header hide">
          <button type="button" class="close" data-dismiss="modal">
						<span class="txt-white">&times;</span>
					</button>
          <h4 class="modal-title">Contact Us</h4>
        </div>
        <div class="modal-body">
          <div>
					<ul>
	<li><strong><em>WHAT</em></strong> should I do with the <strong><em>DISCOUNT COUPON CODE?</em></strong></li>
</ul>

<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<strong>YOU</strong> can <strong><em>GET DISCOUNTS</em></strong> on <strong><em>ANYTHING</em></strong> you <strong><em>BUY</em></strong> or <strong><em>ANY SERVICES YOU USE</em></strong> on <strong>SEVEN ATARA!</strong></p>

<p>&nbsp;</p>

<p>Check the content on the page to know more and contact us for clarity!</p>

<p>&nbsp;</p>

<p>Doubts on how to do?</p>

<p>Click the link below..</p>

<p>Youtube link</p>

<p>&nbsp;</p>
					</div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
	<!-- End Modal -->

	<!-- Modal -->
  <div class="modal fade" id="contactModal" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">
						<span class="txt-white">&times;</span>
					</button>
          <h4 class="modal-title">Contact Us</h4>
        </div>
        <div class="modal-body">
          <b><p>
					<h3>Seven Atara</h3>
					Phone : 070106 09203<br>
					Phone : 081899 86248<br>
						<a href="mailto:info@sevenatara.com?Subject=Hello" target="_top">info@sevenatara.com</a></p></b>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
	<!-- End Modal -->
	<!-- Modal -->
  <div class="modal fade" id="faqModal" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">
						<span class="txt-white">&times;</span>
					</button>
          <h4 class="modal-title">Frequently Asked Questions</h4>
        </div>
        <div class="modal-body">
          <div>
					<!-- start -->
					<h1 style="text-align:center"><strong><em>FREQUENTLY ASKED QUESTIONS</em></strong></h1>

<p>1. What is <strong><em>SEVEN ATARA</em></strong>?</p>

<p style="margin-left:40px"><strong><em>SEVEN ATARA</em></strong> is an <strong><em>eCommerce platform</em></strong>. It is the <strong><em>first</em></strong> ever <strong><em>One-Stop Platform</em></strong> for all <strong><em>Goods &amp; Services</em></strong>.</p>

<p><em>2. <strong>Why</strong></em> Should I <strong><em>Pre-Register</em></strong> with <strong><em>SEVEN ATARA</em></strong>?</p>

<ul>
	<li>To <strong><em>get Discounts</em></strong> as you <strong><em>Purchase Goods</em></strong> and <strong><em>Avail Services</em></strong> on <strong><em>SEVEN ATARA</em></strong>!</li>
	<li>To <strong><em>Refer People</em></strong> who you know, to <strong><em>Earn</em></strong> on their <strong><em>Transactions (Buying, Selling, Providing Of Service &amp; Using of Services)!</em></strong></li>
</ul>

<p>3. <strong>How</strong> can I <strong><em>Earn</em></strong> on <strong><em>SEVEN ATARA</em></strong>?</p>

<ul>
	<li>You will get a <strong><em>Referral Code</em></strong> as you <strong><em>Pre-Register</em></strong> with <strong><em>SEVEN ATARA</em></strong>.</li>
	<li>You can <strong><em>Refer</em></strong> <strong><em>People</em></strong> who you know, TELL them ABOUT SEVEN ATARA and <strong><em>Make Them Register</em></strong> with <strong><em>SEVEN ATARA</em></strong> with your <strong><em>Referral Code</em></strong>.&nbsp;</li>
	<li>You will <strong><em>Earn</em></strong> a proportionate <strong><em>Commission</em></strong> on any of their <strong><em>Transactions(Buying, Selling, Providing Of Service &amp; Using Of Services)</em></strong> who <strong><em>Registered</em></strong> with <strong><em>Your</em></strong> own <strong><em>Referral Code</em></strong></li>
</ul>

<p><em>4. <strong>How</strong></em> will I get <strong><em>Discounts</em></strong>?</p>

<p style="margin-left:40px">You will get a <strong><em>Discount Coupon Code </em></strong>as u <strong><em>Pre-Register</em></strong>. You can <strong><em>use</em></strong> that <strong><em>Discount</em></strong> <strong><em>Coupon Code</em></strong> to get <strong><em>Discounts</em></strong> as you <strong><em>Purchase</em></strong> and <strong><em>Use Services</em></strong> on <strong><em>SEVEN ATARA</em></strong>!</p>

<p>5. What all <strong><em>Goods &amp; Services </em></strong>can I <strong><em>Find</em></strong> on <strong><em>SEVEN ATARA</em></strong>?</p>

<p style="margin-left:40px">&nbsp;Check <strong><em>&ldquo;Goods &amp; Services&rdquo;</em></strong> on https://www.sevenatara.com/</p>

<p><em>6.&nbsp;</em><strong><em>How</em></strong> <strong><em>SEVEN ATARA Works</em></strong>?</p>

<p style="margin-left:40px">Check <strong><em>&ldquo;How We Work?&rdquo;</em></strong> on https://www.sevenatara.com/</p>

<p><em>7. </em><strong><em>Where</em></strong> should I get <strong><em>started</em></strong> with <strong><em>SEVEN ATARA</em></strong>?</p>

<p style="margin-left:40px"><strong><em>Click</em></strong> <a href="https://www.sevenatara.com/">https://www.sevenatara.com/</a> to <strong><em>Get Started!</em></strong></p>

<p><strong><em>8. When</em></strong> can I expect the <strong><em>Launch</em></strong> of <strong><em>SEVEN ATARA</em></strong>?</p>

<p style="margin-left:40px"><strong><em>Follow us on, </em></strong></p>

<p style="margin-left:40px">https://www.facebook.com/7atara/</p>

<p style="margin-left:40px"><a href="https://www.linkedin.com/company/seven-atara-7a/">https://www.linkedin.com/company/seven-atara-7a/</a></p>

<p style="margin-left:40px"><a href="https://www.instagram.com/seven_atara/">https://www.instagram.com/seven_atara/</a></p>

<p style="margin-left:40px"><a href="https://twitter.com/SevenAtara">https://twitter.com/SevenAtara</a></p>

<p style="margin-left:40px"><a href="https://plus.google.com/u/0/103953527938257248270">https://plus.google.com/u/0/103953527938257248270</a></p>

<p style="margin-left:40px"><a href="https://in.pinterest.com/sevenatara/">https://in.pinterest.com/sevenatara/</a></p>

<p style="margin-left:40px"><a href="https://sevenatara.tumblr.com/">https://sevenatara.tumblr.com/</a></p>

<p style="margin-left:40px">to keep <strong><em>Yourself UPDATED!</em></strong></p>
					<!-- end -->
					</div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
	<!-- End Modal -->
<button type="button" class="btn btn-primary yellow btn-lg initModal hide" data-toggle="modal" data-target="#initModal"></button>
	<div class="modal fade" id="initModal" role="dialog">
    <div class="modal-dialog modal-lg" style="width:90%">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
				<div class="" style="text-align:center;font-size: 24px;">Step - 1 out of 2</div>
          <button type="button" class="close" data-dismiss="modal" style="color: white;font-size: 40px;margin-top: -40px;">&times;</button>
        </div>
        <div class="modal-body" style="background-image: url('public/pre/images/bg-v1.png');
				     background-repeat: no-repeat;
    background-size: cover;
    height: 100%;
				">
				<div class="hide">
					<img src="{{ asset('/').('public/pre/images/welcome.png') }}">
				</div>
<!-- start -->
<div>
<h1 style="text-align:center"><strong><em>EARN? HOW?</em></strong></h1>

<h1 style="text-align:center"><strong><em>PRE-REGISTER!</em></strong></h1>

<h3 style="text-align:center"><strong><em>WHY</em><strong> should I</strong><em> PRE-REGISTER</em><strong>?</strong></strong></h3>

<h3 style="text-align:center"><em>To </em><strong><em>GET</em></strong><em> your </em><strong><em>REFERRAL CODE &amp; DISCOUNT COUPON CODE.</em></strong></h3>

<p style="text-align:center">Note: <strong>DISCOUNT COUPON CODES</strong>&nbsp;WILL&nbsp;BE GIVEN <strong>ONLY</strong> IF YOU <strong>PRE-REGISTER!</strong></p>

<h3 style="text-align:center"><strong><em>REFERRAL CODE &amp; DISCOUNT COUPON CODE?</em></strong></h3>

<h3 style="text-align:center"><em><strong>FILL DETAILS</strong></em> &amp; <em><strong>CLICK NEXT</strong></em> to <em><strong>know more</strong></em>&hellip;!</h3>

<p style="text-align:center">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>

<p style="text-align:center">Doubts on how to do?</p>

<p style="text-align:center">Video Box!</p>
<h1 style="text-align:center"><strong><em>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</em></strong></h1>
</div>
<div class="" style="text-align:center;font-size: 24px;">Step - 1 out of 2</div>
<!-- end -->
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
	<!-- End Modal -->

	<!-- start of goods model -->

<!-- Modal -->
  <div class="modal fade" id="goodsModal" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">
						<span class="txt-white">&times;</span>
					</button>
          <h4 class="modal-title">Goods</h4>
        </div>
        <div class="modal-body">
					<ul class='gist'>

	<li>Agro-Products &amp; Agro-supplies</li>
	<li>Apparels &amp; Garments&nbsp;</li>
	<li>Automobiles &amp; Motorcycles</li>
	<li>Beauty products &amp; Personal Care&nbsp;</li>
	<li>Chemicals&nbsp;</li>
	<li>Computer Hardware &amp; Software&nbsp;</li>
	<li>Construction supplies &amp; Real Estate&nbsp;</li>
	<li>Consumer Electronics&nbsp;</li>
	<li>Electrical Equipments &amp; supplies&nbsp;</li>
	<li>Energy&nbsp;supplies &amp; supplements</li>
	<li>Environmental supplies</li>
	<li>Fashion Accessories</li>
	<li>Food &amp; Beverages&nbsp;</li>
	<li>Furniture</li>
	<li>Gifts &amp; Crafts&nbsp;</li>
	<li>Hardwares</li>
	<li>Home Appliances</li>
	<li>Home &amp; Garden&nbsp;supplies</li>
	<li>Health &amp; Medical&nbsp;products</li>
	<li>Industrial Parts &amp; Fabrication supplies</li>
	<li>Lights &amp; Lighting&nbsp;accessories</li>
	<li>Luggage, Bags &amp; Cases</li>
	<li>Machineries</li>
	<li>Measurement &amp; Analysis Instruments&nbsp;</li>
	<li>Minerals &amp; Metallurgical equipments</li>
	<li>Office &amp; School Supplies&nbsp;</li>
	<li>Packaging &amp; Printing&nbsp;supplies</li>
	<li>Rubber &amp; Plastics supplies &amp; products&nbsp;</li>
	<li>Security &amp; Protection supplies</li>
	<li>Service Equipments</li>
	<li>footwear&#39;s and footcare supplies&nbsp;</li>
	<li>Sports &amp; Entertainment&nbsp;supplies</li>
	<li>Telecommunication&nbsp;supplies</li>
	<li>Textile &amp; Leather Products</li>
	<li>Tools</li>
	<li>Toys &amp; Hobby supplies</li>
	<li>Transportation&nbsp;equipments &amp; accessories</li>


</ul>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
	<!-- End Modal -->

	<!-- end of goods model -->

	<!-- Modal -->
  <div class="modal fade" id="serviceModal" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">
						<span class="txt-white">&times;</span>
					</button>
          <h4 class="modal-title">Services</h4>
        </div>
        <div class="modal-body">
					<ul class="gist">
		<li>
	Automotive Services
	</li>
	<li>
	Beauty &amp; personal care&nbsp;
	</li>
	<li>
	Construction, Renovation, Home Services &amp; Repairs
	</li>
	<li>
	Emergency/Accident support services
	</li>
	<li>
	Environmental Services
	</li>
	<li>
	Event Planning,management &amp; support Services
	</li>
	<li>
	Finance &amp; Insurance Services
	</li>
	<li>
	Health &amp; Medical Services
	</li>
	<li>
	Heavy equipment &amp; Vehicular rental
	</li>
	<li>
	Hobbies &amp; Lessons
	</li>
	<li>
	Home Services
	</li>
	<li>
	Local Services
	</li>
	<li>
	Natural Resources Services
	</li>
	<li>
	Personal care &amp; support Services &amp; More
	</li>
	<li>
	Pet care&nbsp;
	</li>
	<li>
	Professional Services
	</li>
	<li>
	Safety/Security &amp; Legal Services
	</li>
	<li>
	Shipping &amp; Logistics
	</li>
	<li>
	Sports-trainers &amp; Sports-coaches
	</li>

</ul>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
	<!-- End Modal -->

	<!-- end of goods model -->
	


<link href="{{ asset('/').('public/pre/css/bootstrap.min.css') }}" type="text/css" rel="stylesheet"/>
<script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
<script src="{{ asset('/').('public/pre/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('/').('public/pre/js/custom.js') }}"></script>

<!-- Hotjar Tracking Code for https://www.sevenatara.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:864932,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-118415562-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-118415562-1');
</script>



</body>

</html>