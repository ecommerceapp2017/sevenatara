@extends('layout.public')
@section('content')
<section class="section white-backgorund">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2">
                        <div class="title-wrap">
                            <h2 class="title lines">Culture and Values</h2>
                            <p class="lead">7ATARA CULTURE IS ABOUT ADVOCATING TRADE AROUND THE WORLD.</p>
                        </div>
                    </div><!-- end col -->
                </div><!-- end row -->
                <div class="row">
                    <div class="col-sm-12">
                        <p>We inculcate an ecosystem where all people – consumers, merchants, third-party service providers and others – have an opportunity to flourish. Our prosperity and progress is built on the spirit of entrepreneurship, innovation, and a staunch focus on meeting the needs of our customers. We believe that a strong sense of shared values enables us to maintain a common company culture and community, no matter how large we grow.</p>
                        <div>
                            <h2>OUR VALUES</h2>
                            Passion is at the heart of our company and we move persistently to the core of innovation and to a better future. Our values include:
                        </div>
                </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                    <h5>Customer</h5>
                        <figure>
                            <img src="public/assets/img/blog/blog_01.jpg" alt="" />
                        </figure>
                        <p>Be our first priority. Committed in hearts and minds.</p>
                    </div><!-- end col -->
                    <div class="col-sm-4">
                    <h5>Quality</h5>
                        <figure>
                            <img src="public/assets/img/blog/blog_02.jpg" alt="" />
                        </figure>
                        <p>What we do, we do well, and we strive to be the best.</p>
                    </div><!-- end col -->
                    <div class="col-sm-4">
                    <h5>Embodied Changes</h5>
                        <figure>
                            <img src="public/assets/img/blog/blog_03.jpg" alt="" />
                        </figure>
                        <p>In this fast moving world, we must be consistent, innovative and ready to adapt to new business conditions in order to be persistent.</p>
                    </div><!-- end col -->
                </div><!-- end row -->
                <div class="row">
                <div class="col-sm-4">
                    <h5>Integrity</h5>
                        <figure>
                            <img src="public/assets/img/blog/blog_03.jpg" alt="" />
                        </figure>
                        <p>We wish our people to be straight forward and open minded. Keep it simple!</p>
                    </div><!-- end col -->
                    <div class="col-sm-4">
                    <h5>Passion</h5>
                        <figure>
                            <img src="public/assets/img/blog/blog_03.jpg" alt="" />
                        </figure>
                        <p>We require our people to be bold and courageous through entrepreneurial risk to reach boundaries and experiments with consistent honesty and genuine towards the community and we believe in people, we never give up.</p>
                    </div><!-- end col -->
                    <div class="col-sm-4">
                    <h5>Commitment</h5>
                        <figure>
                            <img src="public/assets/img/blog/blog_03.jpg" alt="" />
                        </figure>
                        <p>We persuade our people to create a culture of warmth respect where everyone is welcome and determined with entrepreneurial spirit, cost consciousness which creates excellence.</p>
                    </div><!-- end col -->
                </div>               
                <div class="row">
                <div class="col-sm-4">
                    <h5>Our Focus</h5>
                        <figure>
                            <img src="public/assets/img/blog/blog_03.jpg" alt="" />
                        </figure>
                        <p>We build responsible and honest relationships with communication that pursue growth and learning</p>
                    </div><!-- end col -->
                </div>
                <hr class="spacer-100">
                
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="mb-20">Our Store Locations</h4>
                    </div><!-- end col -->
                </div><!-- end row -->
                
                <div class="row column-4">
                    <div class="col-sm-6 col-md-3">
                        <div class="icon-boxes style2 light-backgorund">
                            <div class="box-content text-left">
                                <h6 class="alt-font">Istanbul</h6>
                                <p>
                                    77 Mass. Ave., E14/E15
                                    <br>
                                    Seattle, MA 02139-4307 USA
                                </p>
                            </div>
                        </div><!-- icon-box -->
                    </div><!-- end col -->
                    <div class="col-sm-6 col-md-3">
                        <div class="icon-boxes style2 light-backgorund">
                            <div class="box-content text-left">
                                <h6 class="alt-font">New York</h6>
                                <p>
                                    77 Mass. Ave., E14/E15
                                    <br>
                                    Seattle, MA 02139-4307 USA
                                </p>
                            </div>
                        </div><!-- icon-box -->
                    </div><!-- end col -->
                    <div class="col-sm-6 col-md-3">
                        <div class="icon-boxes style2 light-backgorund">
                            <div class="box-content text-left">
                                <h6 class="alt-font">London</h6>
                                <p>
                                    77 Mass. Ave., E14/E15
                                    <br>
                                    Seattle, MA 02139-4307 USA
                                </p>
                            </div>
                        </div><!-- icon-box -->
                    </div><!-- end col -->
                    <div class="col-sm-6 col-md-3">
                        <div class="icon-boxes style2 light-backgorund">
                            <div class="box-content text-left">
                                <h6 class="alt-font">Paris</h6>
                                <p>
                                    77 Mass. Ave., E14/E15
                                    <br>
                                    Seattle, MA 02139-4307 USA
                                </p>
                            </div>
                        </div><!-- icon-box -->
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </section>
<!-- end section -->
@stop