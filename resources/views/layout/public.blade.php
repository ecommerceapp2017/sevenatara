<!DOCTYPE html>
<html lang="en">


<head>
    <title>7Atara</title>
    <meta charset="utf-8">
    <meta name="description" content="Plus E-Commerce Template">
    <meta name="author" content="Diamant Gjota" />
    <meta name="keywords" content="plus, html5, css3, template, ecommerce, e-commerce, bootstrap, responsive, creative" />
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">

    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!--Favicon-->
    <link rel="shortcut icon" href="public/assets/img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="public/assets/img/favicon.ico" type="image/x-icon">

    <!-- css files -->
    <link href="{{ asset('/').('public/assets/css/bootstrap.min.css') }}" type="text/css" rel="stylesheet"/>
	<link href="{{ asset('/').('public/assets/css/font-awesome.min.css') }}" type="text/css" rel="stylesheet"/>
	<link href="{{ asset('/').('public/assets/css/owl.carousel.min.css') }}" type="text/css" rel="stylesheet"/>
    <link href="{{ asset('/').('public/assets/css/owl.theme.default.min.css') }}" type="text/css" rel="stylesheet"/>
	<link href="{{ asset('/').('public/assets/css/animate.css') }}" type="text/css" rel="stylesheet"/>
	<link href="{{ asset('/').('public/assets/css/swiper.css') }}" type="text/css" rel="stylesheet"/>
    <link href="{{ asset('/').('public/assets/css/style.css') }}" type="text/css" rel="stylesheet"/>

    <!-- this is default skin you can replace that with: dark.css, yellow.css, red.css ect -->
    <link id="pagestyle" rel="stylesheet" type="text/css" href="{{ asset('/').('public/assets/css/default.css') }}"  />

    <!-- Google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:100,300,400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Dosis:200,300,400,500,600,700,800&amp;subset=latin-ext" rel="stylesheet">

    <script src="{{ asset('/').('public/assets/js/jquery-3.1.1.min.js') }}"></script>
    <script src="{{ asset('/').('public/assets/js/cookie.js') }}"></script>
    <script src="{{ asset('/').('public/assets/js/custom/custom.js') }}"></script>

</head>
    <body>

       <!-- start section -->
        <section class="primary-background hidden-xs hide">
            <div class="container-fluid">
                <div class="row grid-space-0">
                    <div class="col-sm-12">
                        <figure>
                            <a href="category.html">
                                <img src="public/assets/img/banners/top_banner.jpg" alt=""/>
                            </a>
                        </figure>
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </section>
        <!-- end section -->

        <!-- start topBar -->
        <div class="topBar">
            <div class="container">
                <ul class="list-inline pull-left hidden-sm hidden-xs">
                    <li><span class="text-primary">Have a question?</span> Call +123 4567 8910</li>
                </ul>

                <ul class="topBarNav pull-right">
                    <li class="linkdown">
                        <a href="javascript:void(0);">
                            <i class="fa fa-usd mr-5"></i>
                            USD
                            <i class="fa fa-angle-down ml-5"></i>
                        </a>
                        <ul class="w-100">
                            <li><a href="javascript:void(0);"><i class="fa fa-eur mr-5"></i>EUR</a></li>
                            <li class="active"><a href="javascript:void(0);"><i class="fa fa-usd mr-5"></i>USD</a></li>
                            <li><a href="javascript:void(0);"><i class="fa fa-gbp mr-5"></i>GBP</a></li>
                        </ul>
                    </li>
                    <li class="linkdown">
                        <a href="javascript:void(0);">
                            <img src="public/assets/img/flags/flag-french.jpg" class="mr-5" alt="">
                            <span class="hidden-xs">
                                French
                                <i class="fa fa-angle-down ml-5"></i>
                            </span>
                        </a>
                        <ul class="w-100">
                            <li><a href="javascript:void(0);"><img src="public/assets/img/flags/flag-english.jpg" class="mr-5" alt="">English</a></li>
                            <li class="active"><a href="javascript:void(0);"><img src="public/assets/img/flags/flag-french.jpg" class="mr-5" alt="">French</a></li>
                            <li><a href="javascript:void(0);"><img src="public/assets/img/flags/flag-german.jpg" class="mr-5" alt="">German</a></li>
                            <li><a href="javascript:void(0);"><img src="public/assets/img/flags/flag-spain.jpg" class="mr-5" alt="">Spain</a></li>
                        </ul>
                    </li>
                    @if (!Auth::check())
                    <li>
                        <a href="{{ URL::to('/login') }}">
                            <i class="fa fa-user mr-5"></i>
                            <span class="hidden-xs">
                                Login / Register
                            </span>
                        </a>
                    </li>
                    @else
                     <li class="linkdown">
                        <a href="javascript:void(0);">
                            <i class="fa fa-user mr-5"></i>
                            <span class="hidden-xs">
                                My Account
                                <i class="fa fa-angle-down ml-5"></i>
                            </span>
                        </a>
                        <ul class="w-150">
                            <li><a href="{{ URL::to('/profile') }}">Profile</a></li>
                            <li><a href="{{ URL::to('/upgrade') }}">Upgrade Account</a></li>
                            <li><a href="{{ URL::to('/my-orders') }}">My Orders</a></li>
                            <li><a href="{{ URL::to('/logout') }}">Logout</a></li>
                            <!--
                            <li class="divider"></li>
                            <li><a href="wishlist.html">Wishlist (5)</a></li>
                            <li><a href="cart.html">My Cart</a></li>
                            <li><a href="checkout.html">Checkout</a></li>
                            -->
                        </ul>
                    </li>
                    @endif
                    <li class="linkdown hide">
                        <a href="javascript:void(0);">
                            <i class="fa fa-user mr-5"></i>
                            <span class="hidden-xs">
                                My Account
                                <i class="fa fa-angle-down ml-5"></i>
                            </span>
                        </a>
                        <ul class="w-150">
                            <li><a href="login.html">Login</a></li>
                            <li><a href="register.html">Create Account</a></li>
                            <li class="divider"></li>
                            <li><a href="wishlist.html">Wishlist (5)</a></li>
                            <li><a href="cart.html">My Cart</a></li>
                            <li><a href="checkout.html">Checkout</a></li>
                        </ul>
                    </li>
                    <li class="linkdown">
                        <a href="{{ URL::to('/cart') }}">
                            <i class="fa fa-shopping-basket mr-5"></i>
                            <span class="hidden-xs">
                                Cart<sup class="text-primary cart-val">
                                @if(Auth::check())
                                {{Auth::user()->cart}}
                                @endif
                                </sup>
                                <i class="fa fa-angle-down ml-5 hide"></i>
                            </span>
                        </a>
                        <ul class="cart w-250 hide">
                            <li>
                                <div class="cart-items">
                                    <ol class="items">
                                        <li>
                                            <a href="shop-single-product-v1.html" class="product-image">
                                                <img src="public/assets/img/products/men_06.jpg" alt="Sample Product ">
                                            </a>
                                            <div class="product-details">
                                                <div class="close-icon">
                                                    <a href="javascript:void(0);"><i class="fa fa-close"></i></a>
                                                </div>
                                                <p class="product-name">
                                                    <a href="shop-single-product-v1.html">Lorem Ipsum dolor sit</a>
                                                </p>
                                                <strong>1</strong> x <span class="price text-primary">$59.99</span>
                                            </div><!-- end product-details -->
                                        </li><!-- end item -->
                                        <li>
                                            <a href="shop-single-product-v1.html" class="product-image">
                                                <img src="public/assets/img/products/shoes_01.jpg" alt="Sample Product ">
                                            </a>
                                            <div class="product-details">
                                                <div class="close-icon">
                                                    <a href="javascript:void(0);"><i class="fa fa-close"></i></a>
                                                </div>
                                                <p class="product-name">
                                                    <a href="shop-single-product-v1.html">Lorem Ipsum dolor sit</a>
                                                </p>
                                                <strong>1</strong> x <span class="price text-primary">$39.99</span>
                                            </div><!-- end product-details -->
                                        </li><!-- end item -->
                                        <li>
                                            <a href="shop-single-product-v1.html" class="product-image">
                                                <img src="public/assets/img/products/bags_07.jpg" alt="Sample Product ">
                                            </a>
                                            <div class="product-details">
                                                <div class="close-icon">
                                                    <a href="javascript:void(0);"><i class="fa fa-close"></i></a>
                                                </div>
                                                <p class="product-name">
                                                    <a href="shop-single-product-v1.html">Lorem Ipsum dolor sit</a>
                                                </p>
                                                <strong>1</strong> x <span class="price text-primary">$29.99</span>
                                            </div><!-- end product-details -->
                                        </li><!-- end item -->
                                    </ol>
                                </div>
                            </li>
                            <li>
                                <div class="cart-footer">
                                    <a href="cart.html" class="pull-left"><i class="fa fa-cart-plus mr-5"></i>View Cart</a>
                                    <a href="checkout.html" class="pull-right"><i class="fa fa-shopping-basket mr-5"></i>Checkout</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div><!-- end container -->
        </div>
        <!-- end topBar -->

        <div class="middleBar">
            <div class="container">
                <div class="row display-table">
                    <div class="col-sm-3 vertical-align text-left hidden-xs">
                        <a href="{{ URL::to('/') }}">
                            <img width="160" src="public/assets/img/logo-big.png" alt="" />
                        </a>
                    </div><!-- end col -->
                    <div class="col-sm-7 vertical-align text-center">
                        <form>
                            <div class="row grid-space-1">
                                <div class="col-sm-3">
                                    <select class="form-control input-lg" name="category">
                                        <option value="all">All Categories</option>
                                        <optgroup label="Mens">
                                            <option value="shirts">Shirts</option>
                                            <option value="coats-jackets">Coats & Jackets</option>
                                            <option value="underwear">Underwear</option>
                                            <option value="sunglasses">Sunglasses</option>
                                            <option value="socks">Socks</option>
                                            <option value="belts">Belts</option>
                                        </optgroup>
                                        <optgroup label="Womens">
                                            <option value="bresses">Bresses</option>
                                            <option value="t-shirts">T-shirts</option>
                                            <option value="skirts">Skirts</option>
                                            <option value="jeans">Jeans</option>
                                            <option value="pullover">Pullover</option>
                                        </optgroup>
                                        <option value="kids">Kids</option>
                                        <option value="fashion">Fashion</option>
                                        <optgroup label="Sportwear">
                                            <option value="shoes">Shoes</option>
                                            <option value="bags">Bags</option>
                                            <option value="pants">Pants</option>
                                            <option value="swimwear">Swimwear</option>
                                            <option value="bicycles">Bicycles</option>
                                        </optgroup>
                                        <option value="bags">Bags</option>
                                        <option value="shoes">Shoes</option>
                                        <option value="hoseholds">HoseHolds</option>
                                        <optgroup label="Technology">
                                            <option value="tv">TV</option>
                                            <option value="camera">Camera</option>
                                            <option value="speakers">Speakers</option>
                                            <option value="mobile">Mobile</option>
                                            <option value="pc">PC</option>
                                        </optgroup>
                                    </select>
                                </div><!-- end col -->
                                <div class="col-sm-6">
                                    <input type="text" name="keyword" class="form-control input-lg" placeholder="Search">
                                </div><!-- end col -->
                                <div class="col-sm-3">
                                    <input type="submit"  class="btn btn-default btn-block btn-lg" value="Search">
                                </div><!-- end col -->
                            </div><!-- end row -->
                        </form>
                    </div><!-- end col -->
                    <div class="col-sm-2 vertical-align header-items hidden-xs hide">
                        <div class="header-item mr-5">
                            <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Wishlist">
                                <i class="fa fa-heart-o"></i>
                                <sub>32</sub>
                            </a>
                        </div>
                        <div class="header-item">
                            <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Compare">
                                <i class="fa fa-refresh"></i>
                                <sub>2</sub>
                            </a>
                        </div>
                    </div><!-- end col -->
                </div><!-- end  row -->
            </div><!-- end container -->
        </div><!-- end middleBar -->

        <!-- start navbar -->
        <div class="navbar yamm navbar-default">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" data-toggle="collapse" data-target="#navbar-collapse-3" class="navbar-toggle">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="{{ URL::to('/') }}" class="navbar-brand visible-xs">
                        <img src="public/assets/img/logo.png" alt="logo">
                    </a>
                </div>
                <div id="navbar-collapse-3" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <!-- Home -->
                        <li><a href="{{ URL::to('/') }}">Home</a></li>
                        <!-- Features -->
                        <li class="hide dropdown left"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Features<i class="fa fa-angle-down ml-5"></i></a>
                            <ul class="dropdown-menu">
                                <li><a href="headers.html">Headers</a></li>
                                <li><a href="footers.html">Footers</a></li>
                                <li><a href="sliders.html">Sliders</a></li>
                                <li><a href="typography.html">Typography</a></li>
                                <li><a href="grid.html">Grid</a></li>
                                <li class="divider"></li>
                                <li class="dropdown-submenu"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Dropdown Level 1</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Dropdown Level</a></li>
                                        <li class="dropdown-submenu"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Dropdown Level 2</a>
                                            <ul class="dropdown-menu">
                                                <li><a href="javascript:void(0);">Dropdown Level</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                            </ul><!-- end ul dropdown-menu -->
                        </li><!-- end li dropdown -->
                        <!-- Pages -->
                        <li class="hide dropdown yamm-fw"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Pages<i class="fa fa-angle-down ml-5"></i></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <!-- Content container to add padding -->
                                    <div class="yamm-content">
                                        <div class="row">
                                            <ul class="col-sm-3">
                                                <li class="title">
                                                    <h6>Shop Pages</h6>
                                                </li>
                                                <li><a href="shop-sidebar-left.html">Sidebar Left</a></li>
                                                <li><a href="shop-sidebar-right.html">Sidebar Right</a></li>
                                                <li><a href="shop-filter-top.html">Filters Top</a></li>
                                                <li><a href="shop-full-width-sidebar-left.html">Full Width Sidebar Left</a></li>
                                                <li><a href="shop-full-width-sidebar-right.html">Full Width Sidebar Right</a></li>
                                                <li><a href="shop-full-width-filter-top.html">Full Width Filters Top</a></li>
                                                <li><a href="category.html">Category <span class="label primary-background">1.1</span></a></li>
                                                <li><a href="shop-single-product-v1.html">Single product</a></li>
                                                <li><a href="shop-single-product-v2.html">Single product v2 <span class="label primary-background">1.3</span></a></li>
                                                <li class="title">
                                                    <h6>Contact Pages</h6>
                                                </li>
                                                <li><a href="contact-v1.html">Contact Us Version 1</a></li>
                                                <li><a href="contact-v2.html">Contact Us Version 2</a></li>
                                            </ul><!-- end ul col -->
                                            <ul class="col-sm-3">
                                                <li class="title">
                                                    <h6>About us Pages</h6>
                                                </li>
                                                <li><a href="about-us-v1.html">About Us Version 1</a></li>
                                                <li><a href="about-us-v2.html">About Us Version 2</a></li>
                                                <li><a href="about-us-v3.html">About Us Version 3</a></li>
                                                <li class="title">
                                                    <h6>Blog Pages</h6>
                                                </li>
                                                <li><a href="blog-v1.html">Blog Version 1</a></li>
                                                <li><a href="blog-v2.html">Blog Version 2</a></li>
                                                <li><a href="blog-v3.html">Blog Version 3</a></li>
                                                <li><a href="blog-article-v1.html">Blog article</a></li>
                                            </ul><!-- end ul col -->
                                            <ul class="col-sm-3">
                                                <li class="title">
                                                    <h6>User account</h6>
                                                </li>
                                                <li><a href="login.html">Login</a></li>
                                                <li><a href="register.html">Register</a></li>
                                                <li><a href="login-register.html">Login or Register</a></li>
                                                <li><a href="my-account.html">My Account</a></li>
                                                <li><a href="cart.html">Cart</a></li>
                                                <li><a href="wishlist.html">Wishlist</a></li>
                                                <li><a href="checkout.html">Checkout</a></li>
                                                <li><a href="user-information.html">User Information</a></li>
                                                <li><a href="order-list.html">Order List</a></li>
                                                <li><a href="order-confirmation.html">Order Confirmation <span class="label primary-background">1.1</span></a></li>
                                                <li><a href="forgot-password.html">Forgot Password</a></li>
                                            </ul><!-- end ul col -->
                                            <ul class="col-sm-3">
                                                <li class="title">
                                                    <h6>Other Pages</h6>
                                                </li>
                                                <li><a href="help.html">Help</a></li>
                                                <li><a href="faq.html">Faq</a></li>
                                                <li><a href="privacy-policy.html">Privacy Policy</a></li>
                                                <li><a href="blank-page.html">Blank Page <span class="label primary-background">1.1</span></a></li>
                                                <li><a href="404-error.html">404 Error</a></li>
                                                <li><a href="500-error.html">500 Error</a></li>
                                                <li><a href="coming-soon.html">Coming soon</a></li>
                                                <li><a href="subscribe.html">Subscribe</a></li>
                                            </ul><!-- end ul col -->
                                        </div><!-- end row -->
                                    </div><!-- end yamn-content -->
                                </li><!-- end li -->
                           </ul><!-- end ul dropdown-menu -->
                        </li><!-- end li dropdown -->
                        <!-- elements -->
                        <li class=""><a href="{{ URL::to('/product') }}">Product</a></li>
                        <li class=""><a href="{{ URL::to('/about') }}">About</a></li>
                        <li class=""><a href="{{ URL::to('/contact ') }}">Contact</a></li>
                        <!-- Collections -->
                        <li class="hide dropdown yamm-fw"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Collections<i class="fa fa-angle-down ml-5"></i></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <div class="yamm-content">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-3">
                                                <a href="javascript:void(0);">
                                                    <figure class="zoom-out">
                                                        <img alt="" src="public/assets/img/banners/collection_01.jpg">
                                                    </figure>
                                                </a>
                                            </div><!-- end col -->
                                            <div class="col-xs-12 col-sm-3">
                                                <a href="javascript:void(0);">
                                                    <figure class="zoom-in">
                                                        <img alt="" src="public/assets/img/banners/collection_02.jpg">
                                                    </figure>
                                                </a>
                                            </div><!-- end col -->
                                            <div class="col-xs-12 col-sm-3">
                                                <a href="javascript:void(0);">
                                                    <figure class="zoom-out">
                                                        <img alt="" src="public/assets/img/banners/collection_03.jpg">
                                                    </figure>
                                                </a>
                                            </div><!-- end col -->
                                            <div class="col-xs-12 col-sm-3">
                                                <a href="javascript:void(0);">
                                                    <figure class="zoom-in">
                                                        <img alt="" src="public/assets/img/banners/collection_04.jpg">
                                                    </figure>
                                                </a>
                                            </div><!-- end col -->
                                        </div><!-- end row -->

                                        <hr class="spacer-20 no-border">

                                        <div class="row">
                                            <div class="col-xs-12 col-sm-3">
                                                <h6>Pellentes que nec diam lectus</h6>
                                                <p>Proin pulvinar libero quis auctor pharet ra. Aenean fermentum met us orci, sedf eugiat augue pulvina r vitae. Nulla dolor nisl, molestie nec aliquam vitae, gravida sodals dolor...</p>
                                                <button type="button" class="btn btn-default round btn-md">Read more</button>
                                            </div><!-- end col -->
                                            <div class="col-xs-12 col-sm-3">
                                                <div class="thumbnail store style1">
                                                    <div class="header">
                                                        <div class="badges">
                                                            <span class="product-badge top left white-backgorund text-primary semi-circle">Sale</span>
                                                            <span class="product-badge top right text-primary">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-half-o"></i>
                                                            </span>
                                                        </div>
                                                        <figure class="layer">
                                                            <img src="public/assets/img/products/men_01.jpg" alt="">
                                                        </figure>
                                                        <div class="icons">
                                                            <a class="icon semi-circle" href="javascript:void(0);"><i class="fa fa-heart-o"></i></a>
                                                            <a class="icon semi-circle" href="javascript:void(0);"><i class="fa fa-gift"></i></a>
                                                            <a class="icon semi-circle" href="javascript:void(0);" data-toggle="modal" data-target=".productQuickView"><i class="fa fa-search"></i></a>
                                                        </div>
                                                    </div>
                                                    <div class="caption">
                                                        <h6 class="thin"><a href="javascript:void(0);">Lorem Ipsum dolor sit</a></h6>
                                                        <div class="price">
                                                            <small class="amount off">$68.99</small>
                                                            <span class="amount text-primary">$59.99</span>
                                                        </div>
                                                        <a href="javascript:void(0);"><i class="fa fa-cart-plus mr-5"></i>Add to cart</a>
                                                    </div><!-- end caption -->
                                                </div><!-- end thumbnail -->
                                            </div><!-- end col -->
                                            <div class="col-xs-12 col-sm-3">
                                                <div class="thumbnail store style1">
                                                    <div class="header">
                                                        <div class="badges">
                                                            <span class="product-badge top left white-backgorund text-primary semi-circle">Sale</span>
                                                            <span class="product-badge top right text-primary">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-half-o"></i>
                                                            </span>
                                                        </div>
                                                        <figure class="layer">
                                                            <img src="public/assets/img/products/women_01.jpg" alt="">
                                                        </figure>
                                                        <div class="icons">
                                                            <a class="icon semi-circle" href="javascript:void(0);"><i class="fa fa-heart-o"></i></a>
                                                            <a class="icon semi-circle" href="javascript:void(0);"><i class="fa fa-gift"></i></a>
                                                            <a class="icon semi-circle" href="javascript:void(0);" data-toggle="modal" data-target=".productQuickView"><i class="fa fa-search"></i></a>
                                                        </div>
                                                    </div>
                                                    <div class="caption">
                                                        <h6 class="thin"><a href="javascript:void(0);">Lorem Ipsum dolor sit</a></h6>
                                                        <div class="price">
                                                            <small class="amount off">$68.99</small>
                                                            <span class="amount text-primary">$59.99</span>
                                                        </div>
                                                        <a href="javascript:void(0);"><i class="fa fa-cart-plus mr-5"></i>Add to cart</a>
                                                    </div><!-- end caption -->
                                                </div><!-- end thumbnail -->
                                            </div><!-- end col -->
                                            <div class="col-xs-12 col-sm-3">
                                                <div class="thumbnail store style1">
                                                    <div class="header">
                                                        <div class="badges">
                                                            <span class="product-badge top left white-backgorund text-primary semi-circle">Sale</span>
                                                            <span class="product-badge top right text-primary">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-half-o"></i>
                                                            </span>
                                                        </div>
                                                        <figure class="layer">
                                                            <img src="public/assets/img/products/kids_01.jpg" alt="">
                                                        </figure>
                                                        <div class="icons">
                                                            <a class="icon semi-circle" href="javascript:void(0);"><i class="fa fa-heart-o"></i></a>
                                                            <a class="icon semi-circle" href="javascript:void(0);"><i class="fa fa-gift"></i></a>
                                                            <a class="icon semi-circle" href="javascript:void(0);" data-toggle="modal" data-target=".productQuickView"><i class="fa fa-search"></i></a>
                                                        </div>
                                                    </div>
                                                    <div class="caption">
                                                        <h6 class="thin"><a href="javascript:void(0);">Lorem Ipsum dolor sit</a></h6>
                                                        <div class="price">
                                                            <small class="amount off">$68.99</small>
                                                            <span class="amount text-primary">$59.99</span>
                                                        </div>
                                                        <a href="javascript:void(0);"><i class="fa fa-cart-plus mr-5"></i>Add to cart</a>
                                                    </div><!-- end caption -->
                                                </div><!-- end thumbnail -->
                                            </div><!-- end col -->
                                        </div><!-- end row -->
                                    </div><!-- end yamm-content -->
                                </li><!-- end li -->
                            </ul><!-- end dropdown-menu -->
                        </li><!-- end dropdown -->
                    </ul><!-- end navbar-nav -->
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown right">
                            <a href="#" data-toggle="dropdown" class="dropdown-toggle">
                                <span class="hidden-sm">Categories</span><i class="fa fa-bars ml-5"></i>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="dropdown-submenu"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Mens</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="category.html">Shirts</a></li>
                                        <li><a href="category.html">Coats & Jackets</a></li>
                                        <li><a href="category.html">Underwear</a></li>
                                        <li><a href="category.html">Sunglasses</a></li>
                                        <li><a href="category.html">Socks</a></li>
                                        <li><a href="category.html">Belts</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown-submenu"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Womens</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="category.html">Bresses</a></li>
                                        <li><a href="category.html">T-shirts</a></li>
                                        <li><a href="category.html">Skirts</a></li>
                                        <li><a href="category.html">Jeans</a></li>
                                        <li><a href="category.html">Pullover</a></li>
                                    </ul>
                                </li>
                                <li><a href="javascript:void(0);">Kids</a></li>
                                <li><a href="javascript:void(0);">Fashion</a></li>
                                <li class="dropdown-submenu"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">SportWear</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="category.html">Shoes</a></li>
                                        <li><a href="category.html">Bags</a></li>
                                        <li><a href="category.html">Pants</a></li>
                                        <li><a href="category.html">SwimWear</a></li>
                                        <li><a href="category.html">Bicycles</a></li>
                                    </ul>
                                </li>
                                <li><a href="javascript:void(0);">Bags</a></li>
                                <li><a href="javascript:void(0);">Shoes</a></li>
                                <li><a href="javascript:void(0);">HouseHolds</a></li>
                                <li class="dropdown-submenu"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Technology</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="category.html">TV</a></li>
                                        <li><a href="category.html">Camera</a></li>
                                        <li><a href="category.html">Speakers</a></li>
                                        <li><a href="category.html">Mobile</a></li>
                                        <li><a href="category.html">PC</a></li>
                                    </ul>
                                </li>
                            </ul><!-- end ul dropdown-menu -->
                        </li><!-- end dropdown -->
                    </ul><!-- end navbar-right -->
                </div><!-- end navbar collapse -->
            </div><!-- end container -->
        </div><!-- end navbar -->


<?php
#Show hide Menu
$actual_link = $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
$url = explode("/",$actual_link);
$counts = count($url) - 1;
$urls = $url[$counts];
//  echo $urls;exit;
if($urls=='upgrade' || $urls=='profile' || $urls=='upload' || $urls =='cart' || $urls =='my-product' || $urls =='checkout' || $urls =='my-orders'|| $urls == 'post'
|| $urls=='post-workman-equipment'
){

// if(Auth::check()){

// }else{

//   if($urls=='cart' || $urls=='checkout'){
//     echo 'conti';
//   }else{
//     echo 'redir';
//   }
//   }

    // $url = $_SERVER['HTTP_HOST'];
    // //echo $url;
    // //if (strpos($url, 'localhost')) {
    // if($_SERVER['HTTP_HOST']=='localhost'){
    //     $path = 'applocal';
    // }else{
    //     $path = 'applive';
    // }
    // $url = Config::get('CONST.'.$path.'.url');
    //header('Location: '.$url);
    //echo $path;

//exit;
?>
<!-- start section -->
        <section class="section white-backgorund">
            <div class="container">
                <div class="row">
                    <!-- start sidebar -->
                    <div class="col-sm-3">
                        <div class="widget">
                            @if (Auth::check())
                            <h6 class="subtitle">Account Navigation</h6>

                            <ul class="list list-unstyled">
                                <li class='active'>
                                    <a href="{{ URL::to('/profile') }}">My Account</a>
                                </li>
                                <li>
                                    <a href="{{ URL::to('/upgrade') }}">Upgrade Account </a>
                                </li>
                                <li>
                                    <a href="{{ URL::to('/upload') }}">Upload Document </a>
                                </li>
                                @if(Auth::user()->bank_detail_id!='' && Auth::user()->business_detail_id!='')
                                <li>
                                    <a href="{{ URL::to('/post') }}">Post Products / Service</a>
                                </li>
                                @endif
                                @if(Auth::user()->bank_detail_id!='' && Auth::user()->business_detail_id!='' )
                                <li>
                                    <a href="{{ URL::to('/my-product') }}">My Product / Service</a>
                                </li>
                                @endif
                                @if(Auth::user()->bank_detail_id!='' && Auth::user()->business_detail_id!='' && Auth::user()->role_service_user!='')
                                <li>
                                    <a href="{{ URL::to('/post-workman-equipment') }}">Post Workman / Equipment Details</a>
                                </li>
                                @endif
                            </ul>
                            @endif
                        </div><!-- end widget -->
                    </div><!-- end col -->
                    <!-- end sidebar -->
<?php
}
?>



        @yield('content')
        <!-- start footer -->
        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="icon-boxes style1">
                            <div class="icon">
                                <i class="fa fa-truck text-gray"></i>
                            </div><!-- end icon -->
                            <div class="box-content">
                                <h6 class="alt-font text-light text-uppercase">Free Shipping</h6>
                                <p class="text-gray">Aenean semper lacus sed molestie sollicitudin.</p>
                            </div>
                        </div><!-- icon-box -->
                    </div><!-- end col -->
                    <div class="col-sm-3">
                        <div class="icon-boxes style1">
                            <div class="icon">
                                <i class="fa fa-life-ring text-gray"></i>
                            </div><!-- end icon -->
                            <div class="box-content">
                                <h6 class="alt-font text-light text-uppercase">Support 24/7</h6>
                                <p class="text-gray">Aenean semper lacus sed molestie sollicitudin.</p>
                            </div>
                        </div><!-- icon-box -->
                    </div><!-- end col -->
                    <div class="col-sm-3">
                        <div class="icon-boxes style1">
                            <div class="icon">
                                <i class="fa fa-gift text-gray"></i>
                            </div><!-- end icon -->
                            <div class="box-content">
                                <h6 class="alt-font text-light text-uppercase">Gift cards</h6>
                                <p class="text-gray">Aenean semper lacus sed molestie sollicitudin.</p>
                            </div>
                        </div><!-- icon-box -->
                    </div><!-- end col -->
                    <div class="col-sm-3">
                        <div class="icon-boxes style1">
                            <div class="icon">
                                <i class="fa fa-credit-card text-gray"></i>
                            </div><!-- end icon -->
                            <div class="box-content">
                                <h6 class="alt-font text-light text-uppercase">Payment 100% Secure</h6>
                                <p class="text-gray">Aenean semper lacus sed molestie sollicitudin.</p>
                            </div>
                        </div><!-- icon-box -->
                    </div><!-- end col -->
                </div><!-- end row -->

                <hr class="spacer-30">

                <div class="row">
                    <div class="col-sm-3">
                        <h5 class="title">Plus</h5>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin suscipit, libero a molestie consectetur, sapien elit lacinia mi.</p>

                        <hr class="spacer-10 no-border">

                        <ul class="social-icons">
                            <li class="facebook"><a href="javascript:void(0);"><i class="fa fa-facebook"></i></a></li>
                            <li class="twitter"><a href="javascript:void(0);"><i class="fa fa-twitter"></i></a></li>
                            <li class="dribbble"><a href="javascript:void(0);"><i class="fa fa-dribbble"></i></a></li>
                            <li class="linkedin"><a href="javascript:void(0);"><i class="fa fa-linkedin"></i></a></li>
                            <li class="youtube"><a href="javascript:void(0);"><i class="fa fa-youtube"></i></a></li>
                            <li class="behance"><a href="javascript:void(0);"><i class="fa fa-behance"></i></a></li>
                        </ul>
                    </div><!-- end col -->
                    <div class="col-sm-3">
                        <h5 class="title">My Account</h5>
                        <ul class="list alt-list">
                            <li><a href="my-account.html"><i class="fa fa-angle-right"></i>My Account</a></li>
                            <li><a href="wishlist.html"><i class="fa fa-angle-right"></i>Wishlist</a></li>
                            <li><a href="cart.html"><i class="fa fa-angle-right"></i>My Cart</a></li>
                            <li><a href="checkout.html"><i class="fa fa-angle-right"></i>Checkout</a></li>
                        </ul>
                    </div><!-- end col -->
                    <div class="col-sm-3">
                        <h5 class="title">Information</h5>
                        <ul class="list alt-list">
                            <li><a href="about-us-v1.html"><i class="fa fa-angle-right"></i>About Us</a></li>
                            <li><a href="faq.html"><i class="fa fa-angle-right"></i>FAQ</a></li>
                            <li><a href="privacy-policy.html"><i class="fa fa-angle-right"></i>Privacy Policy</a></li>
                            <li><a href="contact-v1.html"><i class="fa fa-angle-right"></i>Contact Us</a></li>
                        </ul>
                    </div><!-- end col -->
                    <div class="col-sm-3">
                        <h5 class="title">Payment Methods</h5>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                        <ul class="list list-inline">
                            <li class="text-white"><i class="fa fa-cc-visa fa-2x"></i></li>
                            <li class="text-white"><i class="fa fa-cc-paypal fa-2x"></i></li>
                            <li class="text-white"><i class="fa fa-cc-mastercard fa-2x"></i></li>
                            <li class="text-white"><i class="fa fa-cc-discover fa-2x"></i></li>
                        </ul>
                    </div><!-- end col -->
                </div><!-- end row -->

                <hr class="spacer-30">

                <div class="row text-center">
                    <div class="col-sm-12">
                        <p class="text-sm">&COPY; 2017. Made with <i class="fa fa-heart text-danger"></i> by <a href="javascript:void(0);">DiamondCreative.</a></p>
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </footer>
        <!-- end footer -->


        <!-- JavaScript Files -->
        <script src="{{ asset('/').('public/assets/js/bootstrap.min.js') }}"></script>
		<script src="{{ asset('/').('public/assets/js/owl.carousel.min.js') }}"></script>
		<script src="{{ asset('/').('public/assets/js/jquery.downCount.js') }}"></script>
		<script src="{{ asset('/').('public/assets/js/nouislider.min.js') }}"></script>
		<script src="{{ asset('/').('public/assets/js/jquery.sticky.js') }}"></script>
		<script src="{{ asset('/').('public/assets/js/pace.min.js') }}"></script>
		<script src="{{ asset('/').('public/assets/js/star-rating.min.js') }}"></script>
		<script src="{{ asset('/').('public/assets/js/wow.min.js') }}"></script>
		<script src="{{ asset('/').('public/assets/js/gmaps.js') }}"></script>
		<script src="{{ asset('/').('public/assets/js/swiper.min.js') }}"></script>
		<script src="{{ asset('/').('public/assets/js/main.js') }}"></script>


    </body>


</html>
