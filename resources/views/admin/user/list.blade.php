@extends('admin.layout.layout')
@section('content')        
<link rel="stylesheet" href="{{ asset('/').('public/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        {{$title}}
        <small>List</small>
      </h1>
      <ol class="breadcrumb hide">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- /.box -->

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Users List</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>S.No</th>
                  <th>User Id</th>
                  <th>Email</th>
                  <th>Phone</th>
                  <th>Self Referal Code</th>
                  <th>Referal Code</th>
                  <th>Coupon Code</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php $i = 0; ?>
                @foreach($users as $user)
                <?php $i++; ?>
                <tr>
                  <td>{{$i}}</td>
                  <td>#{{$user->id}}</td>
                  <td>{{$user->email}}</td>
                  <td>{{$user->phone}}</td>
                  <td>{{$user->self_referal_code}}</td>
                  <td>{{$user->referal_code}}</td>
                  <td>{{$user->coupon_code}}</td>
                  <?php
                  if($user->status=='0')
                  {
                    $status = 'Registered';
                  }
                  else
                  {
                    $status = 'Deleted';
                  }
                  ?>
                  <td>
                  {{$status}}
                  </td>
                  <td>
                  <a href ="users/delete/{{$user->id}}">
                  <button type="button" class="btn btn-block btn-danger">
                  <i class="fa fa-fw fa-trash" style='color:white'></i>
                  </button>
                  </a>
                  </td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<!-- DataTables -->
<script>
  $(function () {
    $('#example1').DataTable({
      stateSave: true
    })
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
<script src="{{ asset('/').('public/admin/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('/').('public/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
@stop