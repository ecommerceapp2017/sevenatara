@extends('admin.layout.layout')
@section('content')        
<link rel="stylesheet" href="{{ asset('/').('public/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        {{$title}}
        <small>List</small>
      </h1>
      <ol class="breadcrumb hide">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
      </ol>
    </section>
    @if($title=='Create Category')
    <!-- Main content -->
    <section class="content">
<form action="createCategory" method="post">
      <div class="row">
        <div class="col-xs-12">
          <!-- /.box -->
          @if (Session::has('err'))
        <div class="alert alert-danger">
        <?php
        $err = Session::get('err');
        foreach($err->messages() as $msg)
        {
          echo $msg[0].'<br>';
        }
        ?>
        </div>
        @endif
        @if (Session::has('success'))
        <div class="alert alert-success">
        Category created succesfully
        </div>
        @endif
        <div class="box box-default">
        <div class="box-header with-border">
        <h3 class="box-title">Category Information</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">

            <div class="col-md-6">
              <div class="form-group">
                <label>Category Name</label>
                <input type="text" class="form-control" placeholder="Category Name" name="name">
              </div>
              <div class="form-group">
                <label>Category Description</label>
                <textarea class="form-control" placeholder="Category Name" rows="4" name="description"></textarea>
              </div>
              <!-- /.form-group -->
            </div>
            <!-- /.col -->
            <div class="col-md-6">
              <div class="form-group">
                <label>Tax Type</label>
                <select class="form-control" name="tax_type">
                  <option value="1">Fixed</option>
                  <option value="2">Percent</option>
                </select>
              </div>
              <div class="form-group">
                <label>Tax Value</label>
                <input type="text" placeholder="Tax value" class="form-control" name="tax_value">
              </div>
              
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          <center>
            <input type="submit" class="btn btn-primary">
          </center>
        </div>
        </div>
        </div>
      </div>
</form>
      <!-- /.row -->
    </section>
    <!-- /.content -->
    @else
    <!-- Main content -->
    <section class="content">
<form action="editCategory" method="post">
<input type="hidden" name='id' value='{{$data['id']}}'>
      <div class="row">
        <div class="col-xs-12">
          <!-- /.box -->
          @if (Session::has('err'))
        <div class="alert alert-danger">
        <?php
        $err = Session::get('err');
        foreach($err->messages() as $msg)
        {
          echo $msg[0].'<br>';
        }
        ?>
        </div>
        @endif
        @if (Session::has('success'))
        <div class="alert alert-success">
        Category updated succesfully
        </div>
        @endif
        <div class="box box-default">
        <div class="box-header with-border">
        <h3 class="box-title">Category Information</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">

            <div class="col-md-6">
              <div class="form-group">
                <label>Category Name</label>
                <input type="text" class="form-control" placeholder="Category Name" name="name" value="{{$data['name']}}">
              </div>
              <div class="form-group">
                <label>Category Description</label>
                <textarea class="form-control" placeholder="Category Name" rows="4" name="description">{{$data['description']}}</textarea>
              </div>
              <!-- /.form-group -->
            </div>
            <!-- /.col -->
            <div class="col-md-6">
              <div class="form-group">
                <label>Tax Type</label>
                <select class="form-control" name="tax_type">
                  <option value="1" @if($data['tax_type']=='1') selected @endif>Fixed</option>
                  <option value="2" @if($data['tax_type']=='2') selected @endif>Percent</option>
                </select>
              </div>
              <div class="form-group">
                <label>Tax Value</label>
                <input type="text" placeholder="Tax value" class="form-control" name="tax_value" value="{{$data['tax_value']}}">
              </div>
              
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          <center>
            <input type="submit" class="btn btn-primary">
          </center>
        </div>
        </div>
        </div>
      </div>
</form>
      <!-- /.row -->
    </section>
    <!-- /.content -->
    @endif
  </div>
  <!-- /.content-wrapper -->
<!-- DataTables -->
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
<script src="{{ asset('/').('public/admin/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('/').('public/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
@stop