@extends('admin.layout.layout')
@section('content')        
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Product Details</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form style="padding:20px 20px 20px 20px">
                                    <div class='form-group'>
                                        <label for="exampleInputEmail1">Product Type</label>
                                        <ul>
                                            <li style="list-style:none;">
                                                <input type="radio" name="p_p_type" value="1" class='form-check-input' checked id='type' /> Perishable
                                                <input type="radio" name="p_p_type" class='form-check-input' value="2" id="type" /> Non Perishable
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Category Type</label>
                                        <select class='form-control' id='p_cat_type'>
                                            @foreach($data as $dat)
                                            <option value='{{$dat["id"]}}'>{{$dat['name']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Product Name :</label>
                                        <input type="email" class="form-control" id="p_p_name" placeholder="Product Name" name="email">
                                        <span class='err' id='p_p_name_err'>
                            </div>
                            <div class="form-group">
                            <div class='row'>
                            <div class='col-md-6'>
                            <label for="email">Price per unit :</label>
                            <input type="email" class="form-control" id="p_p_price" placeholder="Price per unit" name="email">
                            <span class='err' id='p_p_price_err'>
                            </div>
                            <div class='col-md-6'>
                            <label for="email">Unit Type :</label>
                            <select class='form-control' id='p_p_unit_type'>
                            <option value='1'>Kg</option>
                            <option value='2'>Gallon</option>
                            </select>
                            </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <div class='row'>
                            <div class='col-md-6'>
                            <label for="email">Select GST Applicable :</label>
                            <select class='form-control' id='p_p_gst'>
                            <option value='1'>Yes</option>
                            <option value='0'>No</option>
                            </select>
                            </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <div class='row'>
                            <div class='col-md-6'>
                            <label for="email">Supply Capacity :</label>
                            <input type="email" class="form-control" id="p_p_supply_capacity" placeholder="Supply Capacity" name="email">
                            <span class='err' id='p_p_supply_capacity_err'>
                            </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <div class='row'>
                            <div class='col-md-6'>
                            <label for="email">Add Color :</label>
                            <input type="email" class="form-control" id="p_p_color" placeholder="Add Color" name="email">
                            <span class='err' id='p_p_color_err'>
                            </div>
                            </div>
                            <div class='row'>
                            <div class='col-md-6'>
                            <label for="email">Add Variety :</label>
                            <input type="email" class="form-control" id="p_p_variety" placeholder="Add Variety" name="email">
                            <span class='err' id='p_p_variety_err'>
                            </div>
                            </div>
                            <div class='row'>
                            <div class='col-md-6'>
                            <label for="email">Material Type :</label>
                            <input type="email" class="form-control" id="p_p_material_type" placeholder="Material Type" name="email">
                            <span class='err' id='p_p_material_type_err'>
                            </div>
                            </div>
                            </div>
                            <div class='form-group'>
                            <label for="email">Description :</label>
                            <textarea rows="" cols="" class='form-control' placeholder='Description' id='p_p_description'></textarea>
                            <span class='err' id='p_p_description_err'>
                            </div>
                            <div class="form-group">
                            <div class='row'>
                            <div class='col-md-6'>
                            <label for="email">Minimum Order Quantity (MOQ) :</label>
                            <input type="email" class="form-control" id="p_p_moq" placeholder="Minimum Order Quantity (MOQ" name="email">
                            <span class='err' id='p_p_moq_err'>
                            </div>
                            <div class='col-md-6'>
                            <label for="email">Returning Quantity :</label>
                            <input type="email" class="form-control" id="p_p_returns" placeholder="Returning Quantity" name="email">
                            <span class='err' id='p_p_returns_err'>
                            </div>
                            </div>
                            </div>
                            <div class='form-group'>
                            <label for="email">Sample Available ? :</label>
                            <div class="radio-input radio-primary">
                            <input type="radio" name="p_p_sample" id="" value="1">
                            <label for="radio4">
                            Available
                            </label>
                            <input type="radio" name="p_p_sample" id="radio4" value="0" checked>
                            <label for="radio4">
                            Not Available
                            </label>
                            </div>
                            </div>
                            <div class='form-group'>
                            <label for="email">Warranty ? :</label>
                            <div class="radio-input radio-primary">
                            <input type="radio" name="p_p_warranty" id="" value="1">
                            <label for="radio4">
                            Yes
                            </label>
                            <input type="radio" name="p_p_warranty" id="radio4" value="0" checked>
                            <label for="radio4">
                            No
                            </label>
                            </div>
                            </div>
                            <div class='form-group'>
                            <label for="email">Guarantee ? :</label>
                            <div class="radio-input radio-primary">
                            <input type="radio" name="p_p_gurantee" id="radio4" value="1">
                            <label for="radio4">
                            Yes
                            </label>
                            <input type="radio" name="p_p_gurantee" id="radio4" value="0" checked>
                            <label for="radio4">
                            No
                            </label>
                            </div>
                            </div>
                            <div class='form-group' id='uploadForms'>
                            <label for="email">Upload Product Images</label>
                            <figure class="user-avatar medium">
								<img class="u_p_p_i" alt="avatar" src="public/assets/img/avatar.png" style="height:65px;width:65px">
                                <span id="cat_pic" href="javascript:;" class="u_p_p button mid-short dark-light ">Upload Image</span>
                                <span id="u_p_p_s"></span>
                                <input type="hidden" id="u_p_p_im">
							</figure>
                            <span class='err' id='u_p_p_im_err'>
                            </div>
                            <center style="display:none">
                            <a href="javascript:void(0);" id='postProduct' class="btn btn-default btn-lg">Post Product</a>
                            </center>
                            </form>
          </div>
          <!-- /.box -->

          


      

        </div>
        <!--/.col (left) -->
       
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  @stop