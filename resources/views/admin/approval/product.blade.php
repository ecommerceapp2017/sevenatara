@extends('admin.layout.layout')
@section('content')        
<link rel="stylesheet" href="{{ asset('/').('public/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Approval
        <small>Product</small>
      </h1>
      <ol class="breadcrumb hide">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- /.box -->

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Product List</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>S.No</th>
                  <th>Product Name</th>
                  <th>Action</th>
                  <th>View</th>
                </tr>
                </thead>
                <tbody>
                <?php $i = 0; ?>
                @foreach($contacts as $contact)
                <?php $i++; ?>
                <tr>
                  <td>{{$i}}</td>
                  <td>{{$contact->name}}</td>
                  <?php
                  if($contact->status=='1'){
                    $status = '<a href="action/approve/product/'.$contact->id.'"><button type="button" class="btn btn-small btn-success">Approve</button></a><a  href="action/reject/product/'.$contact->id.'><button type="button" class="btn btn-small btn-danger">Reject</button></a>';
                  }
                  else if($contact->status=='4'){
                    $status = '<a href="action/approve/product/'.$contact->id.'"><button type="button" class="btn btn-small btn-warning">Re-Approve</button></a>';
                  }
                  else{
                    $status = '<a href="action/suspend/product/'.$contact->id.'"><button type="button" class="btn btn-small btn-danger">Suspend</button></a>';
                  }
                  ?>
                  <td><?php echo $status?></td>
                  <td><a href="approval-products/{{$contact->id}}" class="btn btn-small btn-primary">View Product</a></td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<!-- DataTables -->

<div class="modal fade" id="modal-default">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Product Detail</h4>
              </div>
              <div class="modal-body">
                <form>
                  <div class="form-group">
                  <label for="exampleInputEmail1">Product Name</label>
                  <input type="text" class="form-control" id="c_name" placeholder="Account Holder Name" name="email">
                  <span class="error" id="c_name_error"></span>
                  </div>
                  <div class="form-group">
                  <label for="exampleInputEmail1">Price</label>
                  <input type="text" class="form-control" id="c_name" placeholder="Account Holder Name" name="email">
                  <span class="error" id="c_name_error"></span>
                  </div>
                  <div class="form-group">
                  <label for="exampleInputEmail1">GST</label>
                  <input type="text" class="form-control" id="c_name" placeholder="Account Holder Name" name="email">
                  <span class="error" id="c_name_error"></span>
                  </div>
                  <div class="form-group">
                  <label for="exampleInputEmail1">Supply Capacity</label>
                  <input type="text" class="form-control" id="c_name" placeholder="Account Holder Name" name="email">
                  <span class="error" id="c_name_error"></span>
                  </div>
                  <div class="form-group">
                  <label for="exampleInputEmail1">Color</label>
                  <input type="text" class="form-control" id="c_name" placeholder="Account Holder Name" name="email">
                  <span class="error" id="c_name_error"></span>
                  </div>
                  <div class="form-group">
                  <label for="exampleInputEmail1">Variety</label>
                  <input type="text" class="form-control" id="c_name" placeholder="Account Holder Name" name="email">
                  <span class="error" id="c_name_error"></span>
                  </div>
                  <div class="form-group">
                  <label for="exampleInputEmail1">Material Type</label>
                  <input type="text" class="form-control" id="c_name" placeholder="Account Holder Name" name="email">
                  <span class="error" id="c_name_error"></span>
                  </div>
                  <div class="form-group">
                  <label for="exampleInputEmail1">Description</label>
                  <input type="text" class="form-control" id="c_name" placeholder="Account Holder Name" name="email">
                  <span class="error" id="c_name_error"></span>
                  </div>
                  <div class="form-group">
                  <label for="exampleInputEmail1">MOQ</label>
                  <input type="text" class="form-control" id="c_name" placeholder="Account Holder Name" name="email">
                  <span class="error" id="c_name_error"></span>
                  </div>  
                  <div class="form-group">
                  <label for="exampleInputEmail1">Retuns</label>
                  <input type="text" class="form-control" id="c_name" placeholder="Account Holder Name" name="email">
                  <span class="error" id="c_name_error"></span>
                  </div>  
                  <div class="form-group">
                  <label for="exampleInputEmail1">Sample</label>
                  <input type="text" class="form-control" id="c_name" placeholder="Account Holder Name" name="email">
                  <span class="error" id="c_name_error"></span>
                  </div>  
                  <div class="form-group">
                  <label for="exampleInputEmail1">Gurantee</label>
                  <input type="text" class="form-control" id="c_name" placeholder="Account Holder Name" name="email">
                  <span class="error" id="c_name_error"></span>
                  </div>  
                  <div class="form-group">
                  <label for="exampleInputEmail1">Warranty</label>
                  <input type="text" class="form-control" id="c_name" placeholder="Account Holder Name" name="email">
                  <span class="error" id="c_name_error"></span>
                  </div>  
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
<script src="{{ asset('/').('public/admin/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('/').('public/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
@stop