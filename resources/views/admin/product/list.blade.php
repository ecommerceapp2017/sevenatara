<?php
namespace App\Http\Controllers;
use App\Category;
?>
@extends('admin.layout.layout')
@section('content')        
<link rel="stylesheet" href="{{ asset('/').('public/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        {{$title}}
        <small>List</small>
      </h1>
      <ol class="breadcrumb hide">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- /.box -->

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Products List</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>S.No</th>
                  <th>Name</th>
                  <th>Price</th>
                  <th>Product Type</th>
                  <th>Category Type</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php $i = 0; ?>
                @foreach($users as $user)
                <?php $i++; ?>
                <tr>
                  <td>{{$i}}</td>
                  <td>#{{$user->name}}</td>
                  <td>{{$user->price}}</td>
                  <?php
                  $typeDat = Category::where('id', $user['cat_type'])->first(); 
                  if($user['type']=='0')
                  {
                    $type = 'Non Perishable';
                  }
                  else
                  {
                    $type = 'Perishable';
                  }
                  ?>
                  <td>{{$typeDat['name']}}</td>
                  <td>{{$type}}</td>
                  <?php
                  if($user->status==0)
                  {
                    $status = 'Not Active';
                    $text = 'Activate';
                    $class = 'btn-success';
                  }
                  else
                  {
                    $status = 'Active';
                    $text = 'Deactivate';
                    $class = 'btn-danger';
                  }
                  ?>
                  <td>{{$status}}</td>
                  <td>
                  <a class='btn btn-primary' href='products/{{$user->id}}'>View Product</a>
                  <a href="product-status/{{$user->id}}" class="btn {{$class}}">{{$text}}</a>
                  </td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<!-- DataTables -->
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
<script src="{{ asset('/').('public/admin/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('/').('public/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
@stop