@extends('admin.layout.layout')
@section('content')        
<link rel="stylesheet" href="{{ asset('/').('public/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        {{$title}}
        <small>List</small>
      </h1>
      <ol class="breadcrumb hide">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- /.box -->

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Products Detail</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                        <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Basic Details</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form">
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Product Type</label>
                  <ul>
                  <li  style="list-style:none;">
                      <input type="radio" name="ptype" value="1" class='form-check-input' @if($users['type']==1) checked @endif checked id='type'  disabled/>
                      Perishable
                      <input type="radio" name="ptype" class='form-check-input' value="0" @if($users['type']==0) checked @endif checked id="type" disabled />
                      Non Perishable
                  </li>
                  </ul>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Category Type</label>
                  <select disabled>
                    @foreach($products as $product)
                    <option value="{{$product->id}}" @if($users['cat_type']==$product['id']) selected @endif>{{$product->name}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Name</label>
                  <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter name"  value='{{$users->name}}' name='name' disabled>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Price</label>
                  <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter price"  value='{{$users->price}}' name='price' disabled>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Order</label>
                  <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter order"  value='{{$users->order}}' name='order' disabled>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Supply</label>
                  <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter supply"  value='{{$users->supply}}' name='supply' disabled>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Rating</label>
                  <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter rating"  value='{{$users->rating}}' name='rating' disabled>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Description</label>
                  <textarea disabled class="form-control">{{$users->rating}}</textarea>
                </div>
              </div>
              <!-- /.box-body -->
            </form>
            </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>

        
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<!-- DataTables -->
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
<script src="{{ asset('/').('public/admin/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('/').('public/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
@stop