@extends('layout.public')
@section('content')

                    <!-- end sidebar -->
                    <div class="col-sm-9">
                        <div class="row">
                            <div class="col-sm-12 text-left">
                                <h5 class="title">Upgrade Account</h5>
                            </div><!-- end col -->
                        </div><!-- end row -->

                        <hr class="spacer-5"><hr class="spacer-20 no-border">

                        <div class="row">

                        @if(Auth::user()->aadhar_number!='')
                        <div class="row column-4">
                        <div class="col-sm-6 col-md-3">
                            <div class="thumbnail blog">
                                <div class="header">
                                    <figure>
                                        <img src="public/assets/img/blog/blog_01.jpg" alt="">
                                    </figure>
                                    <div class="meta">
                                        <span><i class="fa fa-calendar mr-5"></i>Oct 25, 2016</span>
                                        <span><i class="fa fa-comment mr-5"></i>(15)</span>
                                        <span><i class="fa fa-heart mr-5"></i>(35)</span>
                                    </div>
                                </div>
                                <div class="caption">
                                    <h6><a href="blog-article-v1.html">Buyer</a></h6>
                                    <p>Buy products Online</p>
                                    <a href="javascript:;" class="btn btn-default semi-circle btn-sm">Upgraded</a>
                                </div><!-- end caption -->
                            </div><!-- end thumbnail -->
                        </div><!-- end col -->
                        <div class="col-sm-6 col-md-3">
                            <div class="thumbnail blog">
                                <div class="header">
                                    <figure>
                                        <img src="public/assets/img/blog/blog_02.jpg" alt="">
                                    </figure>
                                    <div class="meta">
                                        <span><i class="fa fa-calendar mr-5"></i>Oct 25, 2016</span>
                                        <span><i class="fa fa-comment mr-5"></i>(15)</span>
                                        <span><i class="fa fa-heart mr-5"></i>(35)</span>
                                    </div>
                                </div>
                                <div class="caption">
                                    <h6><a href="blog-article-v1.html">Broker</a></h6>
                                    <p>Do Brokage Online</p>
                                    <a href="javascript:;" class="btn btn-default semi-circle btn-sm">Upgraded</a>
                                </div><!-- end caption -->
                            </div><!-- end thumbnail -->
                        </div><!-- end col -->
                        <div class="col-sm-6 col-md-3">
                            <div class="thumbnail blog">
                                <div class="header">
                                    <figure>
                                        <img src="public/assets/img/blog/blog_04.jpg" alt="">
                                    </figure>
                                    <div class="meta">
                                        <span><i class="fa fa-calendar mr-5"></i>Oct 25, 2016</span>
                                        <span><i class="fa fa-comment mr-5"></i>(15)</span>
                                        <span><i class="fa fa-heart mr-5"></i>(35)</span>
                                    </div>
                                </div>
                                <div class="caption">
                                    <h6><a href="blog-article-v1.html">Seeker</a></h6>
                                    <p>Seek things Online</p>
                                    <a href="javascript:;" class="btn btn-default semi-circle btn-sm">Upgraded</a>
                                </div><!-- end caption -->
                            </div><!-- end thumbnail -->
                        </div><!-- end col -->
                        <div class="col-sm-6 col-md-3">
                            <div class="thumbnail blog">
                                <div class="header">
                                    <figure>
                                        <img src="public/assets/img/blog/blog_04.jpg" alt="">
                                    </figure>
                                    <div class="meta">
                                        <span><i class="fa fa-calendar mr-5"></i>Oct 25, 2016</span>
                                        <span><i class="fa fa-comment mr-5"></i>(15)</span>
                                        <span><i class="fa fa-heart mr-5"></i>(35)</span>
                                    </div>
                                </div>
                                <div class="caption">
                                    <h6><a href="blog-article-v1.html">Service User</a></h6>
                                    <p>Service user as Online</p>
                                    <a href="javascript:;" class="btn btn-default semi-circle btn-sm">Upgraded</a>
                                </div><!-- end caption -->
                            </div><!-- end thumbnail -->
                        </div><!-- end col -->

                        <div class="col-sm-6 col-md-3">
                            <div class="thumbnail blog">
                                <div class="header">
                                    <figure>
                                        <img src="public/assets/img/blog/blog_04.jpg" alt="">
                                    </figure>
                                    <div class="meta">
                                        <span><i class="fa fa-calendar mr-5"></i>Oct 25, 2016</span>
                                        <span><i class="fa fa-comment mr-5"></i>(15)</span>
                                        <span><i class="fa fa-heart mr-5"></i>(35)</span>
                                    </div>
                                </div>
                                <div class="caption">
                                    <h6><a href="blog-article-v1.html">Seller</a></h6>
                                    <p>Sell things Online</p>
                                    @if(Auth::user()->business_detail_id=='' || Auth::user()->bank_detail_id=='')
                                    <a href="{{ URL::to('/upload') }}" class="btn btn-warning semi-circle btn-sm" >Upgrade Account</a>
                                    @else
                                    <a href="javascript:;" class="btn btn-default semi-circle btn-sm">Upgraded</a>
                                    @endif
                                </div><!-- end caption -->
                            </div><!-- end thumbnail -->
                        </div><!-- end col -->
                        <div class="col-sm-6 col-md-3">
                            <div class="thumbnail blog">
                                <div class="header">
                                    <figure>
                                        <img src="public/assets/img/blog/blog_04.jpg" alt="">
                                    </figure>
                                    <div class="meta">
                                        <span><i class="fa fa-calendar mr-5"></i>Oct 25, 2016</span>
                                        <span><i class="fa fa-comment mr-5"></i>(15)</span>
                                        <span><i class="fa fa-heart mr-5"></i>(35)</span>
                                    </div>
                                </div>
                                <div class="caption">
                                    <h6><a href="blog-article-v1.html">Service Provider</a></h6>
                                    <p>Service Provider Online</p>
                                    @if(Auth::user()->role_service_user=='')
                                    <a href="javascript:;" class="btn btn-warning semi-circle btn-sm" data-toggle="modal" data-target=".productReview">Upgrade Account</a>
                                    @endif
                                    @if(Auth::user()->bank_detail_id=='' && Auth::user()->business_detail_id=='' && Auth::user()->role_service_user!='')
                                    <a href="upload" class="btn btn-warning semi-circle btn-sm" data-toggle="modal" data-target=".productReview">Upload Document</a>
                                    @endif
                                    @if(Auth::user()->bank_detail_id!='' && Auth::user()->business_detail_id!='' &&  Auth::user()->role_service_user!='')
                                    <a href="javascript:;" class="btn btn-default semi-circle btn-sm">Upgraded</a>
                                    @endif

                                </div><!-- end caption -->
                            </div><!-- end thumbnail -->
                        </div><!-- end col -->
                    </div>
                        @else
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="firstname">Enter Aadhar Number <span class="text-danger">*</span></label>
                                    <input id="referal_code" type="text" placeholder="Enter Aadhar Number" name="firstname" class="form-control input-sm required" maxlength="20">
                                </div><!-- end form-group -->
                                <div class="form-group">
                                        <div class="checkbox-input mb-10">
                                            <input id="show_balance" name="show_balance" class="styled" type="checkbox">
                                            <label for="show_balance">
                                                I agree for terms and conditions
                                            </label>
                                        </div><!-- end checkbox-input -->
                                </div><!-- end form-group -->
                                <div class="form-group">
                                    <a href="javascript:void(0);" id='aadharButton' class="btn btn-default round btn-md"><i class="fa fa-save mr-5"></i> Update</a>
                                </div><!-- end form-group -->
                            </div><!-- end col -->
                        @endif
                        </div><!-- end row -->
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </section>
        <!-- end section -->

        <!-- Modal Product Review -->
                        <div class="modal fade productReview" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <h5>Upgrade as  Service Provider</h5>
                                    </div><!-- end modal-header -->
                                    <div class="modal-body">
                                    <!-- start of pop -->

                            <div class="form-group">
                            <label for="exampleInputEmail1">Post Service Details</label>
                                <ul>
                                <li style="list-style:none;">
                                    <input type="radio" name="p_p_type" value="1" class="form-check-input" checked="" id="type">
                                    Indivdual
                                    <input type="radio" name="p_p_type" class="form-check-input" value="2" id="type">
                                    Company
                                </li>
                                </ul>
                            </div>

                            <!-- start of company -->
                                <div class="formCompany" style="">

                                <div class="form-group">
                                <label for="exampleInputEmail1">Category Type</label>
                                <select class="form-control" id="p_s_type">
                                @foreach($datas as $dat)
                                <option value='{{$dat["id"]}}'>{{$dat['name']}}</option>
                                @endforeach
                                </select>
                                </div>

                                <div class="form-group">
                                <label for="email">Company Name :</label>
                                <input type="email" class="form-control" id="p_s_name" placeholder="Company Name" name="email">
                                <span class="err" id="p_s_name_err" style="display: none;">
                                </span></div>

                                <div class="form-group">
                                <div class="row">
                                <div class="col-md-6">
                                <label for="email">GSTIN :</label>
                                <input type="email" class="form-control" id="p_s_gstin" placeholder="GSTIN" name="email">
                                <span class="err" id="p_s_gstin_err" style="display: none;">
                                </span></div>
                                </div>
                                </div>

                                <div class="form-group">
                                <div class="row">
                                <div class="col-md-6">
                                <label for="email">Address :</label>
                                <input type="email" class="form-control" id="p_s_address" placeholder="Address" name="email">
                                <span class="err" id="p_s_address_err" style="display: none;">
                                </span></div>
                                </div>
                                </div>

                                <div class="form-group">
                                <div class="row">
                                <div class="col-md-4">
                                <label for="email">Country :</label>
                                <select class="form-control" id="p_s_country">
                                <option value="1">India</option>
                                </select>
                                </div>
                                <div class="col-md-4">
                                <label for="email">State :</label>
                                <select class="form-control" id="p_s_state">
                                <option value="1">Tamil Nadu</option>
                                </select>
                                </div>
                                <div class="col-md-4">
                                <label for="email">City :</label>
                                <select class="form-control" id="p_s_city">
                                <option value="1">Chennai</option>
                                </select>
                                </div>
                                </div>
                                </div>

                                <div class="form-group">
                                <label for="email">Region of Service :</label>
                                <select class="form-control" id="p_s_service_city">
                                <option value="1">Chennai</option>
                                </select>
                                </div>

                                <div class="form-group">
                                <label for="email">Billing Address :</label>
                                <input type="email" class="form-control" id="p_s_b_address" placeholder="Billing Address" name="email">
                                <span class="err" id="p_s_b_address_err" style="display: none;">
                                </span></div>

                                <div class="form-group">
                                <label for="email">Certification if Available :</label>
                                <input type="email" class="form-control" id="p_s_cert" placeholder="Certification" name="email">
                                <span class="err" id="p_s_cert_err" style="display: none;">
                                </span></div>

                                <div class="form-group">
                                <label for="email">TAN Number :</label>
                                <input type="email" class="form-control" id="p_s_tan" placeholder="TAN Number" name="email">
                                <span class="err" id="p_s_tan_err" style="display: none;">
                                </span></div>

                                <div class="form-group">
                                <label for="email">Aadhar Number :</label>
                                <input type="email" class="form-control" id="p_s_aadhar" placeholder="Aadhar Number" name="email">
                                <span class="err" id="p_s_aadhar_err" style="display: none;">
                                </span></div>

                                <div class="form-group">
                                <label for="email">PAN Number :</label>
                                <input type="email" class="form-control" id="p_s_pan" placeholder="PAN Number" name="email">
                                <span class="err" id="p_s_pan_err" style="display: none;">
                                </span></div>

                                <div class="form-group">
                                <div class="checkbox-input checkbox-primary">
                                <input id="checkbox2" class="styled" type="checkbox" checked="">
                                <label for="checkbox2">
                                I there by agree to the TERMS &amp; CONDITIONS of 7ATARA
                                </label>
                                </div>
                                </div>
                                <center>
                                <a href="javascript:void(0);" id="postServiceDetailCompany" class="btn btn-default btn-lg">Post Service Detail</a>
                                </center>
                                </div>
                                <!-- end of company -->

                                <!-- start of individual -->
                                <div class="formIndividual" style="display: none;">

                                <div class="form-group">
                                <label for="exampleInputEmail1">Category Type</label>
                                <select class="form-control" id="p_si_type">
                                @foreach($datas as $dat)
                                <option value='{{$dat["id"]}}'>{{$dat['name']}}</option>
                                @endforeach
                                </select>
                                </div>

                                <div class="form-group">
                                <label for="email">Name :</label>
                                <input type="email" class="form-control" id="p_si_name" placeholder="Name" name="email">
                                <span class="err" id="p_si_name_err" style="display: none;">
                                </span></div>

                                <div class="form-group">
                                <div class="row">
                                <div class="col-md-12">
                                <label for="email">Address :</label>
                                <input type="email" class="form-control" id="p_si_address" placeholder="Address" name="email">
                                <span class="err" id="p_si_address_err" style="display: none;">
                                </span></div>

                                </div>
                                </div>
                                <div class="form-group">
                                <label for="email">Certification if Available :</label>
                                <input type="email" class="form-control" id="p_si_cert" placeholder="Certification" name="email">
                                <span class="err" id="p_si_cert_err" style="display: none;">
                                </span></div>
                                <div class="form-group">
                                <label for="email">Region of Service :</label>
                                <select class="form-control" id="p_si_service_city">
                                <option value="1">Chennai</option>
                                </select>
                                </div>
                                <div class="form-group">
                                <label for="exampleInputEmail1">Proof of Identity</label>
                                <?php
                                if(Auth::user()->poi_img==''){
                                $path = 'public/assets/img/avatar.png';
                                }
                                else{
                                $path = Auth::user()->poi_img;
                                }
                                ?>
                                <figure class="user-avatar medium">
                                    <img class='poiImg' alt="avatar" src='{{$path}}' style='height:65px;width:65px' >
                                    <span id='poi_pic' href="javascript:;" class="btn-md round">Upload Image</span>
                                    <span id='poi_status'></span>
                                </figure>
                                <input type='hidden' name='poiUrl'>
                                <span class="error" id="poiUrlHTML"></span>
                                </div>
                                <div class="form-group" id="uploadForms">
                                <label for="email">Upload Photograph</label>
                                <figure class="user-avatar medium">
								<?php
								if(Auth::user()->image==''){
								$path = 'public/assets/img/avatar.png';
								}
								else{
								$path = Auth::user()->image;
								}
								?>
								<img class='userImg' alt="avatar" src='{{$path}}' style='height:65px;width:65px' >
                                <span id='cat_pic' href="javascript:;" class="button mid-short dark-light" >Upload Image</span>
							    </figure>
                                </div>
                                <div class="form-group">
                                <label for="email">Machinery equipments of vehicle involved ? :</label>
                                <div class="radio-input radio-primary">
                                <input type="radio" name="p_si_machine" id="" value="1">
                                <label for="radio4">
                                Yes
                                </label>
                                <input type="radio" name="p_si_machine" id="radio4" value="0" checked="">
                                <label for="radio4">
                                No
                                </label>
                                </div>
                                </div>
                                <div class="form-group">
                                <div class="checkbox-input checkbox-primary">
                                <input id="checkbox2" class="styled" type="checkbox" checked="">
                                <label for="checkbox2">
                                I there by agree to the TERMS &amp; CONDITIONS of 7ATARA
                                </label>
                                </div>
                                </div>
                                <center>
                                <a href="javascript:void(0);" id="postServiceDetailIndividual" class="btn btn-default btn-lg">Post Service Detail</a>
                                </center>
                                </div>
                                <!-- end of individual -->
                                    <!-- end of pop -->
                                    </div><!-- end modal-body -->
                                </div><!-- end modal-content -->
                            </div><!-- end modal-dialog -->
                        </div><!-- end productRewiew -->

<script src="{{ asset('/').('public/assets/js/custom/user.js') }}"></script>
<script src="{{ asset('/').('public/assets/js/custom/s3/ajaxupload.3.5.js') }}"></script>
<script src="{{ asset('/').('public/assets/js/custom/s3/s3_aad.js') }}"></script>
<script src="{{ asset('/').('public/assets/js/custom/s3/s3_poi.js') }}"></script>
<script src="{{ asset('/').('public/assets/js/custom/s3/s3_profile.js') }}"></script>
@stop
