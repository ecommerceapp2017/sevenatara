@extends('layout.public')
@section('content')
<?php
if(Auth::check()){

}else{
$url = $_SERVER['HTTP_HOST'];
if($_SERVER['HTTP_HOST']=='localhost'){
    $path = 'applocal';
}else{
    $path = 'applive';
}
$url = Config::get('CONST.'.$path.'.url');
header('Location: '.$url);
exit;
}
?>

                    <div class="col-sm-9">
                        <div class="row">
                            <div class="col-sm-12 text-left">
                                <h5 class="title">My personal information</h5>
                                <button class='btn btn-primary pull-right' id='showPasswordScreen'>Change Password</button>
                                <button class='btn btn-primary pull-right hide' id='showProfileScreen'>Back to Profile</button>
                            </div><!-- end col -->
                        </div><!-- end row -->
                        <div class="profile-image">
						<div class="profile-image-data">
							<figure class="user-avatar medium">
								<?php
								if(Auth::user()->image==''){
								$path = 'public/assets/img/avatar.png';
								}
								else{
								$path = Auth::user()->image;
								}
								?>
								<img class='userImg' alt="avatar" src='{{$path}}' style='height:65px;width:65px' >
                                <span id='cat_pic' href="javascript:;" class="button mid-short dark-light" >Upload Image</span>
							</figure>

						</div>

						<span id='status'></span>
						<span id='imgErr' class='error'></span>
					</div>
                        <hr class="spacer-5"><hr class="spacer-20 no-border">

                        <div class="row">
                        <div class="col-md-12 hide" id="passwordSection">

                        <div class="col-md-6">
                        <div class="form-group">
                                <label for="firstname">Old Password</label>
                                <input id="oldPassword" type="password" placeholder="Old Password" name="firstname" class="form-control input-sm required">
                                <span id='oldPassword_error' class='error'></span>
                        </div>
                        </div>

                        <div class="col-md-6">
                        <div class="form-group">
                                <label for="firstname">New Password</label>
                                <input id="newPassword" type="password" placeholder="New Password" name="firstname" class="form-control input-sm required">
                                <span id='newPassword_error' class='error'></span>
                        </div>
                        <div class="form-group">
                                <label for="firstname">Confirm Password</label>
                                <input id="confirmPassword" type="password" placeholder="Confirm Password" name="firstname" class="form-control input-sm required" >
                                <span id='confirmPassword_error' class='error'></span>
                        </div>
                        <div class="form-group">
                                    <a href="javascript:void(0);" id="updatePassword" class="btn btn-default round btn-md"><i class="fa fa-save mr-5"></i> Change Password</a>
                        </div><!-- end form-group -->

                        </div>
                        </div>
<div  id="profileSection">
                            <div class="col-md-6">

                                <div class="form-group">
                                    <label for="firstname">First Name</label>
                                    <input id="firstname" type="text" placeholder="First Name" name="firstname" class="form-control input-sm required" value="{{Auth::user()->first_name}}">
                                    <span id='first_name_error' class='error'></span>
                                </div><!-- end form-group -->
                                <div class="form-group">
                                    <label for="lastname">Email <span class="text-danger">*</span></label>
                                    <input id="" type="text" placeholder="Email" disabled="disabled" name="lastname" class="form-control input-sm required" value="{{Auth::user()->email}}">
                                </div><!-- end form-group -->
                                <div class="form-group">
                                    <label for="lastname">Address </label>
                                    <input id="address" type="text" placeholder="Address" name="address" class="form-control input-sm required" value="{{Auth::user()->address}}">
                                    <input type='hidden' id='lat' value='{{Auth::user()->lat}}'>
                                    <input type='hidden' id='lng' value="{{Auth::user()->lng}}">
                                    <span id='address_error' class='error'></span>
                                </div><!-- end form-group -->
                                <!--
                                <label>Date of Birth</label>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <select class="form-control" name="select">
                                                <option value="1">01</option>
                                                <option value="2">02</option>
                                                <option value="3">03</option>
                                                <option value="4">04</option>
                                                <option value="5">05</option>
                                                <option value="6">06</option>
                                                <option value="7">07</option>
                                                <option value="8">08</option>
                                                <option value="9">09</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>
                                                <option value="24">24</option>
                                                <option value="25">25</option>
                                                <option value="26">26</option>
                                                <option value="27">27</option>
                                                <option value="28">28</option>
                                                <option value="29">29</option>
                                                <option value="30">30</option>
                                                <option value="31">31</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <select class="form-control" name="select">
                                                <option value="january">January</option>
                                                <option value="february">February</option>
                                                <option value="march">March</option>
                                                <option value="april">April</option>
                                                <option value="may">May</option>
                                                <option value="june">June</option>
                                                <option value="july">July</option>
                                                <option value="agust">Agust</option>
                                                <option value="september">September</option>
                                                <option value="october">October</option>
                                                <option value="november">November</option>
                                                <option value="december">December</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                -->
                                <div class="form-group">
                                    <div class="checkbox-input checkbox-primary">
                                        <input id="newsletter" class="styled" type="checkbox" checked>
                                        <label for="newsletter">
                                            Sign up for our newsletter!
                                        </label>
                                    </div>
                                </div><!-- end form-group -->
                                <div class="form-group">
                                    <a href="javascript:void(0);" id="updateProfile" class="btn btn-default round btn-md"><i class="fa fa-save mr-5"></i> Save</a>
                                </div><!-- end form-group -->
                            </div><!-- end col -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="oldPassword">Last Name </label>
                                    <input id="lastname" type="text" placeholder="Last Name" name="oldPassword" class="form-control input-sm required" value='{{Auth::user()->last_name}}'>
                                    <span id='last_name_error' class='error'></span>
                                </div><!-- end form-group -->
                                <div class="form-group">
                                    <label for="newPassword">Phone</label>
                                    <input id="newPassword" type="text" placeholder="Phone" value="{{Auth::user()->phone}}" disabled='disabled' name="newPassword" class="form-control input-sm required">
                                </div><!-- end form-group -->
                                <!-- here-->
                            </div><!-- end col -->
</div>
                        </div><!-- end row -->
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </section>
        <!-- end section -->
<script src="{{ asset('/').('public/assets/js/custom/s3/ajaxupload.3.5.js') }}"></script>
<script src="{{ asset('/').('public/assets/js/custom/s3/s3_profile.js') }}"></script>
<script src="{{ asset('/').('public/assets/js/custom/user.js') }}"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDMYk26GLWUnUvtvmRwMRdtY-MenRbrvjA&libraries=places&sensor=false"></script>
<script type="text/javascript">
google.maps.event.addDomListener(window, 'load', function () {
var places = new google.maps.places.Autocomplete(document.getElementById('address'));
google.maps.event.addListener(places, 'place_changed', function () {
var place = places.getPlace();
var address = place.formatted_address;
var mesg = "address: " + address;
var appUrl = Cookies.get('appUrl');
$.post(appUrl + "googleplace", {  address: address })
    .done(function(data) {
        if (data.success == 1) {
            console.log(data.data);
            var res = data.data.split(",");
            $('#lat').val(res[0]);
            $('#lng').val(res[1]);
        } else {
            console.log(data.message);
        }
    });
});
});</script>
@stop
