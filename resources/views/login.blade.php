@extends('layout.public')
@section('content')
{{ csrf_field() }}

 <!-- start section -->
        <section class="section white-backgorund">
            <div class="container">
                <div class="row">
                    <!-- start sidebar -->
                    
                    <!-- end sidebar -->
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-12 text-left">
                                <h2 class="title">Login / Register</h2>
                            </div><!-- end col -->
                        </div><!-- end row -->
                        
                        <hr class="spacer-5"><hr class="spacer-20 no-border">
                        
                        <div class="row">
                            <div class="col-sm-6">
                                <h5 class="subtitle">Register</h5>
                                <form class="form-horizontal">
                                    <div class="form-group" id="registerEmailGroup">
                                        <label for="email" class="col-sm-3 control-label">Email <span class="text-danger">*</span></label>
                                        <div class="col-sm-6">
                                          <input type="email" class="form-control input-md" id="registeremail" placeholder="Email">
                                          <span id='registererroremail' class='err'></span>
                                        </div>
                                    </div><!-- end form-group -->
                                    <div class="form-group" id="registerPhoneGroup">
                                        <label for="name" class="col-sm-3 control-label">Phone <span class="text-danger">*</span></label></label>
                                        <div class="col-sm-6">
                                          <input type="text" class="form-control input-md" id="registerphone" placeholder="Phone">
                                          <span id='registererrorphone' class='err'></span>
                                        </div>
                                    </div><!-- end form-group -->
                                    <div class="form-group" id="registerPasswordGroup">
                                        <label for="name" class="col-sm-3 control-label">Password <span class="text-danger">*</span></label></label>
                                        <div class="col-sm-6">
                                          <input type="password" class="form-control input-md" id="registerpassword" placeholder="Password">
                                          <span id='registererrorpassword' class='err'></span>
                                        </div>
                                    </div><!-- end form-group -->
                                    <div class="form-group">
                                        <label for="password" class="col-sm-3 control-label" id='ref_title'>Referal Code</label>
                                        <div class="col-sm-6">
                                          <input type="text" class="form-control input-md" id="referal_code" placeholder="Enter Referal Code">
                                          <span id='registererrorreferal_code' class='err'></span>
                                          <span id='registererrorotp' class='err'></span>
                                        </div>
                                    </div><!-- end form-group -->
                                    <div class="form-group">
                                        <div class="col-sm-offset-6 col-sm-10">
                                            <a href="javascript:void(0);" id="registerAction" class="btn btn-default round btn-md registerUser"><i class="fa fa-user mr-5"></i> Register</a>
                                        </div>
                                    </div><!-- end form-group -->
                                    <div class="form-group otpUser hide">
                                        <div class="col-sm-offset-6 col-sm-10">
                                            <a href="javascript:void(0);" class="btn btn-default round btn-md"><i class="fa fa-user mr-5"></i> Submit OTP</a>
                                        </div>
                                    </div><!-- end form-group -->
                                    <input type='hidden' id='registeredID'>
                                </form>
                            </div><!-- end col -->
                            <div class="col-sm-6">
                                <h5 class="subtitle loginTitle">Login</h5>
                                <form class="form-horizontal">
                                    <div class="form-group">
                                        <label for="name" class="col-sm-3 control-label">Email / Phone<span class="text-danger">*</span></label>
                                        <div class="col-sm-6">
                                          <input type="text" class="form-control input-md" id="loginemail" placeholder="Email / Phone">
                                          <span id='loginerroremail' class='err'></span>
                                        </div>
                                    </div><!-- end form-group -->
                                    <div class="form-group ">
                                        <label for="email" class="col-sm-3 control-label loginPassword">Password <span class="text-danger">*</span></label>
                                        <div class="col-sm-6">
                                          <input type="password" class="form-control input-md loginPassword" id="loginpassword" placeholder="Password">
                                          <span id='loginerrorpassword' class='err'></span>
                                        <a href="javascript:;"><span class='forgotButton pull-right' id="forgotAction" style="font-size:14px;">Forgot your password?</span></a>
                                        </div>
                                    </div><!-- end form-group -->
                                    <div class="form-group loginUser" id="loginAction">
                                        <div class="col-sm-offset-6 col-sm-10">
                                            <a href="javascript:;" class="btn btn-default round btn-md"><i class="fa fa-user mr-5"></i> <span class='loginActionText'>Login</span></a>
                                        </div>
                                    </div><!-- end form-group -->
                                </form>
                            </div><!-- end col -->
                        </div><!-- end row -->
                    </div><!-- end col -->
                </div><!-- end row -->                
            </div><!-- end container -->
        </section>
        <!-- end section -->
<script src="{{ asset('/').('public/assets/js/custom/login.js') }}"></script>
@stop