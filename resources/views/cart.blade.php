<?php
namespace App;
use Auth;
use URL;
use App\Cart;
use App\Product;
if(Auth::check()){
    $product = array();
    $cart = Cart::where('user_id', Auth::user()->id)->get();
    foreach ($cart as $key => $value) {
        $p = Product::where('id', $value->product_id)->first();
        //$p->unit = $value->count*$p['price'];
        $p->unit = $value->count*$p['price'];
        $p->unit_others = $value->count;
        $p->status = $value->id;
        $p->gst = $value->id;
        array_push($product,$p);
    }
    //print_r($product);exit;
}
?>
@extends('layout.public')
@section('content')
<div class="col-sm-9">
                        <div class="row">
                            <div class="col-sm-12 text-left">
                                <h2 class="title">My Cart</h2>
                            </div><!-- end col -->
                        </div><!-- end row -->
                        
                        <hr class="spacer-5"><hr class="spacer-20 no-border">
                        @if(Auth::check())
                        @if($count !=0)
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="table-responsive">    
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th colspan="2">Products</th>
                                                <th>Price</th>
                                                <th>Quantity</th>
                                                <th colspan="2">Total</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($product as $item)
                                            <tr class='trr_{{$item->gst}}'>
                                                <td>
                                                    <a href="shop-single-product-v1.html">
                                                        <img width="60px" src="{{$item->image}}" alt="product">
                                                    </a>
                                                </td>
                                                <td>
                                                    <h6 class="regular"><a href="shop-single-product-v1.html">{{$item->name}}</a></h6>
                                                    <p>{{$item->description}}</p>
                                                    <input type='hidden'>
                                                </td>
                                                <td>
                                                    Rs.<span id='pri_{{$item->id}}' >{{$item->price}}</span>
                                                </td>
                                                <td>
                                                    <select class="form-control qty-item" name="select" id='{{$item->id}}'>
                                                        <?php
                                                        
                                                        for ($i=1; $i <= 10 ; $i++) { 
                                                            if($item->unit_others==$i){
                                                                $selection = 'selected';
                                                            }else{
                                                                $selection = '';
                                                            }
                                                            echo $item->id;
                                                            echo "<option ".$selection." value='".$i."'>".$i."</option>";
                                                        }
                                                        ?>
                                                    </select>
                                                </td>
                                                <td>
                                                    <span class="text-primary" id='tot_{{$item->id}}'>Rs.{{$item->unit}}</span>
                                                    <input type='hidden' class='tt to_{{$item->id}}' value='{{$item->unit}}'>
                                                </td>
                                                <td>
                                                    <button id="{{Auth::user()->id}}/{{$item->status}}" class="deleteCart close" data-id='{{$item->gst}}'>
                                                    ×
                                                    </button>
                                                </td>
                                            </tr>
                                        @endforeach
                                            
                                        </tbody>
                                    </table><!-- end table -->
                                </div><!-- end table-responsive -->
                                
                                <hr class="spacer-10 no-border">
                                
                                <a href="{{ URL::to('/product') }}" class="btn btn-light semi-circle btn-md pull-left">
                                    <i class="fa fa-arrow-left mr-5"></i> Continue shopping
                                </a>
                                @if($count !=0)
                                <a href="{{ URL::to('/checkout') }}" class="btn btn-default semi-circle btn-md pull-right" id='checkOut'>
                                    Checkout <i class="fa fa-arrow-right ml-5"></i>
                                </a>
                                @endif
                            </div><!-- end col -->
                        </div><!-- end row -->
                        @else
                        <h1>Cart is empty, add some items</h1>
                        @endif
                        @else
                        <h1 class='text-center'><a href='login'>Login</a> to add in Cart</h1>
                        @endif
                    </div>
                    </section>
@stop