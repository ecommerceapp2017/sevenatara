@extends('layout.public')
@section('content')


                    @if(Auth::user()->documents_uploaded=='')
                    <div class="col-sm-9">
                        <div class="row">
                            <div class="col-sm-12 text-left">
                                <h5 class="title">Upload Documents</h5>
                            </div><!-- end col -->
                        </div><!-- end row -->

                        <hr class="spacer-5"><hr class="spacer-20 no-border">

                        <div class="row">
                            <div class="row">
                    <div class="col-sm-4">
                        <div class="widget">
                            <div class="panel-group accordion" id="categoryFilter">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#categoryFilter" href="#categoryFilterCollapseOne">
                                                Business Details
                                            </a>
                                        </h3>
                                    </div>
                                    <div id="categoryFilterCollapseOne" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <ul class="list list-unstyled">
                                                <li>
                                                    <div class="checkbox-input checkbox-primary">
                                                    You need to provide your GSTIN, TAN and other business information
                                                    <br>
                                                    <br>
                                                    @if(Auth::user()->business_detail_id=='')
                                                    <center><a href="javascript:void(0);" class="btn btn-success round btn-lg" data-toggle="modal" data-target=".firstData">ADD DETAILS</a></center>
                                                    @else
                                                    <center><a href="javascript:void(0);" class="btn round btn-lg" >Business Details Updated</a></center>
                                                    @endif
                                                    </div>
                                                </li>
                                            </ul>
                                        </div><!-- end panel-body -->
                                    </div><!-- end panel-collapse -->
                                </div><!-- end panel -->
                            </div><!-- end accordion -->
                        </div><!-- end widget -->
                    </div><!-- end col -->

                    <div class="col-sm-4">
                        <div class="widget">
                            <div class="panel-group accordion" id="brandFilter">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#brandFilter" href="#brandFilterCollapseOne">
                                                Bank Details
                                            </a>
                                        </h3>
                                    </div>
                                    <div id="brandFilterCollapseOne" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <ul class="list list-unstyled">
                                                <li>
                                                    <div class="checkbox-input checkbox-primary">
                                                        We need your bank account details and KYC documents to verify your bank account
                                                        <br>
                                                        <br>
                                                    @if(Auth::user()->bank_detail_id=='')
                                                    <center><a href="javascript:void(0);" class="btn btn-success round btn-lg openSecond" data-toggle="modal" data-target=".secondData">ADD DETAILS</a></center>
                                                    @else
                                                    <center><a href="javascript:void(0);" class="btn round btn-lg" >Business Details Updated</a></center>
                                                    @endif
                                                    </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div><!-- end panel-body -->

                                    </div><!-- end panel-collapse -->

                                </div><!-- end panel -->

                            </div><!-- end accordion -->

                        </div><!-- end widget -->

                    </div><!-- end col -->
                    @if(Auth::user()->bank_detail_id!='' && Auth::user()->business_detail_id!='')
                    <a href="{{ URL::to('/post') }}" class="btn btn-success round btn-md pull-right"><i class="fa fa-heart mr-5"></i>Post Products</a>
                    @endif







                </div><!-- end row -->
                        </div><!-- end row -->
                    </div><!-- end col -->
                    @else
                    <div class="col-sm-9">
                        <div class="row">
                            <div class="col-sm-12 text-left">
                                <h5 class="title">Sell Product</h5>
                            </div><!-- end col -->
                        </div><!-- end row -->

                        <hr class="spacer-5"><hr class="spacer-20 no-border">

                        <div class="row">

                        @if(Auth::user()->aadhar_number!='')
                          <div class="col-md-12">
                                    <!-- Nav tabs --><div class="card">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#product" aria-controls="home" role="tab" data-toggle="tab">Products</a></li>
                                        <li role="presentation"><a href="#second" aria-controls="profile" role="tab" data-toggle="tab">Services</a></li>
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="product">

                                        <form>
                                            <div class='form-group'>
                                            <label for="exampleInputEmail1">Product Type</label>
                                                <ul>
                                                <li  style="list-style:none;">
                                                    <input type="radio" name="ptype" value="1" class='form-check-input' checked id='type' />
                                                    Perishable
                                                    <input type="radio" name="ptype" class='form-check-input' value="0" id="type" />
                                                    Non Perishable
                                                </li>
                                                </ul>
                                            </div>
                                            <div class="form-group">
                                            <label for="exampleInputEmail1">Category Type</label>
                                            <select class='form-control' id='c_type'>
                                            @foreach($data as $dat)
                                            <option value='{{$dat["id"]}}'>{{$dat['name']}}</option>
                                            @endforeach
                                            </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="email">Name :</label>
                                                <input type="email" class="form-control" id="name" placeholder="Enter name" name="email">
                                                <span class='err' id='name_err'>
                                            </div>
                                            <div class="form-group">
                                                <div class='row'>
                                                <div class='col-md-6'>
                                                <label for="email">Price / KG :</label>
                                                    <input type="email" class="form-control" id="price" placeholder="Price / KG" name="email">
                                                    <span class='err' id='price_err'>
                                                </div>
                                                <div class='col-md-6'>
                                                <label for="email">Order Limit :</label>
                                                    <input type="email" class="form-control" id="order" placeholder="Order Limit" name="email">
                                                    <span class='err' id='order_err'>
                                                </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class='row'>
                                                <div class='col-md-6'>
                                                <label for="email">Supply Capacity :</label>
                                                    <input type="email" class="form-control" id="supply" placeholder="Supply Capacity" name="email">
                                                    <span class='err' id='supply_err'>
                                                </div>
                                                <div class='col-md-6'>
                                                <label for="email">Rating :</label>
                                                    <input type="email" class="form-control" id="rating" placeholder="Rating 1- 5 " name="email">
                                                    <span class='err' id='rating_err'>
                                                </div>
                                                </div>
                                            </div>
                                            <div class='form-group'>
                                            <label for="email">Description :</label>
                                                <textarea rows="" cols="" class='form-control' placeholder='Description' id='description'></textarea>
                                                <span class='err' id='description_err'>
                                            </div>
                                            <div class='form-group'>
                                                <label for="email">Sample Available ? :</label>
                                                <select class="form-control" name="select" id='sampleData'>
                                                    <option id='0'>No</option>
                                                    <option id='1'>Yes</option>
                                                </select>
                                            </div>
                                            <div class='form-group' id='uploadForm'>
                                                <label for="email">Upload sample</label>
                                                <input type='file' class='form-control'>
                                            </div>
                                            <center>
                                            <a href="javascript:void(0);" id='postProduct' class="btn btn-default btn-lg">Post Product</a>
                                            </center>


                                        </form>

                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="second">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</div>
                                    </div>
                                    </div>
                                </div>

                        @else
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="firstname">Enter Aadhar Number <span class="text-danger">*</span></label>
                                    <input id="referal_code" type="text" placeholder="Enter Aadhar Number" name="firstname" class="form-control input-sm required">
                                </div><!-- end form-group -->
                                <div class="form-group">
                                        <div class="checkbox-input mb-10">
                                            <input id="show_balance" name="show_balance" class="styled" type="checkbox">
                                            <label for="show_balance">
                                                I agree for terms and conditions
                                            </label>
                                        </div><!-- end checkbox-input -->
                                </div><!-- end form-group -->
                                <div class="form-group">
                                    <a href="javascript:void(0);" id='aadharButton' class="btn btn-default round btn-md"><i class="fa fa-save mr-5"></i> Update</a>
                                </div><!-- end form-group -->
                            </div><!-- end col -->
                        @endif
                        </div><!-- end row -->
                    </div><!-- end col -->
                    @endif
                </div><!-- end row -->
            </div><!-- end container -->
        </section>
        <!-- end section -->

        <!-- Modal Product Review -->
                        <div class="modal fade productReview" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <h5>Upgrade as Lessor / Seller / Service Provider</h5>
                                    </div><!-- end modal-header -->
                                    <div class="modal-body">
                                        <!--
                                        <h6 class="subtitle">Lorem ipsum dolar sit amet</h6>
                                        <strong>How do you rate this product?</strong>

                                        <hr class="spacer-5 no-border">
                                        -->
                                        <form>
                                            <input type="text" class="rating rating-loading" value="2.5" data-size="sm" title="">
                                        </form>

                                        <hr class="spacer-10 no-border">

                                        <div class="form-group">
                                            <label for="reviewName">Referal Code</label>
                                            <input type="text" id="ref_code" class="form-control input-md" placeholder="Enter your Referal Code">
                                        </div><!-- end form-group -->
                                        <div class="form-group hide">
                                            <label for="reviewEmail">E-mail</label>
                                            <input type="text" id="reviewEmail" class="form-control input-md" placeholder="Your E-mail">
                                        </div><!-- end form-group -->
                                        <div class="form-group">
                                            <a href="javascript:;" class="product-image">
                                                <img src="" class="" style='height:100px;width:100px'>
                                            </a>

                                        </div><!-- end form-group -->
                                        <div class="form-group hide">
                                            <label for="reviewMessage">Review</label>
                                            <textarea id="reviewMessage" rows="5" class="form-control" placeholder="Your review"></textarea>
                                        </div><!-- end form-group -->
                                       <div class="form-group">
                                        <div class="checkbox-input mb-10">
                                            <input id="show_bal" name="show_bal" class="styled" type="checkbox">
                                            <label for="show_bal">
                                                I agree for terms and conditions
                                            </label>
                                        </div><!-- end checkbox-input -->
                                </div><!-- end form-group -->
                                        <div class="form-group">
                                            <input type="submit" class="btn btn-default btn-block round btn-md button big dark lessorbtn" value="Submit Review">
                                        </div><!-- end form-group -->
                                    </div><!-- end modal-body -->
                                </div><!-- end modal-content -->
                            </div><!-- end modal-dialog -->
                        </div><!-- end productRewiew -->

<!-- Model Content -->
<!-- Modal one -->
                        <div class="modal fade firstData" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                        <h4 class="modal-title" id="myModalLabel">Business Details</h4>
                                    </div><!-- end modal-header -->
                                    <div class="modal-body">
                                            <form>
                                            <div class="form-group">
                                            <label for="exampleInputEmail1">Company Name</label>
                                            <input type="text" class="form-control" id="b_name" placeholder="Company Name" name="email">
                                            <span class='error' id='b_name_error'></span>
                                            </div>
                                            <div class="form-group">
                                            <label for="exampleInputEmail1">Business Type</label>
                                            <input type="radio" name="b_type" id="radio3" value="option1">Wholesaler
                                            <input type="radio" name="b_type" id="radio3" value="option1">Retailer
                                            <input type="radio" name="b_type" id="radio3" value="option1">Distributor
                                            <input type="radio" name="b_type" id="radio3" value="option1">Manufacturer
                                            <input type="radio" name="b_type" id="radio3" value="option1">Others
                                            </div>
                                            <div class="form-group">
                                            <label for="exampleInputEmail1">GSTIN Number</label>
                                            <input type="text" class="form-control" id="b_gstin" placeholder="GSTIN Number" name="email">
                                            <span class='error' id='b_gstin_error'></span>
                                            </div>
                                            <div class="form-group">
                                            <label for="exampleInputEmail1">Company Address</label>
                                            <input class="form-control" id="b_address" placeholder="Company Address">
                                            <input type='hidden' id='b_lat'>
                                            <input type='hidden' id='b_lng'>
                                            <span class='error' id='b_address_error'></span>
                                            </div>
                                            <div class="form-group">
                                            <label for="exampleInputEmail1">PAN Number</label>
                                            <input type="text" class="form-control" id="b_pan_no" placeholder="PAN Number" name="email">
                                            <span class='error' id='b_pan_no_error'></span>
                                            </div>
                                            <div class="form-group">
                                            <label for="exampleInputEmail1">PAN Card Image</label>
                                            <?php
                                            if(Auth::user()->pan_image==''){
                                            $path = 'public/assets/img/avatar.png';
                                            }
                                            else{
                                            $path = Auth::user()->pan_image;
                                            }
                                            ?>
                                            <figure class="user-avatar medium">
											    <img class='panImg' alt="avatar" src='{{$path}}' style='height:65px;width:65px' >
                                                <span id='pan_pic' href="javascript:;" class="btn-md round">Upload Image</span>
                                                <span id='pan_status'></span>
                                            </figure>
                                            <span class="error" id="panUrlHTML"></span>
                                            </div>
                                            <div class="form-group">
                                            <label for="exampleInputEmail1">TAN Number</label>
                                            <input type="text" class="form-control" id="b_tan_no" placeholder="TAN Number" name="email">
                                            <span class='error' id='b_tan_no_error'></span>
                                            </div>
                                            <div class="form-group">
                                            <label for="exampleInputEmail1">Aadhar Number</label>
                                            <input type="text" class="form-control" placeholder="Aadhar Number" name="email" value="{{Auth::user()->aadhar_number}}" disabled>
                                            </div>
                                            <div class="form-group">
                                            <label for="exampleInputEmail1">Aadhar Image</label>
                                            <?php
                                            if(Auth::user()->aadhar_image==''){
                                            $path = 'public/assets/img/avatar.png';
                                            }
                                            else{
                                            $path = Auth::user()->aadhar_image;
                                            }
                                            ?>
                                            <figure class="user-avatar medium">
											    <img class='aadharImg' alt="avatar" src='{{$path}}' style='height:65px;width:65px' >
                                                <span id='aadhar_pic' href="javascript:;" class="btn-md round">Upload Image</span>
                                                <span id='aadhar_status'></span>
                                            </figure>
                                            <span class="error" id="aadharUrlHTML"></span>
                                            </div>
                                            <div class="form-group">
                                            <label for="exampleInputEmail1">Digital Image Signature</label>
                                            <?php
                                            if(Auth::user()->dig_image==''){
                                            $path = 'public/assets/img/avatar.png';
                                            }
                                            else{
                                            $path = Auth::user()->dig_image;
                                            }
                                            ?>
                                            <figure class="user-avatar medium">
											    <img class='digImg' alt="avatar" src='{{$path}}' style='height:65px;width:65px' >
                                                <span id='dig_pic' href="javascript:;" class="btn-md round">Upload Image</span>
                                                <span id='dig_status'></span>
                                            </figure>
                                            <span class="error" id="digUrlHTML"></span>
                                            <input type="hidden" id="aadharUrl" value="{{Auth::user()->aadhar_image}}">
                                            <input type="hidden" id="panUrl" value="{{Auth::user()->pan_image}}">
                                            <input type="hidden" id="digUrl" value="{{Auth::user()->dig_image}}">
                                            <input type="hidden" id="ppanUrl" value="{{Auth::user()->p_pan_image}}">
                                            <input type="hidden" id="addrUrl" value="{{Auth::user()->address_img}}">
                                            <input type="hidden" id="ccUrl" value="{{Auth::user()->cheque_img}}">
                                            </div>
                                            <div class='form-group' id='uploadForm'>
                                                <label for="email">Upload sample</label>
                                                <input type='file' class='form-control'>
                                            </div>
                                        </form>
                                    </div><!-- end modal-body -->
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-danger rounded" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-success rounded" id="saveBusiness">Save changes</button>
                                    </div><!-- end modal-body -->
                                </div><!-- end modal-content -->
                            </div><!-- end modal-dialog -->
                        </div><!-- end Modal Large -->

<!-- End of Modal one -->
<!-- Modal Two -->
                        <div class="modal fade secondData" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                        <h4 class="modal-title" id="myModalLabel">Bank Details</h4>
                                    </div><!-- end modal-header -->
                                    <div class="modal-body">
                                            <form>
                                            <div class="form-group">
                                            <label for="exampleInputEmail1">Account Holder Name</label>
                                            <input type="text" class="form-control" id="c_name" placeholder="Account Holder Name" name="email">
                                            <span class="error" id="c_name_error"></span>
                                            </div>
                                            <div class="form-group">
                                            <label for="exampleInputEmail1">Account Number</label>
                                            <input type="text" class="form-control" id="c_no" placeholder="Account Number" name="email">
                                            <span class="error" id="c_no_error"></span>
                                            </div>
                                            <div class="form-group hide">
                                            <label for="exampleInputEmail1">Confirm Account Number</label>
                                            <input type="text" class="form-control" id="name" placeholder="Confirm Account Number" name="email">
                                            </div>
                                            <div class="form-group">
                                            <label for="exampleInputEmail1">Bank Name</label>
                                            <input type="text" class="form-control" id="c_b_name" placeholder="Bank Name" name="email">
                                            <span class="error" id="c_b_name_error"></span>
                                            </div>
                                            <div class="form-group">
                                            <label for="exampleInputEmail1">IFSC Number</label>
                                            <a class='pull-right' href='https://bankifsccode.com/' target="_new">Click here to find your IFSC Code</a>
                                            <input type="text" class="form-control" id="c_ifsc" placeholder="IFSC Number" name="email">
                                            <span class="error" id="c_ifsc_error"></span>
                                            </div>
                                            <div class="form-group">
                                            <h5>KYC Documents</h5>
                                            <label for="exampleInputEmail1">Business Type</label>
                                            <select class='form-control' id='c_btype'>
                                            <option value='0'>Wholesaler</option>
                                            <option value='1'>Retailer</option>
                                            <option value='2'>Manufacturer</option>
                                            <option value='3'>Distributor</option>
                                            </select>
                                            <span class="error" id="c_btype_error"></span>
                                            </div>
                                            <div class="form-group">
                                            <label for="exampleInputEmail1">Personal PAN Card Number</label>
                                            <input type="text" class="form-control" id="c_p_pan" placeholder="Personal PAN Number" name="email">
                                            <span class="error" id="c_p_pan_error"></span>
                                            </div>
                                            <div class="form-group">
                                            <label for="exampleInputEmail1">Personal PAN Card Image</label>
                                            <?php
                                            if(Auth::user()->p_pan_image==''){
                                            $path = 'public/assets/img/avatar.png';
                                            }
                                            else{
                                            $path = Auth::user()->p_pan_image;
                                            }
                                            ?>
                                            <figure class="user-avatar medium">
											    <img class='ppanImg' alt="avatar" src='{{$path}}' style='height:65px;width:65px' >
                                                <span id='ppan_pic' href="javascript:;" class="btn-md round">Upload Image</span>
                                                <span id='ppan_status'></span>
                                            </figure>
                                            <span class="error" id="ppanUrlHTML"></span>
                                            </div>
                                            <div class="form-group">
                                            <label for="exampleInputEmail1">Personal Address</label>
                                            <input type="text" class="form-control" id="c_address" placeholder="Personal Address" name="email">
                                            <span class="error" id="c_address_error"></span>
                                            </div>
                                            <div class="form-group">
                                            <label for="exampleInputEmail1">Address Proof Image</label>
                                            <?php
                                            if(Auth::user()->address_img==''){
                                            $path = 'public/assets/img/avatar.png';
                                            }
                                            else{
                                            $path = Auth::user()->address_img;
                                            }
                                            ?>
                                            <figure class="user-avatar medium">
											    <img class='addrImg' alt="avatar" src='{{$path}}' style='height:65px;width:65px' >
                                                <span id='addr_pic' href="javascript:;" class="btn-md round">Upload Image</span>
                                                <span id='addr_status'></span>
                                            </figure>
                                            <span class="error" id="addrUrlHTML"></span>
                                            </div>
                                            <div class="form-group">
                                            <label for="exampleInputEmail1">Cancelled Image Proof</label>
                                            <?php
                                            if(Auth::user()->cheque_img==''){
                                            $path = 'public/assets/img/avatar.png';
                                            }
                                            else{
                                            $path = Auth::user()->cheque_img;
                                            }
                                            ?>
                                            <figure class="user-avatar medium">
											    <img class='chequeImg' alt="avatar" src='{{$path}}' style='height:65px;width:65px' >
                                                <span id='cheque_pic' href="javascript:;" class="btn-md round">Upload Image</span>
                                                <span id='cheque_status'></span>
                                            </figure>
                                            <span class="error" id="ccUrlHTML"></span>
                                            </div>
                                            <div class="form-group">
                                            <label for="exampleInputEmail1">Note</label>
                                            <textarea class="form-control" placeholder="Note"></textarea>
                                            </div>
                                        </form>
                                    </div><!-- end modal-body -->
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-danger rounded" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-success rounded" id="saveBank">Save changes</button>
                                    </div><!-- end modal-body -->
                                </div><!-- end modal-content -->
                            </div><!-- end modal-dialog -->
                        </div><!-- end Modal Large -->

<!-- End of Modal two -->
<!-- Modal Three -->
                        <div class="modal fade thirdData" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                        <h4 class="modal-title" id="myModalLabel">Store Details</h4>
                                    </div><!-- end modal-header -->
                                    <div class="modal-body">
                                            <form>
                                            <div class="form-group">
                                            <label for="exampleInputEmail1">Display Name</label>
                                            <input type="text" class="form-control" id="name" placeholder="Display Name" name="email">
                                            </div>
                                            <div class="form-group">
                                            <label for="exampleInputEmail1">Business Description</label>
                                            <textarea class="form-control" placeholder="Business Description"></textarea>
                                            </div>
                                        </form>
                                    </div><!-- end modal-body -->
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-danger rounded" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-success rounded">Save changes</button>
                                    </div><!-- end modal-body -->
                                </div><!-- end modal-content -->
                            </div><!-- end modal-dialog -->
                        </div><!-- end Modal Large -->

<!-- End of Modal Three -->
<!-- End of Model Content -->

<script src="{{ asset('/').('public/assets/js/custom/user.js') }}"></script>
<script src="{{ asset('/').('public/assets/js/custom/s3/ajaxupload.3.5.js') }}"></script>
<script src="{{ asset('/').('public/assets/js/custom/s3/s3_aad.js') }}"></script>
<script src="{{ asset('/').('public/assets/js/custom/s3/s3_pan.js') }}"></script>
<script src="{{ asset('/').('public/assets/js/custom/s3/s3_ppan.js') }}"></script>
<script src="{{ asset('/').('public/assets/js/custom/s3/s3_addr.js') }}"></script>
<script src="{{ asset('/').('public/assets/js/custom/s3/s3_cheque.js') }}"></script>
<script src="{{ asset('/').('public/assets/js/custom/s3/s3_dig.js') }}"></script>

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDMYk26GLWUnUvtvmRwMRdtY-MenRbrvjA&libraries=places&sensor=false"></script>
<script type="text/javascript">
google.maps.event.addDomListener(window, 'load', function () {
var places = new google.maps.places.Autocomplete(document.getElementById('b_address'));
google.maps.event.addListener(places, 'place_changed', function () {
var place = places.getPlace();
var address = place.formatted_address;
var mesg = "address: " + address;
var appUrl = Cookies.get('appUrl');
$.post(appUrl + "googleplace", {  address: address })
    .done(function(data) {
        if (data.success == 1) {
            console.log(data.data);
            var res = data.data.split(",");
            $('#b_lat').val(res[0]);
            $('#b_lng').val(res[1]);
        } else {
            console.log(data.message);
        }
    });

});
});</script>
<style>
    .pac-container {
        z-index: 10000 !important;
    }
</style>
@stop
