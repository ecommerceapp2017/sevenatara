@extends('layout.public')
@section('content')


                    <div class="col-sm-9">
                        <div class="row hide">
                            <div class="col-sm-12 text-left">
                                <h2 class="title">My Wishlist</h2>
                            </div><!-- end col -->
                        </div><!-- end row -->
                        
                        
                        
                        <div class='row'>
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#product" aria-controls="home" role="tab" data-toggle="tab">Products</a></li>
                            <li role="presentation"><a href="#second" aria-controls="profile" role="tab" data-toggle="tab">Services</a></li>
                        </ul>
                            <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="product">
<?php
if(count($product)>0)
foreach ($product as $key) {
?>
<div class="col-sm-6 col-md-3">
                        <div class="thumbnail blog">
                            <div class="header">
@if($key->status=='1')
<div class="alert alert-info alert-dismissible" role="alert">                            
<strong>Pending Approval</strong>
</div>
@elseif($key->status==2)
<div class="alert alert-success alert-dismissible" role="alert">                            
<strong>Approved</strong>
</div>
@elseif($key->status==3)
<div class="alert alert-danger alert-dismissible" role="alert">                            
<strong>Rejected</strong>
</div>
@elseif($key->status==4)
<div class="alert alert-warning alert-dismissible" role="alert">                            
<strong>Re-Approved</strong>
</div>
@endif
                                <a href="viewproduct/{{$key->id}}">
                                <figure>
                                    <img src="{{$key->image}}" alt="">
                                </figure>
                                </a>
                            </div>
                            <div class="caption">
                                <h6><a href="viewproduct/{{$key->id}}">{{$key->name}}</a></h6>
                                <a href="viewproduct/{{$key->id}}" class="btn btn-default semi-circle btn-sm">View/Edit</a>
                                <a href="deleteproduct/{{$key->id}}" class="btn btn-danger semi-circle btn-sm">Delete</a>
                            </div><!-- end caption -->
                        </div><!-- end thumbnail -->
                    </div>


<?php
}
else
{
echo "<h2>No Products posted</h2>";
}
?>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="second">
                            <?php
if(count($service)>0)
foreach ($service as $key) {
?>
<div class="col-sm-6 col-md-3">
                        <div class="thumbnail blog">
                            <div class="header">
@if($key->status=='1')
<div class="alert alert-info alert-dismissible" role="alert">                            
<strong>Pending Approval</strong>
</div>
@elseif($key->status==2)
<div class="alert alert-success alert-dismissible" role="alert">                            
<strong>Approved</strong>
</div>
@elseif($key->status==3)
<div class="alert alert-danger alert-dismissible" role="alert">                            
<strong>Rejected</strong>
</div>
@elseif($key->status==4)
<div class="alert alert-warning alert-dismissible" role="alert">                            
<strong>Re-Approved</strong>
</div>
@endif
                                <figure>
                                <a href="viewservice/{{$key->id}}">
                                    <img src="public/assets/img/blog/blog_01.jpg" alt="">
                                    </a>
                                </figure>
                            </div>
                            <div class="caption">
                            <h6><a href="viewservice/{{$key->id}}">{{$key->name}}</a></h6>
                                <a href="viewservice/{{$key->id}}" class="btn btn-default semi-circle btn-sm">View/Edit</a>
                                <a href="viewservice/{{$key->id}}" class="btn btn-danger semi-circle btn-sm">Delete</a>
                            </div><!-- end caption -->
                        </div><!-- end thumbnail -->
                    </div>


<?php
}
else
{
echo "<h2>No Services posted</h2>";
}
?>
                            </div>
                            </div>

                        </div>
                    </div><!-- end col -->
                </div><!-- end row -->                
            </div><!-- end container -->
        </section>
        <!-- end section -->

<!-- End of Modal Three -->      
<!-- End of Model Content -->
<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
<script src="{{ asset('/').('public/assets/js/jquery.dataTables.min.js') }}"></script>
@stop