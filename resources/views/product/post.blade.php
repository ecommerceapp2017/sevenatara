<?php
namespace App;
use App\ServiceDetail;
use Config;
use URL;
use Auth;
?>
<?php
$color = Config::get('db.color.color');

?>
    @extends('layout.public') @section('content')


                <!-- start of all form -->
                <div class="col-sm-9">
                    <div class="row">
                        <div class="col-sm-12 text-left">
                            <h5 class="title">Sell Product</h5>
                        </div>
                        <!-- end col -->
                    </div>
                    <!-- end row -->
                    <!-- start of basic check -->
                    @if(Auth::user()->aadhar_number!='')
                    <div class="col-md-12">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#productTab" aria-controls="productTab" role="productTab" data-toggle="tab">Products</a></li>
                            <li role="presentation"><a href="#serviceTab" aria-controls="serviceTab" role="serviceTab" data-toggle="tab">Services</a></li>
                        </ul>
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="productTab">
                                <form>
                                    <div class='form-group'>
                                        <label for="exampleInputEmail1">Product Type</label>
                                        <ul>
                                            <li style="list-style:none;">
                                                <input type="radio" name="p_p_type" value="1" class='form-check-input' checked id='type' /> Perishable
                                                <input type="radio" name="p_p_type" class='form-check-input' value="2" id="type" /> Non Perishable
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Category Type</label>
                                        <select class='form-control' id='p_cat_type'>
                                            <option selected disabled value="">Choose Category</option>
                                            @foreach($data as $dat)
                                            <option value='{{$dat["id"]}}'>{{$dat['name']}}</option>
                                            @endforeach
                                        </select>
                                        <span class='err' id='p_cat_type_err'>
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleInputEmail1s">Sub Category Type</label>
                                        <select class='form-control' id='p_sb_type'>
                                        <option selected disabled value=""> Choose Sub Category</option>
                                        </select>
                                        <span class='err' id='p_sb_type_err'>
                                    </div>

                                    <div class="form-group">
                                        <label for="email">Product Name :</label>
                                        <input type="email" class="form-control" id="p_p_name" placeholder="Product Name" name="email">
                                        <span class='err' id='p_p_name_err'>
                                    </div>

                                    <div class="form-group">
                                      <div class='row'>
                                      <div class='col-md-3'>
                                        <label for="email">Product Length :</label>
                                        <input type="email" class="form-control" id="p_p_length" placeholder="Product Length" name="email">
                                        <span class='err' id='p_p_length_err'>
                                      </div>
                                      <div class='col-md-3'>
                                        <label for="email">Product Breadth :</label>
                                        <input type="email" class="form-control" id="p_p_breadth" placeholder="Product Breadth" name="email">
                                        <span class='err' id='p_p_breadth_err'>
                                      </div>
                                      <div class='col-md-3'>
                                        <label for="email">Product Height :</label>
                                        <input type="email" class="form-control" id="p_p_height" placeholder="Product Height" name="email">
                                        <span class='err' id='p_p_height_err'>
                                      </div>
                                      <div class='col-md-3'>
                                        <label for="email">Product Weight :</label>
                                        <input type="email" class="form-control" id="p_p_weight" placeholder="Product Weight" name="email">
                                        <span class='err' id='p_p_weight_err'>
                                      </div>
                                      </div>
                                    </div>

                                    <div class="form-group">
                                    <div class='row'>
                                    <div class='col-md-3'>
                                    <label for="email">Price per unit :</label>
                                    <input type="email" class="form-control" id="p_p_price" placeholder="Price per unit" name="email">
                                    <span class='err' id='p_p_price_err'>
                                    </div>
                                    <div class='col-md-6'>
                                    <label for="email">Unit Type :</label>
                                    <select class='form-control' id='p_p_unit_type'>
                                    <option value='0'>kilogram</option><option value='1'>numbers</option><option value='2'>rolls</option><option value='3'>pieces</option><option value='4'>tons</option><option value='5'>units</option><option value='6'>20’ container</option><option value='7'>40’ container</option><option value='8'>bag</option><option value='9'>barrel</option><option value='10'>bottle</option><option value='11'>bushel</option><option value='12'>carton</option><option value='13'>dozen</option><option value='14'>foot</option><option value='15'>gallon</option><option value='16'>gram</option><option value='17'>hectare</option><option value='18'>kilometer</option><option value='19'>meter</option><option value='20'>kilowatt</option><option value='21'>litre</option><option value='22'>metric tons</option><option value='23'>long ton</option><option value='24'>ounce</option><option value='25'>packets</option><option value='26'>packs</option><option value='27'>pair</option><option value='28'>ream</option><option value='29'>pound</option><option value='30'>set</option><option value='31'>sheet</option><option value='32'>short ton</option><option value='33'>sq. ft.</option><option value='34'>sq. metre</option><option value='35'>watt</option><option value='36'>Others</option>
                                    </select>
                                    </div>
                                    <div class='col-md-3' id='othersTab'>
                                    <label for="email">Unit ( Others ) :</label>
                                    <input type="email" class="form-control" id="p_p_others" placeholder="Others" name="email">
                                    <span class='err' id='p_p_others_err'>
                                    </div>
                                    </div>
                                    </div>
                            <div class="form-group">
                            <div class='row'>
                            <div class='col-md-6'>
                            <label for="email">Select GST Applicable :</label>
                            <select class='form-control' id='p_p_gst'>
                            <option value='1'>5 % </option>
                            <option value='2'>12 % </option>
                            <option value='3'>18 % </option>
                            <option value='4'>28 % </option>
                            </select>
                            <a href ="https://www.indiafilings.com/find-gst-rate" target="_new" style='color:blue'>Find GST of your product here</a>
                            </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <div class='row'>
                            <div class='col-md-6'>
                            <label for="email">Supply Capacity :</label>
                            <input type="email" class="form-control" id="p_p_supply_capacity" placeholder="Supply Capacity" name="email">
                            <span class='err' id='p_p_supply_capacity_err'>
                            </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <div class='row'>
                            <label for="email" class='pull-left'>Add Color :</label>
                            <div class='col-md-6'>
                            <div class="checkbox col-md-6">
                            <input type="checkbox" id='assorted' class='checkbox-inline un_col' name="color[]" value="0">Assorted Color</label>
                            </div>
                            <div class="checkbox col-md-6">
                            <?php
                            foreach ($color as $key => $value) {
                                ?>

                                <label><input type="checkbox"  class='checkbox-inline oth_col' name="color[]" value="{{$key}}">{{$value}}</label>

                                <?php
                            }
                            ?>
                            </div>
                            <br>
                            <!--<input type="email" class="form-control" id="p_p_color" placeholder="Add Color" name="email">-->
                            <span class='err' id='p_p_color_err'>
                            </div>
                            </div>
                            <div class='row'>
                            <div class='col-md-6'>
                            <label for="email">Add Variety :</label>
                            <input type="email" class="form-control" id="p_p_variety" placeholder="Add Variety" name="email">
                            <span class='err' id='p_p_variety_err'>
                            </div>
                            </div>
                            <div class='row'>
                            <div class='col-md-6'>
                            <label for="email">Material Type :</label>
                            <input type="email" class="form-control" id="p_p_material_type" placeholder="Material Type" name="email">
                            <span class='err' id='p_p_material_type_err'>
                            </div>
                            </div>
                            </div>
                            <div class='form-group'>
                            <label for="email">Description :</label>
                            <textarea rows="" cols="" class='form-control' placeholder='Description' id='p_p_description'></textarea>
                            <span class='err' id='p_p_description_err'>
                            </div>
                            <div class="form-group">
                            <div class='row'>
                            <div class='col-md-6'>
                            <label for="email">Minimum Order Quantity (MOQ) : - <span class='unitChosen'></span></label>
                            <input type="email" class="form-control" id="p_p_moq" placeholder="Minimum Order Quantity (MOQ)">
                            <span class='err' id='p_p_moq_err'>
                            </div>
                            <div class='col-md-6'>
                            <label for="email">Returning Quantity : - <span class='unitChosen'></span></label>
                            <input type="email" class="form-control" id="p_p_returns" placeholder="Returning Quantity" name="email">
                            <span class='err' id='p_p_returns_err'></span>
                            <span class='err' id='p_p_r_m'></span>
                            </div>
                            </div>
                            </div>
                            <!--<div class='form-group'>
                            <label for="email">Sample Available ? :</label>
                            <div class="radio-input radio-primary">
                            <input type="radio" name="p_p_sample" id="" value="1">
                            <label for="radio4">
                            Available
                            </label>
                            <input type="radio" name="p_p_sample" id="radio4" value="0" checked>
                            <label for="radio4">
                            Not Available
                            </label>
                            </div>
                            </div>-->
                            <div class='form-group'>
                            <label for="email">Warranty ? :</label>
                            <input type="radio" name="warranty_data" checked id="warranty_yes" value="0" class="radio-input radio-primary warranty_data">Yes
                            <input type="radio" name="warranty_data" id="warranty_no" value="1" class="radio-input radio-primary warranty_data">No
                            <figure class="user-avatar medium war_pic">
              								<img class="warantee" alt="avatar" src="public/assets/img/avatar.png" style="height:65px;width:65px">
                                              <span id="warantee_pic" href="javascript:;" class="button mid-short dark-light ">Upload Image</span>
                                              <span id="warantee_status"></span>
                                              <input type="hidden" id="warantee">
              							</figure>
                            <span class='err' id='warantee_err'>
                            </div>
                            <div class='form-group'>
                            <label for="email">Guarantee ? :</label>
                            <input type="radio" name="guarantee_data" checked id="guarantee_yes" value="0" class="radio-input radio-primary guarantee_data">Yes
                            <input type="radio" name="guarantee_data" id="guarantee_no" value="1" class="radio-input radio-primary guarantee_data">No
                            <!--<div class="radio-input radio-primary">
                            <input type="radio" name="p_p_gurantee" id="radio4" value="1">
                            <label for="radio4">
                            Yes
                            </label>
                            <input type="radio" name="p_p_gurantee" id="radio4" value="0" checked>
                            <label for="radio4">
                            No
                            </label>-->
                            <figure class="user-avatar medium gua_pic">
								<img class="gurantee" alt="avatar" src="public/assets/img/avatar.png" style="height:65px;width:65px">
                                <span id="gurantee_pic" href="javascript:;" class="button mid-short dark-light ">Upload Image</span>
                                <span id="gurantee_status"></span>
                                <input type="hidden" id="gurantee">
							</figure>
                            <span class='err' id='gurantee_err'>
                            <!--</div>-->
                            </div><hr>
                            <label for="email">Product Image:</label>
                            <div class='form-group' id='uploadForms'>
                            <figure class="user-avatar medium">
								<img class="u_p_p_i" alt="avatar" src="public/assets/img/avatar.png" style="height:65px;width:65px">
                                <span  href="javascript:;" class="u_p_p button cat_pic mid-short dark-light ">Upload Product Image 1</span>
                                <span id="u_p_p_s"></span>
                                <input type="hidden" id="u_p_p_im">
							</figure>
                            <span class='err' id='u_p_p_im_err'>
                            </div>
                            <div class='form-group' id='uploadForms'>
                            <figure class="user-avatar medium">
								<img class="u_p_p_i_1" alt="avatar" src="public/assets/img/avatar.png" style="height:65px;width:65px">
                                <span  href="javascript:;" class="u_p_p_1 button cat_pic_1 mid-short dark-light ">Upload Product Image 2</span>
                                <span id="u_p_p_s_1"></span>
                                <input type="hidden" id="u_p_p_im_1">
							</figure>
                            <span class='err' id='u_p_p_im_1_err'>
                            </div>
                            <div class='form-group' id='uploadForms'>
                            <figure class="user-avatar medium">
								<img class="u_p_p_i_2" alt="avatar" src="public/assets/img/avatar.png" style="height:65px;width:65px">
                                <span  href="javascript:;" class="u_p_p_2 button cat_pic_2 mid-short dark-light ">Upload Product Image 3</span>
                                <span id="u_p_p_s_2"></span>
                                <input type="hidden" id="u_p_p_im_2">
							</figure>
                            <span class='err' id='u_p_p_im_2_err'>
                            </div>
                            <div class='form-group' id='uploadForms'>
                            <figure class="user-avatar medium">
								<img class="u_p_p_i_3" alt="avatar" src="public/assets/img/avatar.png" style="height:65px;width:65px">
                                <span  href="javascript:;" class="u_p_p_3 button cat_pic_3 mid-short dark-light ">Upload Product Image 4</span>
                                <span id="u_p_p_s_3"></span>
                                <input type="hidden" id="u_p_p_im_3">
							</figure>
                            <span class='err' id='u_p_p_im_3_err'>
                            </div>
                            <div class='form-group' id='uploadForms'>
                            <figure class="user-avatar medium">
								<img class="u_p_p_i_4" alt="avatar" src="public/assets/img/avatar.png" style="height:65px;width:65px">
                                <span  href="javascript:;" class="u_p_p_4 button cat_pic_4 mid-short dark-light ">Upload Product Image 5</span>
                                <span id="u_p_p_s_4"></span>
                                <input type="hidden" id="u_p_p_im_4">
							</figure>
                            <span class='err' id='u_p_p_im_4_err'>
                            </div>
                            <hr>
                            <div class='form-group'>
                            <div class="checkbox-input checkbox-primary">
                            <input id="checkbox2" class="styled" type="checkbox" checked="">
                            <label for="checkbox2">
                            I there by agree to the TERMS & CONDITIONS of 7ATARA
                            </label>
                            </div>
                            </div>
                            <center>
                            <a href="javascript:void(0);" id='postProduct' class="btn btn-default btn-lg">Post Product</a>
                            </center>
                            </form>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="serviceTab">
                            <?php
                            $showServicePost = 0;
                            $dataExist = ServiceDetail::where('user_id', Auth::user()->id)->first();
                            if($dataExist)
                            {
                            //if($dataExist['status']=='1')
                            $showServicePost = 1;
                            if(1==1)
                            {
                            echo "<h2 class='hide'>Service Detail Document Pending </h2>";
                            }
                            else if($dataExist['status']=='3')
                            {
                            echo "<h2 class='hide'>Service Detail Rejected </h2>";
                            }
                            else if($dataExist['status']=='4')
                            {
                            echo "<h2 class='hide'>Service Detail Suspended </h2>";
                            }
                            else {
                                $showServicePost = 1;
                            }
                            }
                            ?>
                            <?php

                            if(Auth::user()->role_service_user!=''){
                            ?>

                            <form>
                            <!-- start of service -->
                            <div class='formService'>

                            <div class="form-group">
                            <label for="email">Name :</label>
                            <input type="email" class="form-control" id="s_name" placeholder="Name" name="email">
                            <span class='err' id='s_name_err'>
                            </div>

                            <div class="form-group">
                            <label for="email">Description :</label>
                            <textarea rows="5" class="form-control" cols="10" id='s_desc' placeholder='Description'></textarea>
                            <span class='err' id='s_desc_err'>
                            </div>

                            <div class="form-group">

                            <div class='row'>

                            <div class='col-md-4'>
                            <label for="email">Price per Hour :</label>
                            <input type="email" class="form-control" id="s_hour" placeholder="/hour" name="email">
                            <span class='err' id='s_hour_err'>
                            </div>

                            <div class='col-md-4'>
                            <label for="email">Price per Day:</label>
                            <input type="email" class="form-control" id="s_day" placeholder="/day" name="email">
                            <span class='err' id='s_day_err'>
                            </div>

                            <div class='col-md-4'>
                            <label for="email">Price per Week:</label>
                            <input type="email" class="form-control" id="s_week" placeholder="/week" name="email">
                            <span class='err' id='s_week_err'>
                            </div>

                            <div class='col-md-4'>
                            <label for="email">Price per Month:</label>
                            <input type="email" class="form-control" id="s_month" placeholder="/month" name="email">
                            <span class='err' id='s_month_err'>
                            </div>

                            <div class='col-md-4'>
                            <label for="email">Price per Year:</label>
                            <input type="email" class="form-control" id="s_year" placeholder="/year" name="email">
                            <span class='err' id='s_year_err'>
                            </div>

                            </div>

                            </div>

                            <div class='form-group'>
                            <div class="checkbox-input checkbox-primary">
                            <input id="checkbox2" class="styled" type="checkbox" checked="">
                            <label for="checkbox2">
                            I there by agree to the TERMS & CONDITIONS of 7ATARA
                            </label>
                            </div>
                            </div>

                            <center>
                            <a href="javascript:void(0);" id='postService' class="btn btn-default btn-lg">Post Service Detail</a>
                            </center>
                            </div>
                            <!-- end of service -->
                            </form>

                            <?php
                            }else{
                            ?>


                                    <h5>Upgrade as seller to Post Services</h5>
                                    <p>Click <a href='upgrade'>here</a> to upgrade as seller</p>
                            <?php
                            }
                            ?>
                        </div>
                    </div>
                    </div>
                    @endif
                    <!-- end of basic check -->

                    </div>
                    <!-- end of all form -->

                </div><!-- end row -->
            </div><!-- end container -->
        </section>
        <!-- end section -->

<script src="{{ asset('/').('public/assets/js/custom/user.js') }}"></script>
<script src="{{ asset('/').('public/assets/js/custom/s3/ajaxupload.3.5.js') }}"></script>
<script src="{{ asset('/').('public/assets/js/custom/s3/s3_post.js') }}"></script>
<script src="{{ asset('/').('public/assets/js/custom/s3/s3_post1.js') }}"></script>
<script src="{{ asset('/').('public/assets/js/custom/s3/s3_post2.js') }}"></script>
<script src="{{ asset('/').('public/assets/js/custom/s3/s3_post3.js') }}"></script>
<script src="{{ asset('/').('public/assets/js/custom/s3/s3_post4.js') }}"></script>
<script src="{{ asset('/').('public/assets/js/custom/s3/s3_warantee.js') }}"></script>
<script src="{{ asset('/').('public/assets/js/custom/s3/s3_gurantee.js') }}"></script>

@stop
