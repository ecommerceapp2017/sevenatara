<?php
namespace App;
use Config;
use Auth;
$color = Config::get('db.color.color');

?>
@extends('layout.public')
@section('content')
        
        
        
                
        <!-- start section -->
        <section class="section white-backgorund">
            <div class="container">
                <div class="row">
                    <!-- start sidebar -->
                    <div class="col-sm-4">
                        <div class='carousel slide product-slider' data-ride='carousel' data-interval="false">
                            <div class='carousel-inner'>
                                <div class='item active'>
                                    <figure>
                                        <img src='{{$product->image}}' alt='' class="product-img-detail"/>
                                    </figure>
                                </div><!-- end item -->
                                

                                <!-- Arrows -->
                                <a class='left carousel-control' id='left'  data-slide='prev'>
                                    <span class='fa fa-angle-left'></span>
                                </a>
                                <a class='right carousel-control' id='right' data-slide='next'>
                                    <span class='fa fa-angle-right'></span>
                                </a>
                            </div><!-- end carousel-inner -->

                            <!-- thumbs -->
                            <input type='hidden' class='1' value='{{$product->image}}'>
                            <input type='hidden' class='2' value='{{$product->image1}}'>
                            <input type='hidden' class='3' value='{{$product->image2}}'>
                            <input type='hidden' class='4' value='{{$product->image3}}'>
                            <input type='hidden' class='5' value='{{$product->image4}}'>

                            <ol class='carousel-indicators mCustomScrollbar meartlab'>
                                <li data-target='.product-slider' data-slide-to='0' id='img_1' class='active img'><img src='{{$product->image}}' alt='' /></li>
                                <li data-target='.product-slider' data-slide-to='0' id='img_2' class='img'><img src='{{$product->image1}}' alt='' /></li>
                                <li data-target='.product-slider' data-slide-to='0' id='img_3' class='img'><img src='{{$product->image2}}' alt='' /></li>
                                <li data-target='.product-slider' data-slide-to='0' id='img_4' class='img'><img src='{{$product->image3}}' alt='' /></li>
                                <li data-target='.product-slider' data-slide-to='0' id='img_5' class='img'><img src='{{$product->image4}}' alt='' /></li>
                            </ol><!-- end carousel-indicators -->
                        </div><!-- end carousel -->
                    </div><!-- end col -->
                    <!-- end sidebar -->
                    <div class="col-sm-8">
                        <div class="row">
                            <div class="col-sm-12">
                                <h2 class="title">{{$product->name}}</h2>
                                    <p class="text-gray alt-font">Product code: #{{$product->id}}</p>
                                    
                                    <ul class="list list-inline">
                                        <li><h5 class="text-primary">Rs.{{$product->price}}</h5></li>
                                        <li>
                                            <i class="fa fa-star text-warning"></i>
                                            <i class="fa fa-star text-warning"></i>
                                            <i class="fa fa-star text-warning"></i>
                                            <i class="fa fa-star text-warning"></i>
                                            <i class="fa fa-star-half-o text-warning"></i>
                                        </li>
                                        <li class="hide"><a href="javascript:void(0);">(0 reviews)</a></li>
                                    </ul>
                            </div><!-- end col -->
                        </div><!-- end row -->
                        
                        <hr class="spacer-5"><hr class="spacer-10 no-border">
                        
                        <div class="row">
                            <div class="col-sm-12">
                                <p>{{$product->description}}</p>
                                <ul class="list alt-list hide">
                                    <li><i class="fa fa-check"></i> Lorem Ipsum dolor sit amet</li>
                                    <li><i class="fa fa-check"></i> Cras aliquet venenatis sapien fringilla.</li>
                                    <li><i class="fa fa-check"></i> Duis luctus erat vel pharetra aliquet.</li>
                                </ul>
                                <hr class="spacer-15">
                                <div class="row">
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <select class="form-control" name="select">
                                            <option value="" selected>Select Color</option>
                                            <?php
                                                foreach ($color as $key => $value) {
                                            ?>
                                            <?php
                                                $co = $product->color;
                                                $c = explode(',',$co);
                                                foreach($c as $b){
                                                    if($b==$key){
                                                        ?>
                                                        <option value="{{$key}}">{{$value}}</option>
                                                        <?php
                                                    }
                                                }
                                            ?>
                                            
                                            <?php
                                                }
                                            ?>
                                        </select>
                                    </div><!-- end col -->
                                    <div class="col-md-4 col-sm-6 col-xs-12 hide">
                                        <select class="form-control" name="select">
                                            <option value="">Size</option>
                                            <option value="">S</option>
                                            <option value="">M</option>
                                            <option value="">L</option>
                                            <option value="">XL</option>
                                            <option value="">XXL</option>
                                        </select>
                                    </div><!-- end col -->
                                    <div class="col-md-4 col-sm-12">
                                        <select class="form-control single-item" name="select" id='single-item'>
                                            <?php
                                                for ($i=1; $i <= 10 ; $i++) { 
                                                    echo "<option value='".$i."'>".$i."</option>";
                                                }
                                            ?>
                                        </select>
                                    </div><!-- end col -->
                                </div><!-- end row -->
                                <hr class="spacer-15">
                                
                                <ul class="list list-inline">
                                    <?php
                                        if (Auth::check()) {
                                            ?>
                                            <li><button type="button" class="btn btn-default btn-md round addCart" id='{{Auth::user()->id}}|{{$product->id}}'><i class="fa fa-shopping-basket mr-5"></i>Add to Cart</button></li>
                                            <?php
                                        }else{
                                            ?>
                                            <li>
                                            <a href='../login'>
                                            <button type="button" class="btn btn-default btn-md round"><i class="fa fa-shopping-basket mr-5"></i>Login to add to Cart</button>
                                            </a>
                                            </li>
                                            <?php
                                        }
                                    ?>
                                    
                                    <li class='hide'><button type="button" class="btn btn-gray btn-md round"><i class="fa fa-heart mr-5"></i>Add to Wishlist</button></li>
                                    <li>Share this product: </li>
                                    <li>
                                        <ul class="social-icons style1">
                                            <li class="facebook"><a href="javascript:void(0);"><i class="fa fa-facebook"></i></a></li>
                                            <li class="twitter"><a href="javascript:void(0);"><i class="fa fa-twitter"></i></a></li>
                                            <li class="pinterest"><a href="javascript:void(0);"><i class="fa fa-pinterest"></i></a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div><!-- end col -->
                        </div><!-- end row -->
                    </div><!-- end col -->
                </div><!-- end row -->
                
                <hr class="spacer-60">
                
                <div class="row">
                    <div class="col-xs-12 col-sm-3">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs style2 tabs-left">
                            <li class="active"><a href="#description" data-toggle="tab">Additional Info</a></li>
                            <li class="hide"><a href="#reviews" data-toggle="tab">Reviews (4)</a></li>
                        </ul>
                    </div><!-- end col -->
                    <div class="col-xs-12 col-sm-9">
                        <!-- Tab panes -->
                        <div class="tab-content style2">
                            <div class="tab-pane active" id="description">
                                <h5>Additional Info</h5>
                                <p class="hide">Vestibulum tellus justo, vulputate ac nunc eu, laoreet pellentesque erat. 
                                    Nulla in fringilla ex. Nulla finibus rutrum lorem vehicula facilisis. 
                                    Sed ornare congue mi, et volutpat diam. Suspendisse eget augue id magna placerat dignissim. 
                                    Fusce at turpis neque. Nullam commodo consequat risus et iaculis.
                                </p>
                                
                                <hr class="spacer-15">
                                
                                <div class="row">
                                    <div class="col-sm-12 col-md-6">
                                        <dl class="dl-horizontal">
                                            <dt>Supply Capacity</dt>
                                            <dd>{{$product->supply_capacity}}</dd>
                                            <dt>Colors</dt>
                                            <dd>{{$product->color}}</dd>
                                            <?php
                                            if($product->gurantee=="0"){
                                                $g = "Yes";
                                            }
                                            else{
                                                $g="No";
                                            }
                                            if($product->warranty=="0"){
                                                $w = "Yes";
                                            }
                                            else{
                                                $w="No";
                                            }
                                            ?>
                                            <dt>Gurantee</dt>
                                            <dd>{{$g}}</dd>
                                            <dt>Warranty</dt>
                                            <dd>{{$w}}</dd>
                                        </dl>
                                    </div><!-- end col -->
                                    <div class="col-sm-12 col-md-6">
                                        <dl class="dl-horizontal">
                                            <dt>Returns</dt>
                                            <dd>{{$product->returns}}</dd>
                                            <dt>Material Type</dt>
                                            <dd>{{$product->material_type}}</dd>
                                            <dt>Variety</dt>
                                            <dd>{{$product->variety}}</dd>
                                        </dl>
                                    </div><!-- end col -->
                                </div><!-- end row -->
                            </div><!-- end tab-pane -->
                            <div class="tab-pane hide" id="reviews">
                                <h5>4 Review for "Lorem ipsum dolor sit amet"</h5>
                                
                                <hr class="spacer-10 no-border">
                                
                                <div class="comments">
                                    <div class="comment-image">
                                        <figure>
                                            <img src="../public/assets/img/team/team_01.jpg" alt="" />
                                        </figure>
                                    </div><!-- end comments-image -->
                                    <div class="comment-content">
                                        <div class="comment-content-head">
                                            <h6 class="comment-title">Lorem ipsum dolor sit</h6>
                                            <ul class="list list-inline comment-meta">
                                                <li>
                                                    <i class="fa fa-star text-warning"></i>
                                                    <i class="fa fa-star text-warning"></i>
                                                    <i class="fa fa-star text-warning"></i>
                                                    <i class="fa fa-star text-warning"></i>
                                                    <i class="fa fa-star text-warning"></i>
                                                </li>
                                            </ul>
                                        </div><!-- end comment-content-head -->
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae sequi ipsa fugit officia eos! Sapiente laboriosam molestiae praesentium ducimus culpa. Magnam, odit, optio. Possimus similique eligendi explicabo, dolore, beatae sequi.</p>
                                        <cite>Joe Doe</cite>
                                    </div><!-- end comment-content -->
                                </div><!-- end comments -->
                                
                                <div class="comments">
                                    <div class="comment-image">
                                        <figure>
                                            <img src="../public/assets/img/team/team_02.jpg" alt="" />
                                        </figure>
                                    </div><!-- end comments-image -->
                                    <div class="comment-content">
                                        <div class="comment-content-head">
                                            <h6 class="comment-title">Lorem ipsum dolor sit</h6>
                                            <ul class="list list-inline comment-meta">
                                                <li>
                                                    <i class="fa fa-star text-warning"></i>
                                                    <i class="fa fa-star text-warning"></i>
                                                    <i class="fa fa-star text-warning"></i>
                                                    <i class="fa fa-star text-warning"></i>
                                                    <i class="fa fa-star-half-o text-warning"></i>
                                                </li>
                                            </ul>
                                        </div><!-- end comment-content-head -->
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae sequi ipsa fugit officia eos! Sapiente laboriosam molestiae praesentium ducimus culpa. Magnam, odit, optio.</p>
                                        <cite>Joe Doe</cite>
                                    </div><!-- end comment-content -->
                                </div><!-- end comments -->
                                
                                <div class="comments">
                                    <div class="comment-image">
                                        <figure>
                                            <img src="../public/assets/img/team/team_03.jpg" alt="" />
                                        </figure>
                                    </div><!-- end comments-image -->
                                    <div class="comment-content">
                                        <div class="comment-content-head">
                                            <h6 class="comment-title">Lorem ipsum dolor sit</h6>
                                            <ul class="list list-inline comment-meta">
                                                <li>
                                                    <i class="fa fa-star text-warning"></i>
                                                    <i class="fa fa-star text-warning"></i>
                                                    <i class="fa fa-star text-warning"></i>
                                                    <i class="fa fa-star text-warning"></i>
                                                    <i class="fa fa-star-o text-warning"></i>
                                                </li>
                                            </ul>
                                        </div><!-- end comment-content-head -->
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae sequi ipsa fugit officia eos! Sapiente laboriosam molestiae praesentium ducimus culpa. Magnam, odit, optio.</p>
                                        <cite>Jane Doe</cite>
                                    </div><!-- end comment-content -->
                                </div><!-- end comments -->
                                
                                <div class="comments">
                                    <div class="comment-image">
                                        <figure>
                                            <img src="../public/assets/img/team/team_04.jpg" alt="" />
                                        </figure>
                                    </div><!-- end comments-image -->
                                    <div class="comment-content">
                                        <div class="comment-content-head">
                                            <h6 class="comment-title">Lorem ipsum dolor sit</h6>
                                            <ul class="list list-inline comment-meta">
                                                <li>
                                                    <i class="fa fa-star text-warning"></i>
                                                    <i class="fa fa-star text-warning"></i>
                                                    <i class="fa fa-star text-warning"></i>
                                                    <i class="fa fa-star-half-empty text-warning"></i>
                                                    <i class="fa fa-star-o text-warning"></i>
                                                </li>
                                            </ul>
                                        </div><!-- end comment-content-head -->
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae sequi ipsa fugit officia eos! Sapiente laboriosam molestiae praesentium ducimus culpa. Magnam, odit, optio.</p>
                                        <cite>John Doe</cite>
                                    </div><!-- end comment-content -->
                                </div><!-- end comments -->
                                
                                <hr class="spacer-30">
                                
                                <h5>Add a review</h5>
                                <p>How do you rate this product?</p>
                                        
                                <hr class="spacer-5 no-border">

                                <form>
                                    <input type="text" class="rating rating-loading" value="5" data-size="sm" title="">
                                </form>

                                <hr class="spacer-10 no-border">

                                <div class="form-group">
                                    <label for="reviewName">Name</label>
                                    <input type="text" id="reviewName" class="form-control input-md" placeholder="Your Name">
                                </div><!-- end form-group -->
                                <div class="form-group">
                                    <label for="reviewEmail">E-mail</label>
                                    <input type="text" id="reviewEmail" class="form-control input-md" placeholder="Your E-mail">
                                </div><!-- end form-group -->
                                <div class="form-group">
                                    <label for="reviewMessage">Review</label>
                                    <textarea id="reviewMessage" rows="5" class="form-control" placeholder="Your review"></textarea>
                                </div><!-- end form-group -->
                                <div class="form-group">
                                    <input type="submit" class="btn btn-default round btn-md" value="Submit Review">
                                </div><!-- end form-group -->
                            </div><!-- end tab-pane -->
                        </div><!-- end tab-content -->
                    </div><!-- end col -->
                </div><!-- end row -->
                
                <hr class="spacer-60">
                
                <div class="row hide">
                    <div class="col-sm-12">
                        <h4 class="mb-20">You May Also Like</h4>
                    </div><!-- end col -->
                </div><!-- end row -->
                
                <div class="row hide">
                    <div class="col-sm-12">
                        <div id="owl-demo" class="owl-carousel column-4 owl-theme">
                            <div class="item">
                                <div class="thumbnail store style1">
                                    <div class="header">
                                        <figure>
                                            <a href="shop-single-product-v1.html">
                                                <img src="../public/assets/img/products/bags_03.jpg" alt="">
                                            </a>
                                        </figure>
                                    </div>
                                    <div class="caption">
                                        <h6 class="regular"><a href="shop-single-product-v1.html">Printed Summer Dress</a></h6>
                                        <div class="price">
                                            <small class="amount off">$68.99</small>
                                            <span class="amount text-primary">$59.99</span>
                                        </div>
                                        <span class="product-badge bottom left text-warning">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-half-o"></i>
                                            <i class="fa fa-star-o"></i>
                                        </span>
                                    </div><!-- end caption -->
                                </div><!-- end thumbnail -->
                            </div><!-- end item -->
                            
                            <div class="item">
                                <div class="thumbnail store style1">
                                    <div class="header">
                                        <figure>
                                            <a href="shop-single-product-v1.html">
                                                <img src="../public/assets/img/products/bags_06.jpg" alt="">
                                            </a>
                                        </figure>
                                    </div>
                                    <div class="caption">
                                        <h6 class="regular"><a href="shop-single-product-v1.html">Printed Summer Dress</a></h6>
                                        <div class="price">
                                            <small class="amount off">$68.99</small>
                                            <span class="amount text-primary">$59.99</span>
                                        </div>
                                        <span class="product-badge bottom left text-warning">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-half-o"></i>
                                            <i class="fa fa-star-o"></i>
                                        </span>
                                    </div><!-- end caption -->
                                </div><!-- end thumbnail -->
                            </div><!-- end item -->
                            
                            <div class="item">
                                <div class="thumbnail store style1">
                                    <div class="header">
                                        <figure>
                                            <a href="shop-single-product-v1.html">
                                                <img src="../public/assets/img/products/hoseholds_01.jpg" alt="">
                                            </a>
                                        </figure>
                                    </div>
                                    <div class="caption">
                                        <h6 class="regular"><a href="shop-single-product-v1.html">Printed Summer Dress</a></h6>
                                        <div class="price">
                                            <small class="amount off">$68.99</small>
                                            <span class="amount text-primary">$59.99</span>
                                        </div>
                                        <span class="product-badge bottom left text-warning">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-half-o"></i>
                                            <i class="fa fa-star-o"></i>
                                        </span>
                                    </div><!-- end caption -->
                                </div><!-- end thumbnail -->
                            </div><!-- end item -->
                            
                            <div class="item">
                                <div class="thumbnail store style1">
                                    <div class="header">
                                        <figure>
                                            <a href="shop-single-product-v1.html">
                                                <img src="../public/assets/img/products/fashion_02.jpg" alt="">
                                            </a>
                                        </figure>
                                    </div>
                                    <div class="caption">
                                        <h6 class="regular"><a href="shop-single-product-v1.html">Printed Summer Dress</a></h6>
                                        <div class="price">
                                            <small class="amount off">$68.99</small>
                                            <span class="amount text-primary">$59.99</span>
                                        </div>
                                        <span class="product-badge bottom left text-warning">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-half-o"></i>
                                            <i class="fa fa-star-o"></i>
                                        </span>
                                    </div><!-- end caption -->
                                </div><!-- end thumbnail -->
                            </div><!-- end item -->
                            <div class="item">
                                <div class="thumbnail store style1">
                                    <div class="header">
                                        <figure>
                                            <a href="shop-single-product-v1.html">
                                                <img src="../public/assets/img/products/hoseholds_03.jpg" alt="">
                                            </a>
                                        </figure>
                                    </div>
                                    <div class="caption">
                                        <h6 class="regular"><a href="shop-single-product-v1.html">Printed Summer Dress</a></h6>
                                        <div class="price">
                                            <small class="amount off">$68.99</small>
                                            <span class="amount text-primary">$59.99</span>
                                        </div>
                                        <span class="product-badge bottom left text-warning">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-half-o"></i>
                                            <i class="fa fa-star-o"></i>
                                        </span>
                                    </div><!-- end caption -->
                                </div><!-- end thumbnail -->
                            </div><!-- end item -->
                        </div><!-- end owl carousel -->
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </section>
        <!-- end section -->
        
        @stop