<?php
namespace App;
use App\ServiceDetail;
use URL; 
use Auth;
?>
    @extends('layout.public') @section('content')

    <!-- start section -->
    <section class="section white-backgorund">
        <div class="container">
            <div class="row">
                <!-- start sidebar -->
                <div class="col-sm-3">
                    <div class="widget">
                        <h6 class="subtitle">Account Navigation</h6>

                        <ul class="list list-unstyled">
                            <li>
                                <a href="{{ URL::to('/profile') }}">My Account</a>
                            </li>
                            <li class=''>
                                <a href="{{ URL::to('/upgrade') }}">Upgrade Account </a>
                            </li>
                            @if(Auth::user()->role_seller!='')
                            <li>
                                <a href="{{ URL::to('/upload') }}">Upload Documents</a>
                            </li>
                            @endif @if(Auth::user()->bank_detail_id!='' && Auth::user()->business_detail_id!='')
                            <li>
                                <a href="{{ URL::to('/post') }}">Post Products</a>
                            </li>
                            @endif
                            <li>
                                <a href="{{ URL::to('/my-product') }}">My Product / Service</a>
                            </li>
                        </ul>
                    </div>
                    <!-- end widget -->
                </div>
                <!-- end col -->
                <!-- end sidebar -->
                <!-- start of all form -->
                <div class="col-sm-9">
                    <div class="row">
                        <div class="col-sm-12 text-left">
                            <h5 class="title">View / Edit Service</h5>
                        </div>
                        <!-- end col -->
                    </div>
                    <!-- end row -->
                    <!-- start of basic check -->
                    @if(Auth::user()->aadhar_number!='')
                    <div class="col-md-12">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#productTab" aria-controls="productTab" role="productTab" data-toggle="tab">Products</a></li>
                        </ul>
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="productTab">
                                <form>
                            <!-- start of service -->
                            <div class='formService'>

                            <div class="form-group">
                            <label for="email">Name :</label>
                            <input type="email" class="form-control" id="s_name" placeholder="Name" name="email" value="{{$product->name}}">
                            <span class='err' id='s_name_err'>
                            </div>

                            <div class="form-group">
                            <label for="email">Description :</label>
                            <textarea rows="5" class="form-control" cols="10" id='s_desc' placeholder='Description'>{{$product->description}}</textarea>
                            <span class='err' id='s_desc_err'>
                            </div>

                            <div class="form-group">

                            <div class='row'>

                            <div class='col-md-4'>
                            <label for="email">Price per Hour :</label>
                            <input type="email" class="form-control" id="s_hour" placeholder="/hour" name="email" value="{{$product->hour}}">
                            <span class='err' id='s_hour_err'>
                            </div>

                            <div class='col-md-4'>
                            <label for="email">Price per Day:</label>
                            <input type="email" class="form-control" id="s_day" placeholder="/day" name="email" value="{{$product->day}}">
                            <span class='err' id='s_day_err'>
                            </div>

                            <div class='col-md-4'>
                            <label for="email">Price per Week:</label>
                            <input type="email" class="form-control" id="s_week" placeholder="/week" name="email" value="{{$product->week}}">
                            <span class='err' id='s_week_err'>
                            </div>

                            <div class='col-md-4'>
                            <label for="email">Price per Month:</label>
                            <input type="email" class="form-control" id="s_month" placeholder="/month" name="email" value="{{$product->month}}">
                            <span class='err' id='s_month_err'>
                            </div>

                            <div class='col-md-4'>
                            <label for="email">Price per Year:</label>
                            <input type="email" class="form-control" id="s_year" placeholder="/year" name="email" value="{{$product->year}}">
                            <span class='err' id='s_year_err'>
                            </div>

                            </div>

                            </div>

                            <div class='form-group'>
                            <div class="checkbox-input checkbox-primary">
                            <input id="checkbox2" class="styled" type="checkbox" checked="">
                            <label for="checkbox2">
                            I there by agree to the TERMS & CONDITIONS of 7ATARA
                            </label>
                            </div>
                            </div>

                            <center>
                            <a href="javascript:void(0);" id='updateService' class="btn btn-default btn-lg">Update Service Detail</a>
                            </center>
                            </div>
                            <!-- end of service -->
                            <input type="hidden" id="sids" value="{{$product->id}}">
                            </form>
                        </div>
                    </div>    
                    </div>
                    @endif
                    <!-- end of basic check -->

                    </div>
                    <!-- end of all form -->

                </div><!-- end row -->                
            </div><!-- end container -->
        </section>
        <!-- end section -->

<script src="{{ asset('/').('public/assets/js/custom/user.js') }}"></script>
<script src="{{ asset('/').('public/assets/js/custom/s3/ajaxupload.3.5.js') }}"></script>
<script src="{{ asset('/').('public/assets/js/custom/s3/s3_post.js') }}"></script>	
@stop