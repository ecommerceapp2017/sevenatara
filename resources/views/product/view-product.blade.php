<?php
namespace App;
use App\ServiceDetail;
use Config;
use URL;
use Auth;
?>
    @extends('layout.public') @section('content')

    <!-- start section -->
    <section class="section white-backgorund">
        <div class="container">
            <div class="row">
                <!-- start sidebar -->
                <div class="col-sm-3">
                    <div class="widget">
                        <h6 class="subtitle">Account Navigation</h6>

                        <ul class="list list-unstyled">
                            <li>
                                <a href="{{ URL::to('/profile') }}">My Account</a>
                            </li>
                            <li class=''>
                                <a href="{{ URL::to('/upgrade') }}">Upgrade Account </a>
                            </li>
                            @if(Auth::user()->role_seller!='')
                            <li>
                                <a href="{{ URL::to('/upload') }}">Upload Documents</a>
                            </li>
                            @endif @if(Auth::user()->bank_detail_id!='' && Auth::user()->business_detail_id!='')
                            <li>
                                <a href="{{ URL::to('/post') }}">Post Products</a>
                            </li>
                            @endif
                            <li>
                                <a href="{{ URL::to('/my-product') }}">My Product / Service</a>
                            </li>
                        </ul>
                    </div>
                    <!-- end widget -->
                </div>
                <!-- end col -->
                <!-- end sidebar -->
                <!-- start of all form -->
                <div class="col-sm-9">
                    <div class="row">
                        <div class="col-sm-12 text-left">
                            <h5 class="title">View / Edit Product</h5>
                        </div>
                        <!-- end col -->
                    </div>
                    <!-- end row -->
                    <!-- start of basic check -->
                    @if(Auth::user()->aadhar_number!='')
                    <div class="col-md-12">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#productTab" aria-controls="productTab" role="productTab" data-toggle="tab">Products</a></li>
                        </ul>
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="productTab">
                                <form>
                                <input type="hidden" id="pid" value="{{$product->id}}">
                                    <div class='form-group'>
                                        <label for="exampleInputEmail1">Product Type</label>
                                        <ul>
                                            <li style="list-style:none;">
                                                <input type="radio" name="p_p_type" value="1" class='form-check-input' <?php if($product->type==1){echo "checked";}?> id='type' /> Perishable
                                                <input type="radio" name="p_p_type" class='form-check-input' value="2" id="type" <?php if($product->type==2){echo "checked";}?>/> Non Perishable
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Category Type</label>
                                        <select class='form-control' id='p_cat_type'>
                                            @foreach($data as $dat)
                                            <option value='{{$dat["id"]}}'
                                            <?php
                                                if($product->cat==$dat['id']){
                                                    echo 'selected';
                                                }
                                            ?>
                                            >{{$dat['name']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1s">Sub Category Type</label>
                                        <select class='form-control' id='p_sb_type'>
                                        <option selected disabled value=""> Choose Sub Category</option>
                                        @foreach($subcat as $dat)
                                            <option value='{{$dat["id"]}}'
                                            <?php
                                                if($product->sub_cat==$dat['id']){
                                                    echo 'selected';
                                                }
                                            ?>
                                            >{{$dat['name']}}</option>
                                            @endforeach
                                        </select>
                                        <span class='err' id='p_sb_type_err'>
                                    </div>

                                    <div class="form-group">
                                        <label for="email">Product Name :</label>
                                        <input type="email" class="form-control" id="p_p_name" placeholder="Product Name" name="email" value="{{$product->name}}">
                                        <span class='err' id='p_p_name_err'>
                            </div>


                            <div class="form-group">
                              <div class='row'>
                              <div class='col-md-3'>
                                <label for="email">Product Length :</label>
                                <input type="email" class="form-control" id="p_p_length" placeholder="Product Length" name="email" value="{{$product->length}}">
                                <span class='err' id='p_p_length_err'>
                              </div>
                              <div class='col-md-3'>
                                <label for="email">Product Breadth :</label>
                                <input type="email" class="form-control" id="p_p_breadth" placeholder="Product Breadth" name="email" value="{{$product->breadth}}">
                                <span class='err' id='p_p_breadth_err'>
                              </div>
                              <div class='col-md-3'>
                                <label for="email">Product Height :</label>
                                <input type="email" class="form-control" id="p_p_height" placeholder="Product Height" name="email" value="{{$product->height}}">
                                <span class='err' id='p_p_height_err'>
                              </div>
                              <div class='col-md-3'>
                                <label for="email">Product Weight :</label>
                                <input type="email" class="form-control" id="p_p_weight" placeholder="Product Weight" name="email" value="{{$product->weight}}">
                                <span class='err' id='p_p_weight_err'>
                              </div>
                              </div>
                            </div>


                            <div class="form-group">
                            <div class='row'>
                            <div class='col-md-6'>
                            <label for="email">Price per unit :</label>
                            <input type="email" class="form-control" id="p_p_price" placeholder="Price per unit" name="email" value="{{$product->price}}">
                            <span class='err' id='p_p_price_err'>
                            </div>
                            <div class='col-md-6'>
                            <label for="email">Unit Type :</label>
                            <select class='form-control' id='p_p_unit_type'>
                              <option value='0' <?php if($product->unit=='0'){ echo 'selected'; } ?>>kilogram</option>
                              <option value='1' <?php if($product->unit=='1'){ echo 'selected'; } ?>>numbers</option>
                              <option value='2' <?php if($product->unit=='2'){ echo 'selected'; } ?>>rolls</option>
                              <option value='3' <?php if($product->unit=='3'){ echo 'selected'; } ?>>pieces</option>
                              <option value='4' <?php if($product->unit=='4'){ echo 'selected'; } ?>>tons</option>
                              <option value='5' <?php if($product->unit=='5'){ echo 'selected'; } ?>>units</option>
                              <option value='6' <?php if($product->unit=='6'){ echo 'selected'; } ?>>20 container</option>
                              <option value='7' <?php if($product->unit=='7'){ echo 'selected'; } ?>>40" container</option>
                              <option value='8' <?php if($product->unit=='8'){ echo 'selected'; } ?>>bag</option>
                              <option value='9' <?php if($product->unit=='9'){ echo 'selected'; } ?>>barrel</option>
                              <option value='10' <?php if($product->unit=='10'){ echo 'selected'; } ?>>bottle</option>
                              <option value='11' <?php if($product->unit=='11'){ echo 'selected'; } ?>>bushel</option>
                              <option value='12' <?php if($product->unit=='12'){ echo 'selected'; } ?>>carton</option>
                              <option value='13' <?php if($product->unit=='13'){ echo 'selected'; } ?>>dozen</option>
                              <option value='14' <?php if($product->unit=='14'){ echo 'selected'; } ?>>foot</option>
                              <option value='15' <?php if($product->unit=='15'){ echo 'selected'; } ?>>gallon</option>
                              <option value='16' <?php if($product->unit=='16'){ echo 'selected'; } ?>>gram</option>
                              <option value='17' <?php if($product->unit=='17'){ echo 'selected'; } ?>>hectare</option>
                              <option value='18' <?php if($product->unit=='18'){ echo 'selected'; } ?>>kilometer</option>
                              <option value='19' <?php if($product->unit=='19'){ echo 'selected'; } ?>>meter</option>
                              <option value='20' <?php if($product->unit=='20'){ echo 'selected'; } ?>>kilowatt</option>
                              <option value='21' <?php if($product->unit=='21'){ echo 'selected'; } ?>>litre</option>
                              <option value='22' <?php if($product->unit=='22'){ echo 'selected'; } ?>>metric tons</option>
                              <option value='23' <?php if($product->unit=='23'){ echo 'selected'; } ?>>long ton</option>
                              <option value='24' <?php if($product->unit=='24'){ echo 'selected'; } ?>>ounce</option>
                              <option value='25' <?php if($product->unit=='25'){ echo 'selected'; } ?>>packets</option>
                              <option value='26' <?php if($product->unit=='26'){ echo 'selected'; } ?>>packs</option>
                              <option value='27' <?php if($product->unit=='27'){ echo 'selected'; } ?>>pair</option>
                              <option value='28' <?php if($product->unit=='28'){ echo 'selected'; } ?>>ream</option>
                              <option value='29' <?php if($product->unit=='29'){ echo 'selected'; } ?>>pound</option>
                              <option value='30' <?php if($product->unit=='30'){ echo 'selected'; } ?>>set</option>
                              <option value='31' <?php if($product->unit=='31'){ echo 'selected'; } ?>>sheet</option>
                              <option value='32' <?php if($product->unit=='32'){ echo 'selected'; } ?>>short ton</option>
                              <option value='33' <?php if($product->unit=='33'){ echo 'selected'; } ?>>sq. ft.</option>
                              <option value='34' <?php if($product->unit=='34'){ echo 'selected'; } ?>>sq. metre</option>
                              <option value='35' <?php if($product->unit=='35'){ echo 'selected'; } ?>>watt</option>
                              <option value='36' <?php if($product->unit=='36'){ echo 'selected'; } ?>>Others</option>
                            </select>
                            </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <div class='row'>
                            <div class='col-md-6'>
                            <label for="email">Select GST Applicable :</label>
                            <select class='form-control' id='p_p_gst'>
                              <option value='1' <?php if($product->gst=='1'){ echo 'selected'; } ?> >5 % </option>
                              <option value='2' <?php if($product->gst=='2'){ echo 'selected'; } ?>>12 % </option>
                              <option value='3' <?php if($product->gst=='3'){ echo 'selected'; } ?>>18 % </option>
                              <option value='4' <?php if($product->gst=='4'){ echo 'selected'; } ?>>28 % </option>
                            </select>
                            </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <div class='row'>
                            <div class='col-md-6'>
                            <label for="email">Supply Capacity :</label>
                            <input type="email" class="form-control" id="p_p_supply_capacity" placeholder="Supply Capacity" name="email" value="{{$product->supply_capacity}}">
                            <span class='err' id='p_p_supply_capacity_err'>
                            </div>
                            </div>
                            </div>
                            <div class='row'>
                            <label for="email" class='pull-left'>Add Color :</label>
                            <div class='col-md-6'>
                            <div class="checkbox col-md-6">
                            <input type="checkbox"
                            <?php
                                if($product->color=='0'){
                                  echo 'checked';
                                }
                            ?>
                            id='assorted' class='checkbox-inline un_col' name="color[]" value="0">Assorted Color</label>
                            </div>
                            <div class="checkbox col-md-6">
                            <?php
                            $color = Config::get('db.color.color');
                            $col = $product->color;
                            $arr = explode(',', $col);
                            foreach ($color as $key => $value) {
                              $chk = '';
                              for ($i=0; $i < count($arr); $i++) {
                                  if($key==$arr[$i]){
                                    $chk = 'checked';
                                  }
                              }
                                ?>

                                <label><input type="checkbox" {{$chk}}  class='checkbox-inline oth_col' name="color[]" value="{{$key}}">{{$value}}</label>

                                <?php
                            }
                            ?>
                            </div>
                            <br>
                            <!--<input type="email" class="form-control" id="p_p_color" placeholder="Add Color" name="email">-->
                            <span class='err' id='p_p_color_err'>
                            </div>
                            </div>
                            <div class='row'>
                            <div class='col-md-6'>
                            <label for="email">Variety :</label>
                            <input type="email" class="form-control" id="p_p_variety" placeholder="Variety" name="email" value="{{$product->variety}}">
                            <span class='err' id='p_p_variety_err'>
                            </div>
                            </div>
                            <div class='row'>
                            <div class='col-md-6'>
                            <label for="email">Material Type :</label>
                            <input type="email" class="form-control" id="p_p_material_type" placeholder="Material Type" name="email" value="{{$product->material_type}}">
                            <span class='err' id='p_p_material_type_err'>
                            </div>
                            </div>
                            </div>
                            <div class='form-group'>
                            <label for="email">Description :</label>
                            <textarea rows="" cols="" class='form-control' placeholder='Description' id='p_p_description'>{{$product->description}}</textarea>
                            <span class='err' id='p_p_description_err'>
                            </div>
                            <div class="form-group">
                            <div class='row'>
                            <div class='col-md-6'>
                            <label for="email">Minimum Order Quantity (MOQ) :</label>
                            <input type="email" class="form-control" id="p_p_moq" placeholder="Minimum Order Quantity (MOQ" name="email" value="{{$product->moq}}">
                            <span class='err' id='p_p_moq_err'>
                            </div>
                            <div class='col-md-6'>
                            <label for="email">Returning Quantity :</label>
                            <input type="email" class="form-control" id="p_p_returns" placeholder="Returning Quantity" name="email" value="{{$product->returns}}">
                            <span class='err' id='p_p_returns_err'>
                            </div>
                            </div>
                            </div>
                            <div class='form-group hide'>
                            <label for="email">Sample Available ? :</label>
                            <div class="radio-input radio-primary">
                            <input type="radio" name="p_p_sample" id="" value="1" <?php if($product->sample==1){echo "checked";}?>>
                            <label for="radio4">
                            Available
                            </label>
                            <input type="radio" name="p_p_sample" id="radio4" value="0" <?php if($product->sample==0){echo "checked";}?>>
                            <label for="radio4">
                            Not Available
                            </label>
                            </div>
                            </div>
                            <div class='form-group'>
                            <label for="email">Warranty ? :</label>
                            <div class="">
                            <input type="radio" name="p_p_warranty" <?php if(strlen($product->warranty)>0){echo "checked";}?> id="warranty_yes" value="" class="radio-input radio-primary warranty_data">Yes
                            <input type="radio" name="p_p_warranty" <?php if(strlen($product->warranty)==0){echo "checked";}?> id="warranty_no" value="" class="radio-input radio-primary warranty_data">No
                            <?php
                              if(strlen($product->warranty)>0){
                            ?>
                            <figure class="user-avatar medium war_pic">
                              
              								<img class="warantee" alt="avatar" src="{{$product->warranty}}" style="height:65px;width:65px">
                              <span id="warantee_pic" href="javascript:;" class="button mid-short dark-light ">Upload Image</span>
                              <span id="warantee_status"></span>
                              <input type="hidden" id="warantee">
              							</figure>
                            <?php } ?>
                            </div>
                            </div>
                            <div class='form-group'>
                            <label for="email">Guarantee ? :</label>
                            <div class="">
                              <input type="radio" name="p_p_gurantee" <?php if(strlen($product->gurantee)>0){echo "checked";}?> id="guarantee_yes" value="" class="radio-input radio-primary guarantee_data">Yes
                              <input type="radio" name="p_p_gurantee" <?php if(strlen($product->gurantee)==0){echo "checked";}?> id="guarantee_no" value="" class="radio-input radio-primary guarantee_data">No
                              <?php
                                if(strlen($product->gurantee)>0){
                              ?>
                              <figure class="user-avatar medium gua_pic">
                								<img class="warantee" alt="avatar" src="{{$product->gurantee}}" style="height:65px;width:65px">
                                    <span id="warantee_pic" href="javascript:;" class="button mid-short dark-light ">Upload Image</span>
                                    <span id="warantee_status"></span>
                                    <input type="hidden" id="warantee">
                							</figure>
                              <?php } ?>
                            </div>
                            </div>
                            <div class='form-group' id='uploadForms'>
                            <label for="email">Upload Product Images</label>
                            <figure class="user-avatar medium">
								                <img class="u_p_p_i" alt="avatar" src="{{$product->image}}" style="height:65px;width:65px">
                                <span id="u_p_p_s"></span>
                                <input type="hidden" id="u_p_p_im" value="{{$product->image}}">
							              </figure>
                            <br>
                            <figure class="user-avatar medium">
								                <img class="u_p_p_i" alt="avatar" src="{{$product->image1}}" style="height:65px;width:65px">
                                <span id="u_p_p_s"></span>
                                <input type="hidden" id="u_p_p_im" value="{{$product->image}}">
							              </figure>
                            <br>
                            <figure class="user-avatar medium">
								                <img class="u_p_p_i" alt="avatar" src="{{$product->image2}}" style="height:65px;width:65px">
                                <span id="u_p_p_s"></span>
                                <input type="hidden" id="u_p_p_im" value="{{$product->image}}">
							              </figure>
                            <br>
                            <figure class="user-avatar medium">
								                <img class="u_p_p_i" alt="avatar" src="{{$product->image3}}" style="height:65px;width:65px">
                                <span id="u_p_p_s"></span>
                                <input type="hidden" id="u_p_p_im" value="{{$product->image}}">
							              </figure>
                            <br>
                            <figure class="user-avatar medium">
								                <img class="u_p_p_i" alt="avatar" src="{{$product->image4}}" style="height:65px;width:65px">
                                <span id="u_p_p_s"></span>
                                <input type="hidden" id="u_p_p_im" value="{{$product->image}}">
							              </figure>
                            <span class='err' id='u_p_p_im_err'>
                            </div>
                            <div class='form-group'>
                            <div class="checkbox-input checkbox-primary">
                            <input id="checkbox2" class="styled" type="checkbox" checked="">
                            <label for="checkbox2">
                            I there by agree to the TERMS & CONDITIONS of 7ATARA
                            </label>
                            </div>
                            </div>
                            <center>
                            <a href="javascript:void(0);" id='updateProduct' class="btn btn-default btn-lg">Update Product</a>
                            </center>
                            </form>
                        </div>
                    </div>
                    </div>
                    @endif
                    <!-- end of basic check -->

                    </div>
                    <!-- end of all form -->

                </div><!-- end row -->
            </div><!-- end container -->
        </section>
        <!-- end section -->

<script src="{{ asset('/').('public/assets/js/custom/user.js') }}"></script>
<script src="{{ asset('/').('public/assets/js/custom/s3/ajaxupload.3.5.js') }}"></script>
<!-- <script src="{{ asset('/').('public/assets/js/custom/s3/s3_post.js') }}"></script> -->
@stop
