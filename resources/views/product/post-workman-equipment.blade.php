@extends('layout.public')
@section('content')


                    <div class="col-sm-9">
                        <div class="row">
                            <div class="col-sm-12 text-left">
                                <h5 class="title">Post Workman / Equipment Details</h5>
                                <br>
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#workTab" aria-controls="workTab" role="workTab" data-toggle="tab">Workman Details</a></li>
                                    <li role="presentation"><a href="#equipmentTab" aria-controls="equipment" role="equipment" data-toggle="tab">Equipment Details</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="workTab">
                                    <!-- start of Workman -->
                                    <div class="table-responsive">    
                                    <table class="table">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <div class="form-group">
                                                        <label for="firstname">Workman name</label>
                                                        <input id="workman_name" type="text" placeholder="Full Name" name="firstname" class="form-control input-sm required">
                                                        <span id='workman_name_error' class='error'></span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <figure class="user-avatar medium">
                                                    <img class='weImg' alt="avatar" src='public/assets/img/avatar.png' style='height:65px;width:65px' >
                                                    <br>
                                                    <span id='we_pic' href="javascript:;" class="button mid-short dark-light" >Upload Aadhar</span>
                                                    <p id='we_error' class='error'></p>
                                                    <span class='we_status'></span>
                                                    <input type='hidden' id='weImgTxt'>
                                                    
                                                </td>
                                                <td>
                                                    <figure class="user-avatar medium">
                                                    <img class='weImg_1' alt="avatar" src='public/assets/img/avatar.png' style='height:65px;width:65px' >
                                                    <br>
                                                    <span id='we_pic_1' href="javascript:;" class="button mid-short dark-light" >Upload License</span>
                                                    <p id='we_1_error' class='error'></p>
                                                    <span class='we_1_status'></span>
                                                    <input type='hidden' id='weImg_1_Txt'>
                                                    
                                                    </figure>
                                                </td>
                                                <td>
                                                    <a id='addWorkMan' href="javascript:void(0)" class="btn btn-default round btn-sm"><i class="fa fa-plus mr-5"></i> Add Workman</a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table><!-- end table -->

                                    <!-- start of workman list -->
                                    <hr>
                                        <div class=''>
                                            <table class="table table-hover">
    <thead>
      <tr>
        <th>#</th>
        <th>Workman Name</th>
        <th>Aadhar</th>
        <th>License</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
    <?php $ctr = 0;?>
    @foreach($data as $dat)
    <?php $ctr++; ?>
      <tr>
        <td>{{$ctr}}</td>
        <td>{{$dat->name}}</td>
        <td><img src='{{$dat->aadhar}}' style='height:65px;width:65px'></td>
        <td><img src='{{$dat->license}}' style='height:65px;width:65px'></td>
        <td>
    <input type='hidden' id='{{$dat->id}}_name' value='{{$dat->name}}'>
    <input type='hidden' id='{{$dat->id}}_aadhar' value='{{$dat->aadhar}}'>
    <input type='hidden' id='{{$dat->id}}_license' value='{{$dat->license}}'>
      <a href="javascript:void(0);" class="btn btn-warning round btn-xs edit_workman" id='{{$dat->id}}' data-toggle="modal" data-target="#myModal">Edit</a>
      <a href="deleteWorkMan/{{$dat->id}}" class="btn btn-danger round btn-xs">Delete</a>
      </td>
      </tr>
    @endforeach
    </tbody>
  </table>
                                        </div>
                                    <!-- end of workman list -->

                                </div>
                                    <!-- end of workman -->
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="equipmentTab">
                                    Equipment
                                    </div>
                                </div>
                            </div><!-- end col -->
                        </div><!-- end row -->                       
                    </div><!-- end col -->
        </section>
        <!-- end section -->

<!-- start edit of workman -->
 <!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog modal-lg">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
<h4 class="modal-title" id="myModalLabel">Edit Workman</h4>
</div>
<div class="modal-body">
<table class="table">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <div class="form-group">
                                                        <label for="firstname">Workman name</label>
                                                        <input id="edit_workman_name" type="text" placeholder="Full Name" name="firstname" class="form-control input-sm required">
                                                        <span id='workman_name_error' class='error'></span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <figure class="user-avatar medium">
                                                    <img class='edit_weImg' alt="avatar" src='public/assets/img/avatar.png' style='height:65px;width:65px' >
                                                    <br>
                                                    <span id='edit_we_pic' href="javascript:;" class="button mid-short dark-light" >Upload Aadhar</span>
                                                    <p id='edit_we_error' class='error'></p>
                                                    <span class='edit_we_status'></span>
                                                    <input type='hidden' id='edit_weImgTxt'>
                                                    
                                                </td>
                                                <td>
                                                    <figure class="user-avatar medium">
                                                    <img class='edit_weImg_1' alt="avatar" src='public/assets/img/avatar.png' style='height:65px;width:65px' >
                                                    <br>
                                                    <span id='edit_we_pic_1' href="javascript:;" class="button mid-short dark-light" >Upload License</span>
                                                    <p id='edit_we_1_error' class='error'></p>
                                                    <span class='edit_we_1_status'></span>
                                                    <input type='hidden' id='edit_weImg_1_Txt'>
                                                    
                                                    </figure>
                                                </td>
                                                <td>
                                                <input type='hidden' id='updateId' >
                                                    <a id='updateWorkMan' href="javascript:void(0)" class="btn btn-default round btn-sm"><i class="fa fa-plus mr-5"></i> Update Workman</a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table><!-- end table -->
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>
</div>
<!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- end edit of workman -->

<script src="{{ asset('/').('public/assets/js/custom/s3/ajaxupload.3.5.js') }}"></script>
<script src="{{ asset('/').('public/assets/js/custom/s3/s3_img.js') }}"></script>
<script src="{{ asset('/').('public/assets/js/custom/s3/s3_img1.js') }}"></script>
<script src="{{ asset('/').('public/assets/js/custom/s3/s3_img_edit.js') }}"></script>
<script src="{{ asset('/').('public/assets/js/custom/s3/s3_img1_edit.js') }}"></script>
<script src="{{ asset('/').('public/assets/js/custom/user.js') }}"></script>
@stop