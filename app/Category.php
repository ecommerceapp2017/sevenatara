<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'category';

    public static $add = array(
        'name' => 'required',
        'description' => 'required',
        'tax_type' => 'required',
        'tax_value' => 'required'
		);
}
