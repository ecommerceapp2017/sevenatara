<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_type','name', 'email', 'fb_id', 'image', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
         'remember_token',
    ];

    #Validations

    
    public static $registerUserFB = array(
        'name' => 'required',
        'email' => 'required',
        'fb_id' => 'required'
		);

    public static $loginUser = array(
        'password' => 'required'
		);

    public static $registerUser = array(
        'phone' => array('required','unique:users'), 
        'email' => array('required','email','unique:users'), 
        'password' => 'required'
		);

    public static $preRegister = array(
        'phone' => array('required','unique:users'), 
        'email' => array('required','email','unique:users'), 
        'password' => 'required'
		);
    
    public static $resetPassword = array(
        'email' => 'required'
		);
        
}
