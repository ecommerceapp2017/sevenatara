<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceCategory extends Model
{
    protected $table = 'service_category';

    public static $add = array(
        'name' => 'required',
        'description' => 'required',
        'tax_type' => 'required',
        'tax_value' => 'required'
		);
}
