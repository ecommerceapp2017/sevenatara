<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MigrateOne extends Model
{
    protected $table = 'migrate1';
}
