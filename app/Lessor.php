<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lessor extends Model
{
    protected $table = 'lessor';
}
