<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buyer extends Model
{
    protected $table = 'buyer';
    public static $updateBlog = array(
        'user_id' => 'required'
    );
}
