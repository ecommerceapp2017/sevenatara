<?php

namespace App\Http\Controllers;
use Input;

use Illuminate\Http\Request;
use Plivo\RestAPI;
use Validator;
use Hash;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Mail;
use SendGrid;
use Config;
class PreController extends Controller
{
    public function index(){
        return view('pre.index');
    }


    public function OTP(){
        $phone = Input::get('phone');
        $userExist = User::where('phone', $phone)->first();
        if($userExist)
        {
            $Response = array('success' => '0', 'message' => 'User already exists');
            return $Response;
        }
        $otp =  substr(str_shuffle("1234567890"), 0, 6);
        $params = array(
        'src' => '917010609203', // Sender's phone number with country code
        'dst' => '91'.$phone, // Receiver's phone number with country code
        'text' => 'Greetings, '.$otp.' is your One Time Password. Love, Team 7A http://www.sevenatara.com  ', // Your SMS text message
        'method' => 'POST' // The method used to call the url
        );
        $p = new RestAPI('MAM2NHMGEWMDIYMMJJNZ', 'NmFlYmRjMjg5MWVjMzYwMDkxZmI0ZGFlOGI4YTU3');
        $Response = $p->send_message($params);
        if($Response['status']==202){
            $Response = array('success' => '1', 'message' => 'Done', 'otp' => $otp);
        }else{
            $Response = array('success' => '0', 'message' => 'Invalid Phone Number');
        }
        return $Response;
    }

    public function preRegister()
  {
    $userData = Input::all();
    $validation = Validator::make($userData, User::$preRegister);
    if ($validation->passes()) {
      if(isset($userData['referal_code']))
      {
        if($userData['referal_code']!='')
        {
          $checkReferal = User::where('self_referal_code', $userData['referal_code'])->first();
          if($checkReferal)
          {

          }
          else
          {
            $Response = array('success' => '2', 'error' => 'Invalid Referal Code');
            return $Response;
          }
        }
      }
      $self_referal_code = substr(str_shuffle("ABCDEFGHJKLMNPQRSTUVWXYZ23456789"), 0, 6);
      $coupon_code = substr(str_shuffle("ABCDEFGHJKLMNPQRSTUVWXYZ23456789"), 0, 6);
      $phone = $userData['phone'];
      $data = new User;
      $data->user_type = 2;
      $data->email = $userData['email'];
      $data->first_name = $userData['first_name'];
      $data->last_name = $userData['last_name'];
      $data->phone = $phone;
      $data->self_referal_code = $self_referal_code;
      $data->coupon_code = $coupon_code;
      $data->referal_code = $userData['referal_code'];
      $data->password = Hash::make($userData['password']);
      $data->raw_password = $userData['password'];
      $data->status = 0;
      $data->save();
      $Response = array('success' => '1', 'id' => $data->id, 'code' => $self_referal_code, 'codes' => $coupon_code);
    }
    else
    {
      $Response = array('success' => '0', 'error' => $validation->messages());
    }
    return $Response;
  }

  public function verify($code){
    $userExist = User::where('self_referal_code', $code)->first();
    if($userExist){
        $Response = array('success' => '1');
    }else{
        $Response = array('success' => '0');
    }
    return $Response;
  }

  public function sendMail($id){
  $userData = User::where("id", $id)->first();
  $Mail = Mail::where('id', '4')->first();
  $from = new SendGrid\Email("Seven Atara", "info@sevenatara.com");
  $subject = $Mail->subject;
  $name = $userData['first_name'].' '.$userData['last_name'];
  $to = new SendGrid\Email($name, $userData['email']);
  $mailContent = $Mail->content;
  $mailContent = str_replace("{{name}}", $name, $mailContent);
  $mailContent = str_replace("{{cc}}", $userData['coupon_code'], $mailContent);
  $mailContent = str_replace("{{rc}}", $userData['self_referal_code'], $mailContent);
  $content = new SendGrid\Content("text/html", $mailContent);
  $mail = new SendGrid\Mail($from, $subject, $to, $content);
  $apiKey = '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';
  #$apiKey = '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';
  $sg = new \SendGrid($apiKey);
  $response = $sg->client->mail()->send()->post($mail);
  $Response = array('success' => '1', 'message' => 'Done');
  return $Response;
  }

  public function referal($code){
    $userExist = User::where('self_referal_code', $code)->first();
    if($userExist){
        return view('pre.referal')->with('data', $code);
    }else{
        return view('pre.referal')->with('data', 'invalid');
    }
  }
}
