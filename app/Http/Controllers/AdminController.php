<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Bank;
use App\Business;
use App\Category;
use App\Contact;
use App\User;
use App\Buyer;
use App\Broker;
use App\Lessee;
use App\Lessor;
use App\Seeker;
use App\Seller;
use App\Product;
use App\ServiceDetail;
use App\Service;

use Session;
use Auth;
use Input;
use Redirect;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    
    public function index()
    {
        return view('admin.login');
    }

    #Admin Auth Login

    public function authAdminLogin() {
        if (Auth::attempt(array('email' => Input::get('email'), 'password' => Input::get('password')))) {
            return Redirect::to('dashboard');
        } else {
            return Redirect::back()->with('warning', 'Invalid Username or Password')->withInput(Input::all());
        }
    }

    public function dashboard(){
        $service = Service::count();
        $product = Product::count();
        $user = User::count();
        $services = ServiceDetail::count();
    	return view('admin.dashboard')->with('service', $service)->with('services', $services)->with('product', $product)->with('user', $user-1);
    }

    #Users

    public function usersAll()
    {
        $users = User::where('user_type', 2)->get();
        return view('admin.user.list')->with('users', $users)->with('title', 'All Users');
    }

    public function usersDelete($id){
        $updateData['status'] = 3;
        User::where('id', $id)->update($updateData);
        return Redirect::to('/users-all');
    }

    public function usersActivated()
    {
        $users = User::where('user_type', 2)->where('status', 1)->get();
        return view('admin.user.list')->with('users', $users)->with('title', 'Activated Users');
    }

    public function usersNotActivated()
    {
        $users = User::where('user_type', 2)->where('status', 0)->get();
        return view('admin.user.list')->with('users', $users)->with('title', 'Not Activated Users');
    }

    public function usersBlocked()
    {
        $users = User::where('user_type', 2)->where('status', 3)->get();
        return view('admin.user.list')->with('users', $users)->with('title', 'Blocked Users');
    }

    public function categoryCreate()
    {
        return view('admin.category.detail')->with('products', array())->with('title', 'Create Category');
    }

    public function categoryEdit($id)
    {
        $data = Category::where('id', $id)->first();
        return view('admin.category.detail')->with('data', $data)->with('title', 'Edit Category');
    }

    

    public function categoryCreateData()
    {
        $userData = Input::all();
        $validation = Validator::make($userData, Category::$add);
        if ($validation->passes()) 
        {
            $data = new Category;
            $data->name = $userData['name'];
            $data->description = $userData['description'];
            $data->tax_type = $userData['tax_type'];
            $data->tax_value = $userData['tax_value'];
            $data->status = 1;
            $data->save();
            return back()->withInput()->with('success', 'Category created');
        }
        else
        {
            return back()->withInput()->with('err', $validation->messages());
        }       
    }

    public function editCategoryData()
    {
        $userData = Input::all();
        $validation = Validator::make($userData, Category::$add);
        if ($validation->passes()) 
        {
            $updateData['name'] = $userData['name'];
            $updateData['description'] = $userData['description'];
            $updateData['tax_type'] = $userData['tax_type'];
            $updateData['tax_value'] = $userData['tax_value'];
            Category::where('id', $userData['id'])->update($updateData);
            return back()->withInput()->with('success', 'Category created');
        }
        else
        {
            return back()->withInput()->with('err', $validation->messages());
        }       
    }

    
    public function productStatus($id)
    {
        $data = Product::where('id', $id)->first();
        if($data['status']==0)
        {
            $status = 1;
        }
        else
        {
            $status = 0;
        }
        $updateData['status'] = $status;
        Product::where('id', $id)->update($updateData);
        return back();
    }
    public function categoryStatus($id)
    {
        $data = Category::where('id', $id)->first();
        if($data['status']==0)
        {
            $status = 1;
        }
        else
        {
            $status = 0;
        }
        $updateData['status'] = $status;
        Category::where('id', $id)->update($updateData);
        return back();
    }
    public function categoryAll()
    {
        $users = Category::all();
        return view('admin.category.index')->with('contacts', $users)->with('title', 'All Category');
    }

    public function productsAll()
    {
        $users = Product::all();
        return view('admin.product.list')->with('users', $users)->with('title', 'All Products');
    }

    public function productsApprove()
    {
        $users = Product::where('status',0)->get();
        return view('admin.product.list')->with('users', $users)->with('title', 'Approve Products');
    }

    public function productGet($id)
    {
        $users = Product::where('id',$id)->first();
        $products = Category::get();
        return view('admin.product.detail')->with('users', $users)->with('products', $products)->with('title', 'Product Detail');
    }

    public function productDetail($id)
    {
        $data = Product::where('id',$id)->first();
        return $data;
    }

    public function contacts()
    {
        $contact = Contact::get();
        return view('admin.contact.index')->with('contacts', $contact);
    }

    #Other Functions
    
    public function checkConnection(){
    	$Response = array('success' => '1', 'message' => 'Connection Success !');
        return $Response;
    }
    
    public function getToken()
    {
        $Response = array('success' => '1', '_token' => csrf_token());
        return $Response;
    }

    public function approval_product()
    {
        $product = Product::get();
    	return view('admin.approval.product')->with('contacts', $product);
    }

    public function approval_product_view($id){
        $data = Category::where('status','1')->get();
        $product = Product::where('id',$id)->first();
    	return view('admin.approval.product-view')->with('contacts', $product)->with('data', $data);
    }

    public function approval_service()
    {
        $product = Service::get();
    	return view('admin.approval.service')->with('contacts', $product);
    }

    public function service_document()
    {
        $product = ServiceDetail::get();
    	return view('admin.approval.services')->with('contacts', $product);
    }

    public function approve($action,$cat,$id){
if($action=='approve'){
$status = 2;
}
else if ($action=='reject'){
$status = 3;
}
else{    
$status = 4;
}
        if($cat=='product'){
$updateData['status'] = $status;
Product::where('id', $id)->update($updateData);
        }
        else if($cat=='service'){
$updateData['status'] = $status;
Service::where('id', $id)->update($updateData);
        }
        else{
$updateData['status'] = $status;
ServiceDetail::where('id', $id)->update($updateData);
        }
    return back();
    }

    public function reject($cat,$id){
    }

    public function suspend($cat,$id){
    }

    public function logout()
    {
        Session::flush();
        return Redirect::to('/master');
    }
}
