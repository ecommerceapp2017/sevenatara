<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Bank;
use App\Business;
use App\Category;
use App\Contact;
use App\Cart;
use App\User;
use App\Buyer;
use App\Broker;
use App\Lessee;
use App\Lessor;
use App\Order;
use App\OrderDetail;
use App\Seeker;
use App\Seller;
use App\Product;
use App\ServiceDetail;
use App\Service;
use App\ServiceCategory;
use App\ServiceSubCategory;
use App\SubCategory;
use App\Workman;


use App\Mail;
use App\Migrate;
use App\MigrateOne;
use Input;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use SendGrid;
use Auth;
use Session;
use Redirect;
use Hash;
use Config;
use Plivo\RestAPI;
use File;
require 'vendor/autoload.php';

class AppController extends Controller
{

  public function index(){
    $products = Product::where('status', '2')->get();
    return view('index')->with('products', $products);
  }

  public function about(){
    return view('about');
  }

  public function products(){
    $data = Product::where('status', '2')->get();
    return view('product.all')->with('products', $data);
  }

  public function productDetail($id){
    $data = Product::where('id', $id)->first();
    return view('product.one')->with('product', $data);
  }


  public function contact(){
    return view('contact');
  }

  public function contactUs()
  {
    $userData = Input::all();
    $data = new Contact;
    $data->name = $userData['name'];
    $data->email = $userData['email'];
    $data->message = $userData['message'];
    $data->status = 0;
    $data->save();
    $Response = array('success' => '1');
    return $Response;
  }

  public function postProduct()
  {
    $userData = Input::all();
    if($userData['type']=='1')
    {
      return $this->postProductData($userData);
    }
    else
    {
      return $this->postProductData($userData);
    }
  }

  public function updateProduct(){
    $userData = Input::all();
    $updateData['user_id'] = $userData['id'];
    $updateData['type'] = $userData['type'];
    $updateData['cat'] = $userData['cat'];
    $updateData['sub_cat'] = $userData['sub'];
    $updateData['name'] = $userData['name'];
    $updateData['image'] = $userData['img'];
    $updateData['price'] = $userData['price'];
    $updateData['unit'] = $userData['unit'];
    $updateData['height'] = $userData['height'];
    $updateData['weight'] = $userData['weight'];
    $updateData['length'] = $userData['length'];
    $updateData['breadth'] = $userData['breadth'];
    if(isset($userData['unit_others'])){
        $updateData['unit_others'] = $userData['unit_others'];
    }
    $updateData['gst'] = $userData['gst'];
    $updateData['supply_capacity'] = $userData['supply_capacity'];
    $updateData['color'] = $userData['color'];
    $updateData['variety'] = $userData['variety'];
    $updateData['material_type'] = $userData['material_type'];
    $updateData['description'] = $userData['description'];
    $updateData['moq'] = $userData['moq'];
    $updateData['returns'] = $userData['returns'];
    $updateData['sample'] = $userData['sample'];
    $updateData['gurantee'] = $userData['gurantee'];
    $updateData['warranty'] = $userData['warranty'];
    $updateData['status'] = '1';
    Product::where('id', $userData['pid'])->update($updateData);
    $Response = array('success' => '1', 'message' => 'Product updated succesfully');
    return $Response;

  }

  public function postServiceDetail()
  {
    $userData = Input::all();
    $dataExist = ServiceDetail::where('user_id', $userData['id'])->first();
    if($dataExist){
      $Response = array('success' => '0', 'message' => 'Service Details already exists');
    }
    else{

      $data = new ServiceDetail;
      $data->name = $userData['name'];
      $data->user_id = $userData['id'];
      $data->type = $userData['c_type'];
      $data->cat = $userData['cat'];
      $data->address = $userData['address'];
      $data->certificate = $userData['certificate'];
      if($userData['c_type']=="1")
      {

      }
      else
      {
        $data->gstin = $userData['gstin'];
        $data->country = $userData['country'];
        $data->state = $userData['state'];
        $data->city = $userData['city'];
        $data->service = $userData['service'];
        $data->b_address = $userData['b_address'];
        $data->tan = $userData['tan'];
        $data->aadhar = $userData['aadhar'];
        $data->pan = $userData['pan'];
      }
      $data->status = '1';
      $data->save();
      $updateData['role_service_user'] = '1';
      User::where('id', $userData['id'])->update($updateData);
      $Response = array('success' => '1', 'message' => 'Service registered succesfully' );
    }
    return $Response;
  }

  public function postService()
  {
    $userData = Input::all();
    $userExist = Service::where('user_id', $userData['id'])->where('name', $userData['name'])->first();
    if($userExist)
    {
      $Response = array('success' => '0', 'message' => 'Service Name already uploaded' );
    }
    else
    {
      $data = new Service;
      $data->user_id = $userData['id'];
      $data->name = $userData['name'];
      $data->description = $userData['description'];
      $data->hour = $userData['hour'];
      $data->day = $userData['day'];
      $data->week = $userData['week'];
      $data->month = $userData['month'];
      $data->year = $userData['year'];
      $data->status = '1';
      $data->save();
      $Response = array('success' => '1', 'message' => 'Service uploaded succesfully' );
    }
    return $Response;
  }

  public function updateService(){
    $userData = Input::all();
    $updateData['user_id'] = $userData['id'];
    $updateData['name'] = $userData['name'];
    $updateData['description'] = $userData['description'];
    $updateData['hour'] = $userData['hour'];
    $updateData['day'] = $userData['day'];
    $updateData['week'] = $userData['week'];
    $updateData['month'] = $userData['month'];
    $updateData['year'] = $userData['year'];
    $updateData['status'] = '1';
    Service::where('id', $userData['sid'])->update($updateData);
    $Response = array('success' => '1', 'message' => 'Service updated succesfully' );
    return $Response;
  }
  public function postProductData($userData)
  {
    $userData = Input::all();
    $userExist = Product::where('user_id', $userData['id'])->where('name', $userData['name'])->first();
    if($userExist){
      $Response = array('success' => '0', 'message' => 'Product Name already uploaded' );
    }
    else{
      $data = new Product;
      $data->user_id = $userData['id'];
      $data->type = $userData['type'];
      $data->cat = $userData['cat'];
      $data->sub_cat = $userData['sub'];
      $data->name = $userData['name'];
      $data->image = $userData['img'];
      $data->image1 = $userData['img1'];
      $data->image2 = $userData['img2'];
      $data->image3 = $userData['img3'];
      $data->image4 = $userData['img4'];
      $data->price = $userData['price'];
      $data->length = $userData['length'];
      $data->breadth = $userData['breadth'];
      $data->height = $userData['height'];
      $data->weight = $userData['weight'];
      $data->unit = $userData['unit'];
      $data->unit_others = $userData['unit_others'];
      $data->gst = $userData['gst'];
      $data->supply_capacity = $userData['supply_capacity'];
      $data->color = $userData['color'];
      $data->variety = $userData['variety'];
      $data->material_type = $userData['material_type'];
      $data->description = $userData['description'];
      $data->moq = $userData['moq'];
      $data->returns = $userData['returns'];
      $data->sample = 1;#$userData['sample'];
      $data->gurantee = $userData['gurantee'];
      $data->warranty = $userData['warranty'];
      $data->status = '1';
      $data->save();
      $Response = array('success' => '1', 'message' => 'Product uploaded succesfully' );
    }
    return $Response;
  }

  public function login(){
    return view('login');
  }

  public function test(){
    return Hash::make('Seven@#123');
    return view('test');
  }

  public function loginUsingFB()
  {
    $userData = Input::all();
    $validation = Validator::make($userData, User::$registerUserFB);
    if ($validation->passes()) {
      $userExist = User::where('email', $userData['email'])->first();
      if($userExist)
      {
        if($userExist['status']==1)
        {
          Auth::loginUsingId($userExist['id']);
          $Response = array('success' => '1', 'id' => $userExist['id']);
        }
        else
        {
          $Response = array('success' => '0', 'message' => 'Account not verified');
        }
      }
      else
      {
        $userData['status'] = 1;
        //$User = User::create($userData);
        $data = new User;
        $data->name = $userData['name'];
        $data->email = $userData['email'];
        $data->fb_id = $userData['fb_id'];
        $data->image = $userData['image'];
        $data->status = 1;
        $data->save();
        Auth::loginUsingId($data->id);
        $Response = array('success' => '1', 'id' => $data->id);
      }
    }
    else
    {
      $Response = array('success' => '0', 'error' => $validation->messages());
    }
    return $Response;
  }


  public function loginUser()
  {
    $userData = Input::all();
    $validation = Validator::make($userData, User::$loginUser);
    if ($validation->passes()) {
      if(isset($userData['email']))
      {
        $userExist = User::where('email', $userData['email'])->orWhere('phone', $userData['email'])->first();
      }
      else
      {
        $Response = array('success' => '0', 'message' => 'Email or Phone Number is required');
        return $Response;
      }
      if($userExist)
      {
        if (Hash::check($userData['password'], $userExist['password'])) {
          if($userExist['status']=='1')
          {
            Auth::loginUsingId($userExist['id']);
            $Response = array('success' => '1', 'data' => $userExist);
          }
          else
          {
            $Response = array('success' => '0', 'message' => 'Account not activated');
          }
        }
        else
        {
          $Response = array('success' => '0', 'message' => 'Invalid Credentials');
        }

      }
      else
      {
        $Response = array('success' => '0', 'message' => 'User not found');
      }
    }
    else
    {
      $Response = array('success' => '0', 'error' => $validation->messages());
    }
    return $Response;
  }

  public function registerUser()
  {
    $userData = Input::all();
    $validation = Validator::make($userData, User::$registerUser);
    if ($validation->passes()) {
      if(isset($userData['referal_code']))
      {
        if($userData['referal_code']!='')
        {
          $checkReferal = User::where('self_referal_code', $userData['referal_code'])->first();
          if($checkReferal)
          {

          }
          else
          {
            $Response = array('success' => '2', 'error' => 'Invalid Referal Code');
            return $Response;
          }
        }
      }
      $self_referal_code = substr(str_shuffle("SULTHANALLAUDEEN1234567890"), 0, 6);
      $otp =  substr(str_shuffle("1234567890"), 0, 6);
      $phone = $userData['phone'];
      $data = new User;
      $data->user_type = 2;
      $data->email = $userData['email'];
      $data->phone = $phone;
      $data->self_referal_code = $self_referal_code;
      $data->password = Hash::make($userData['password']);
      $data->raw_password = $userData['password'];
      $data->otp = $otp;
      $data->status = 0;
      $data->save();
      $Response = array('success' => '1', 'id' => $data->id,'otp' => $otp);
    }
    else
    {
      $Response = array('success' => '0', 'error' => $validation->messages());
    }
    return $Response;
  }

  public function otpProcess()
  {
    $id = Input::get('id');
    $userData = User::where('id', $id)->first();
    if($userData)
    {
      if($userData['status']=='1')
      {
        $Response = array('success' => '0', 'message' => 'Account already verified');
        return $Response;
      }
    }
    else
    {
      $Response = array('success' => '0', 'message' => 'User not found');
      return $Response;
    }
    $params = array(
      'src' => '917010609203', // Sender's phone number with country code
      'dst' => '91'.$userData['phone'], // Receiver's phone number with country code
      'text' => 'OTP for your ECommerceApp is '.$userData['otp'], // Your SMS text message
      'method' => 'POST' // The method used to call the url
    );
    $p = new RestAPI('MANZA1OTU4ODVLYWFINW', 'ZDgwMGVmZjJjYzg2NTljOGJhZTE3M2E1ZDRlMTQ4');
    $Response = $p->send_message($params);
    #return $Response;
    if($Response['status']==202)
    {
      #Mail
      $Mail = Mail::where('id', '2')->first();
      $mailContent = $Mail->content;
      $mailContent = str_replace("{{email}}", $userData['email'], $mailContent);
      $mailContent = str_replace("{{otp}}", $userData['otp'], $mailContent);
      $from = new SendGrid\Email(null, Config::get('CONST.sendgrid.sender'));
      $subject = $Mail->subject;
      $to = new SendGrid\Email(null, $userData['email']);
      $content = new SendGrid\Content("text/html", $mailContent);
      $mail = new SendGrid\Mail($from, $subject, $to, $content);
      $apiKey = Config::get('CONST.sendgrid.api');
      $sg = new \SendGrid($apiKey);
      $response = $sg->client->mail()->send()->post($mail);
      $Response = array('success' => '1', 'message' => 'Done');
    }
    else
    {
      $Response = array('success' => '0',  'message' => $Response);
    }
    return $Response;
  }

  public function otpUser()
  {
    $otp = Input::get('otp');
    $id = Input::get('id');
    if($otp=='' || $id=='')
    {
      $Response = array('success' => '0', 'error' => 'OTP is required');
    }
    else
    {
      $userExist = User::where('id', $id)->first();
      if($userExist)
      {
        if($userExist['otp']==$otp)
        {
          $updateData['status'] = 1;
          $updateData['otp'] = '';
          User::where('id', $id)->update($updateData);
          $Response = array('success' => '1', 'message' => 'Account verified succesfully');
        }
        else
        {
          $Response = array('success' => '0', 'message' => 'Invalid OTP');
        }
      }
      else
      {
        $Response = array('success' => '0', 'error' => 'Invalid OTP');
      }
    }
    return $Response;
  }


  public function resetPassword()
  {
    $userData = Input::all();
    $validation = Validator::make($userData, User::$resetPassword);
    if ($validation->passes()) {
      $userExist = User::where('email', $userData['email'])->first();
      if($userExist)
      {
        $updateData['raw_password'] =  substr(str_shuffle("023456789SULTHANALLAUDEENZYXWTSRAPNMKJHGFEDCBA"), 0, 6);
        $updateData['password'] = Hash::make($updateData['raw_password']);
        User::where('email', $userData['email'])->update($updateData);
        $Response = array('success' => '1', 'message' => 'Password changed succesfully');
        #Mail
        $Mail = Mail::where('id', '1')->first();
        $mailContent = $Mail->content;
        $mailContent = str_replace("{{email}}", $userData['email'], $mailContent);
        $mailContent = str_replace("{{password}}", $updateData['raw_password'], $mailContent);
        $from = new SendGrid\Email(null, Config::get('CONST.sendgrid.sender'));
        $subject = $Mail->subject;
        $to = new SendGrid\Email(null, $userData['email']);
        $content = new SendGrid\Content("text/html", $mailContent);
        $mail = new SendGrid\Mail($from, $subject, $to, $content);

        $apiKey = Config::get('CONST.sendgrid.api');
        $sg = new \SendGrid($apiKey);
        $response = $sg->client->mail()->send()->post($mail);
        //print_r($response);
      }
      else
      {
        $Response = array('success' => '0', 'message' => 'User not found');
      }
    }
    else
    {
      $Response = array('success' => '0', 'error' => $validation->messages());
    }
    return $Response;
  }

  public function profile(){
    return view('profile');
  }

  public function myOrders(){
    //return Auth::user()->id;
    $data = Order::where('user_id', Auth::user()->id)->get();
    //return $data;
    return view('orders')->with('data', $data);;
  }

  public function viewOrderDetail(){
    $userExist = User::where('id', Input::get('id'))->first();
    if($userExist){
      $orderExist = Order::where('id',Input::get('product_id'))->first();
      if($orderExist){
        $order_detail = OrderDetail::where('order_id',Input::get('product_id'))->get();
        $Response = array('success' => '1', 'order' => $orderExist, 'order_detail' => $order_detail);
      }else{
        $Response = array('success' => '0', 'message' => 'Order not exist');
      }
    }else{
      $Response = array('success' => '0', 'message' => 'User not exist');
    }
    return $Response;
  }


  public function changePassword(){
    $userData = Input::all();
  }

  public function product(){
    return view('product');
  }


  public function upgrade()
  {
    $data = ServiceCategory::get();
    return view('upgrade')->with('datas', $data);
  }

  public function upload()
  {
    $data = ServiceCategory::get();
    return view('upload')->with('data', $data);
  }

  public function post()
  {
    $data = Category::where('status','1')->get();
    $datas = ServiceCategory::get();
    return view('product.post')->with('data', $data)->with('datas', $data);
  }

  public function cart()
  {
    if(Auth::check()){
      $this->syncCart(Auth::user()->id);
      $count = Cart::where('user_id',Auth::user()->id)->count();
      $data = Cart::where('user_id',Auth::user()->id)->get();
    }else{
      $data = array();
      $count = 0;
    }
    return view('cart')->with('data', $data)->with('count', $count);
  }

  public function checkout()
  {
    if(Auth::check()){
      $data = Cart::where('user_id',Auth::user()->id)->get();
    }else{
      $data = array();
    }
    return view('checkout')->with('data', $data);
  }

  public function viewproduct($id)
  {
    $data = Category::where('status','1')->get();
    $product = Product::where('id',$id)->first();
    $subcat = SubCategory::where('category_id',$product['cat'])->get();
    return view('product.view-product')->with('data', $data)->with('product', $product)->with('subcat', $subcat);
  }

  public function viewservice($id)
  {
    $data = Category::where('status','1')->get();
    $product = Service::where('id',$id)->first();
    return view('product.view-service')->with('data', $data)->with('product', $product);
  }

  public function addCart($user_id,$product_id, $count){
    $userExist = User::where('id', $user_id)->first();
    if($userExist)
    {
      $productExist = Product::where('id',$product_id)->first();
      if($productExist){
        $cartExist = Cart::where('user_id',$user_id)->where('product_id',$product_id)->first();
        if($cartExist){
          $updateData['count'] = $count;
          Cart::where('id',$cartExist->id)->update($updateData);
          $Response = array('success' => '1', 'message' => 'Product updated in cart', 'count' => $this->syncCart($user_id));
        }else{
          $data = new Cart;
          $data->user_id = $user_id;
          $data->product_id = $product_id;
          $data->count = $count;
          $data->save();
          $Response = array('success' => '1', 'message' => 'Product added to cart', 'count' => $this->syncCart($user_id));
        }
        return $Response;
      }else{
        $Response = array('success' => '1', 'message' => 'User not Exist');
        return $Response;
      }

    }else{
      $Response = array('success' => '1', 'message' => 'User not Exist');
      return $Response;
    }
  }

  public function deleteCart($user_id,$cart_id){
    $userExist = User::where('id', $user_id)->first();
    if($userExist)
    {
      $cartExist = Cart::where('id',$cart_id)->first();
      if($cartExist){
        Cart::where('id', $cart_id)->delete();
        $count = Cart::where('user_id', $user_id)->count();
        $Response = array('success' => '1', 'message' => 'Product deleted from cart', 'count'=> $count);
        return $Response;
      }else{
        $Response = array('success' => '1', 'message' => 'Product in cart not Exist');
        return $Response;
      }

    }else{
      $Response = array('success' => '1', 'message' => 'User not Exist');
      return $Response;
    }
  }

  public function deleteCartAll($id){
    $userExist = User::where('id', Input::get('id'))->first();
    if($userExist)
    {
      Cart::where('user_id', $id)->delete();
      $Response = array('success' => '1', 'message' => 'Cart cleared');
      return $Response;
    }else{
      $Response = array('success' => '1', 'message' => 'User not Exist');
      return $Response;
    }
  }

  public function syncCart($id){
    $count = Cart::where('user_id', $id)->count();
    $updateData['cart'] = $count;
    User::where('id',$id)->update($updateData);
    return $count;
  }

  public function placeOrder($id,$raz_id){
    $userExist = User::where('id', $id)->first();
    if($userExist){
      $cartExist = Cart::where('user_id', $id)->count();
      if($cartExist==0){
        $Response = array('success' => '0', 'message' => 'No products in cart');
      }else{
        $cart = Cart::where('user_id', $id)->get();
        $data = new Order;
        $data->user_id = $id;
        $data->razor_key = $raz_id;
        $data->status = 1;
        $data->save();
        $order = $data->id;
        $price = 0;
        $tbl = '';
        $count = 0;
        foreach ($cart as $key => $value) {
          $count++;
          $product = Product::where('id', $value->product_id)->first();
          $priceValue = $product->price*$value->count;
          $price = $price +$priceValue;
          $data = new OrderDetail;
          $data->order_id = $order;
          $data->user_id = $id;
          $data->product_id = $value->product_id;
          $data->product_name = $product->name;
          $data->count = $value->count;
          $data->price_individual = $product->price;
          $data->price_total = $priceValue;
          $data->status = 1;
          $data->save();
          $tbl.='<tr>
          <td>'.$count.'</td>
          <td>'.$product->name.'</td>
          <td>Rs.'.$product->price.'</td>
          <td>'.$value->count.'</td>
          <td>Rs.'.$priceValue.'</td>
          </tr>';
        }
        $tbl.= '<tr>
        <td></td>
        <td></td>
        <td></td>
        <td><b>Total : </b></td>
        <td><b>Rs.'.$price.'</b></td>
        </tr>';
        $updateData['price'] = $price;
        Order::where('id',$order)->update($updateData);
        Cart::where('user_id', $id)->delete();
        $this->sendInvoice($data->id,$raz_id,$price,$tbl,$userExist->email);
        $Response = array('success' => '1', 'message' => 'Proceed');

      }
    }else{
      $Response = array('success' => '0', 'message' => 'User not Exist');
    }
    return $Response;
  }

  public function sendInvoice($id,$pay,$price,$tbl,$email){
    $Mail = Mail::where('id', '3')->first();
    $mailContent = $Mail->content;
    $mailContent = str_replace("{{email}}", $email, $mailContent);
    $mailContent = str_replace("{{order_id}}", $id, $mailContent);
    $mailContent = str_replace("{{order_pay}}", $pay, $mailContent);
    $mailContent = str_replace("{{order_total}}", $price, $mailContent);
    $mailContent = str_replace("{{tbl}}", $tbl, $mailContent);
    $from = new SendGrid\Email(null, Config::get('CONST.sendgrid.sender'));
    $subject = $Mail->subject;
    $to = new SendGrid\Email(null, $email);
    $content = new SendGrid\Content("text/html", $mailContent);
    $mail = new SendGrid\Mail($from, $subject, $to, $content);
    $apiKey = Config::get('CONST.sendgrid.api');
    $sg = new \SendGrid($apiKey);
    $response = $sg->client->mail()->send()->post($mail);
    return $tbl;
  }

  public function uploadImg(){
    $all = Input::all();
    $image = Input::file('uploadfile');
    $size = $image->getSize();
    if($size <= '5119960')
    {
      $token = substr(str_shuffle("SULTHANALLAUDEEN0123456789"), 0, 50);
      $publicPath = '/assets/uploads';
      $image->move(public_path($publicPath),$token.$image->getClientOriginalName());
      $Response = array('success' => '1', 'message' => url('/').'/public/'.$publicPath.'/'.$token.$image->getClientOriginalName());
    }
    else
    {
      $Response = array('success' => '0', 'message' => 'Image size should not be greater than 2 MB');
    }
    return $Response;
  }

  public function uploadAddr(){
    $all = Input::all();
    $image = Input::file('uploadfile');
    $size = $image->getSize();
    if($size <= '5119960')
    {
      $token = substr(str_shuffle("SULTHANALLAUDEEN0123456789"), 0, 50);
      $publicPath = '/assets/uploads';
      $image->move(public_path($publicPath),$token.$image->getClientOriginalName());
      $Response = array('success' => '1', 'message' => url('/').'/public/'.$publicPath.'/'.$token.$image->getClientOriginalName());
    }
    else
    {
      $Response = array('success' => '0', 'message' => 'Image size should not be greater than 2 MB');
    }
    return $Response;
  }

  public function uploadCheque(){
    $all = Input::all();
    $image = Input::file('uploadfile');
    $size = $image->getSize();
    if($size <= '5119960')
    {
      $token = substr(str_shuffle("SULTHANALLAUDEEN0123456789"), 0, 50);
      $publicPath = '/assets/uploads';
      $image->move(public_path($publicPath),$token.$image->getClientOriginalName());
      $Response = array('success' => '1', 'message' => url('/').'/public/'.$publicPath.'/'.$token.$image->getClientOriginalName());
    }
    else
    {
      $Response = array('success' => '0', 'message' => 'Image size should not be greater than 2 MB');
    }
    return $Response;
  }

  public function uploadIDProof()
  {
    $all = Input::all();
    $image = Input::file('uploadfile');
    $size = $image->getSize();
    if($size <= '5119960')
    {
      $token = substr(str_shuffle("SULTHANALLAUDEEN0123456789"), 0, 50);
      $publicPath = '/assets/uploads';
      $image->move(public_path($publicPath),$token.$image->getClientOriginalName());
      $Response = array('success' => '1', 'message' => url('/').'/public/'.$publicPath.'/'.$token.$image->getClientOriginalName());
    }
    else
    {
      $Response = array('success' => '0', 'message' => 'Image size should not be greater than 2 MB');
    }
    return $Response;
  }
  public function uploadAadhar()
  {
    $all = Input::all();
    $image = Input::file('uploadfile');
    $size = $image->getSize();
    if($size <= '5119960')
    {
      $token = substr(str_shuffle("SULTHANALLAUDEEN0123456789"), 0, 50);
      $publicPath = '/assets/uploads';
      $image->move(public_path($publicPath),$token.$image->getClientOriginalName());
      $Response = array('success' => '1', 'message' => url('/').'/public/'.$publicPath.'/'.$token.$image->getClientOriginalName());
    }
    else
    {
      $Response = array('success' => '0', 'message' => 'Image size should not be greater than 2 MB');
    }
    return $Response;
  }

  public function uploadPan()
  {
    $all = Input::all();
    $image = Input::file('uploadfile');
    $size = $image->getSize();
    if($size <= '5119960')
    {
      $token = substr(str_shuffle("SULTHANALLAUDEEN0123456789"), 0, 50);
      $publicPath = '/assets/uploads';
      $image->move(public_path($publicPath),$token.$image->getClientOriginalName());
      $Response = array('success' => '1', 'message' => url('/').'/public/'.$publicPath.'/'.$token.$image->getClientOriginalName());
    }
    else
    {
      $Response = array('success' => '0', 'message' => 'Image size should not be greater than 2 MB');
    }
    return $Response;
  }

  public function uploadPoi()
  {
    $all = Input::all();
    $image = Input::file('uploadfile');
    $size = $image->getSize();
    if($size <= '5119960')
    {
      $token = substr(str_shuffle("SULTHANALLAUDEEN0123456789"), 0, 50);
      $publicPath = '/assets/uploads';
      $image->move(public_path($publicPath),$token.$image->getClientOriginalName());
      $Response = array('success' => '1', 'message' => url('/').'/public/'.$publicPath.'/'.$token.$image->getClientOriginalName());
    }
    else
    {
      $Response = array('success' => '0', 'message' => 'Image size should not be greater than 2 MB');
    }
    return $Response;
  }

  public function uploadPPan()
  {
    $all = Input::all();
    $image = Input::file('uploadfile');
    $size = $image->getSize();
    if($size <= '5119960')
    {
      $token = substr(str_shuffle("SULTHANALLAUDEEN0123456789"), 0, 50);
      $publicPath = '/assets/uploads';
      $image->move(public_path($publicPath),$token.$image->getClientOriginalName());
      $Response = array('success' => '1', 'message' => url('/').'/public/'.$publicPath.'/'.$token.$image->getClientOriginalName());
    }
    else
    {
      $Response = array('success' => '0', 'message' => 'Image size should not be greater than 2 MB');
    }
    return $Response;
  }

  public function uploadDig()
  {
    $all = Input::all();
    $image = Input::file('uploadfile');
    $size = $image->getSize();
    if($size <= '5119960')
    {
      $token = substr(str_shuffle("SULTHANALLAUDEEN0123456789"), 0, 50);
      $publicPath = '/assets/uploads';
      $image->move(public_path($publicPath),$token.$image->getClientOriginalName());
      $Response = array('success' => '1', 'message' => url('/').'/public/'.$publicPath.'/'.$token.$image->getClientOriginalName());
    }
    else
    {
      $Response = array('success' => '0', 'message' => 'Image size should not be greater than 2 MB');
    }
    return $Response;
  }

  public function updateIDProof()
  {
    $userExist = User::where('id', Input::get('id'))->first();
    if($userExist)
    {
      $updateData['idproof_image'] = Input::get('img');
      User::where('id',Input::get('id'))->update($updateData);
      $Response = array('success' => '1', 'message' => 'Aadhar Card updated succesfully');
    }
    else
    {
      $Response = array('success' => '0', 'message' => 'Error in uploading image');
    }
    return $Response;
  }

  public function linkAadharNumber()
  {
    $userExist = User::where('id', Input::get('id'))->first();
    if($userExist)
    {
      $updateData['aadhar_number'] = Input::get('aadhar_number');
      User::where('id',Input::get('id'))->update($updateData);
      $Response = array('success' => '1', 'message' => 'Aadhar Card updated succesfully');
    }
    else
    {
      $Response = array('success' => '0', 'message' => 'Error in uploading image');
    }
    return $Response;
  }

  public function uploadAadharImg()
  {
    $userExist = User::where('id', Input::get('id'))->first();
    if($userExist)
    {
      $updateData['aadhar_image'] = Input::get('aadhar_image');
      User::where('id',Input::get('id'))->update($updateData);
      $Response = array('success' => '1', 'message' => 'Aadhar Card updated succesfully');
    }
    else
    {
      $Response = array('success' => '0', 'message' => 'Error in uploading image');
    }
    return $Response;
  }

  public function uploadPoiImg()
  {
    $userExist = User::where('id', Input::get('id'))->first();
    if($userExist)
    {
      $updateData['poi_img'] = Input::get('poi_img');
      User::where('id',Input::get('id'))->update($updateData);
      $Response = array('success' => '1', 'message' => 'Pan Card updated succesfully');
    }
    else
    {
      $Response = array('success' => '0', 'message' => 'Error in uploading image');
    }
    return $Response;
  }

  public function uploadPanImg()
  {
    $userExist = User::where('id', Input::get('id'))->first();
    if($userExist)
    {
      $updateData['pan_image'] = Input::get('pan_image');
      User::where('id',Input::get('id'))->update($updateData);
      Business::where('id',Input::get('id'))->update($updateData);
      $Response = array('success' => '1', 'message' => 'Pan Card updated succesfully');
    }
    else
    {
      $Response = array('success' => '0', 'message' => 'Error in uploading image');
    }
    return $Response;
  }

  public function uploadPPanImg()
  {
    $userExist = User::where('id', Input::get('id'))->first();
    if($userExist)
    {
      $updateData['p_pan_image'] = Input::get('p_pan_image');
      User::where('id',Input::get('id'))->update($updateData);
      Business::where('id',Input::get('id'))->update($updateData);
      $Response = array('success' => '1', 'message' => 'Pan Card updated succesfully');
    }
    else
    {
      $Response = array('success' => '0', 'message' => 'Error in uploading image');
    }
    return $Response;
  }

  public function uploadDigImg()
  {
    $userExist = User::where('id', Input::get('id'))->first();
    if($userExist)
    {
      $updateData['dig_image'] = Input::get('dig_image');
      User::where('id',Input::get('id'))->update($updateData);
      $Response = array('success' => '1', 'message' => 'Dig Card updated succesfully');
    }
    else
    {
      $Response = array('success' => '0', 'message' => 'Error in uploading image');
    }
    return $Response;
  }

  public function uploadAddrImg()
  {
    $userExist = User::where('id', Input::get('id'))->first();
    if($userExist)
    {
      $updateData['address_img'] = Input::get('address_img');
      User::where('id',Input::get('id'))->update($updateData);
      $Response = array('success' => '1', 'message' => 'Address Proof updated succesfully');
    }
    else
    {
      $Response = array('success' => '0', 'message' => 'Error in uploading image');
    }
    return $Response;
  }

  public function uploadChequeImg()
  {
    $userExist = User::where('id', Input::get('id'))->first();
    if($userExist)
    {
      $updateData['cheque_img'] = Input::get('cheque_img');
      User::where('id',Input::get('id'))->update($updateData);
      $Response = array('success' => '1', 'message' => 'Cheque Proof updated succesfully');
    }
    else
    {
      $Response = array('success' => '0', 'message' => 'Error in uploading image');
    }
    return $Response;
  }



  public function updateAadhar()
  {
    $userExist = User::where('id', Input::get('id'))->first();
    if($userExist)
    {
      $updateData['aadhar_image'] = Input::get('img');
      User::where('id',Input::get('id'))->update($updateData);
      $Response = array('success' => '1', 'message' => 'ID Proof updated succesfully');
    }
    else
    {
      $Response = array('success' => '0', 'message' => 'Error in uploading image');
    }
    return $Response;
  }


  public function googlePlace() {

    $request_url = "http://maps.googleapis.com/maps/api/geocode/xml?address=" . Input::get('address') . "&sensor=true";
    $xml = simplexml_load_file($request_url) or die("url not loading");
    $status = $xml->status;
    $Lon = "";
    $Lat = "";
    if ($status == "OK") {
      $Lat = $xml->result->geometry->location->lat;
      $Lon = $xml->result->geometry->location->lng;
      $LatLng = "$Lat,$Lon";
      $Response = array('success' => '1', 'data' => $LatLng);
    }else{
      $Response = array('success' => '0', 'message' => 'Error in Google API');
    }
    return $Response;
  }
  public function userImage()
  {
    $all = Input::all();
    $image = Input::file('uploadfile');
    $size = $image->getSize();
    if($size <= '5119960')
    {
      $token = substr(str_shuffle("SULTHANALLAUDEEN0123456789"), 0, 50);
      $publicPath = '/assets/uploads';
      $image->move(public_path($publicPath),$token.$image->getClientOriginalName());
      $Response = array('success' => '1', 'message' => url('/').'/public/'.$publicPath.'/'.$token.$image->getClientOriginalName());
    }
    else
    {
      $Response = array('success' => '0', 'message' => 'Image size should not be greater than 2 MB');
    }
    return $Response;
  }

  public function productImage()
  {
    $all = Input::all();
    $image = Input::file('uploadfile');
    $size = $image->getSize();
    if($size <= '5119960')
    {
      $token = substr(str_shuffle("SULTHANALLAUDEEN0123456789"), 0, 50);
      $publicPath = '/assets/products';
      $image->move(public_path($publicPath),$token.$image->getClientOriginalName());
      $Response = array('success' => '1', 'message' => url('/').'/public/'.$publicPath.'/'.$token.$image->getClientOriginalName());
    }
    else
    {
      $Response = array('success' => '0', 'message' => 'Image size should not be greater than 2 MB');
    }
    return $Response;
  }

  public function updateImage()
  {
    $userExist = User::where('id', Input::get('id'))->first();
    if($userExist)
    {
      $updateData['image'] = Input::get('img');
      User::where('id',Input::get('id'))->update($updateData);
      $Response = array('success' => '1', 'message' => 'Profile image succesfully');
    }
    else
    {
      $Response = array('success' => '0', 'message' => 'Error in uploading image');
    }
    return $Response;
  }

  public function updateProfile()
  {
    $updateData['first_name'] = Input::get('firstname');
    $updateData['last_name'] = Input::get('lastname');
    $updateData['address'] = Input::get('address');
    $updateData['lat'] = Input::get('lat');
    $updateData['lng'] = Input::get('lng');
    User::where('id',Input::get('id'))->update($updateData);
    $Response = array('success' => '1', 'message' => 'Profile updated succesfully');
    return $Response;
  }

  public function updatePassword()
  {
    $old_password = Input::get('old_password');
    $new_password = Input::get('new_password');
    $userExist = User::where('id', Input::get('id'))->first();
    //return $userExist['password'];
    if (Hash::check($old_password, $userExist['password'])) {
      $updateData['password'] = Hash::make($new_password);
      User::where('id', Input::get('id'))->update($updateData);
      $Response = array('success' => '1', 'message' => 'Password updated succesfully');
    }
    else
    {
      $Response = array('success' => '0', 'message' => 'Old Password is incorrect');
    }
    return $Response;
  }

  public function upgradeBuyer()
  {
    $userExist = User::where('id', Input::get('id'))->first();
    if($userExist)
    {
      $buyerExist = Buyer::where('user_id', Input::get('id'))->first();
      if($buyerExist)
      {
        $Response = array('success' => '1', 'message' => 'Buyer already exists');
      }
      else
      {
        $data = new Buyer;
        $data->user_id = Input::get('id');
        $data->referal_code = Input::get('referal_code');
        $data->status = 1;
        $data->save();
        $Response = array('success' => '1', 'message' => 'Upgrade success');
      }
    }
    else
    {
      $Response = array('success' => '0', 'message' => 'User not found');
    }
    return $Response;
  }
  public function upgradeBroker()
  {
    $userExist = User::where('id', Input::get('id'))->first();
    if($userExist)
    {
      $buyerExist = Broker::where('user_id', Input::get('id'))->first();
      if($buyerExist)
      {
        $Response = array('success' => '1', 'message' => 'Broker already exists');
      }
      else
      {
        $data = new Broker;
        $data->user_id = Input::get('id');
        $data->referal_code = Input::get('referal_code');
        $data->status = 1;
        $data->save();
        $Response = array('success' => '1', 'message' => 'Upgrade success');
      }
    }
    else
    {
      $Response = array('success' => '0', 'message' => 'User not found');
    }
    return $Response;
  }
  public function upgradeLessee()
  {
    $userExist = User::where('id', Input::get('id'))->first();
    if($userExist)
    {
      $buyerExist = Lessee::where('user_id', Input::get('id'))->first();
      if($buyerExist)
      {
        $Response = array('success' => '1', 'message' => 'Lessee already exists');
      }
      else
      {
        $data = new Lessee;
        $data->user_id = Input::get('id');
        $data->referal_code = Input::get('referal_code');
        $data->status = 1;
        $data->save();
        $Response = array('success' => '1', 'message' => 'Upgrade success');
      }
    }
    else
    {
      $Response = array('success' => '0', 'message' => 'User not found');
    }
    return $Response;
  }
  public function upgradeLessor()
  {
    $userExist = User::where('id', Input::get('id'))->first();
    if($userExist)
    {
      // $buyerExist = Lessor::where('user_id', Input::get('id'))->first();
      // if($buyerExist)
      // {
      // $Response = array('success' => '1', 'message' => 'Lessor already exists');
      // }
      // else
      // {
      //     $data = new Lessor;
      //     $data->user_id = Input::get('id');
      //     $data->referal_code = Input::get('referal_code');
      //     $data->status = 1;
      //     $data->save();
      //     $Response = array('success' => '1', 'message' => 'Upgrade success');
      // }
      $Response = array('success' => '1', 'message' => 'Upgrade success');
    }
    else
    {
      $Response = array('success' => '0', 'message' => 'User not found');
    }
    return $Response;
  }
  public function upgradeSeeker()
  {
    $userExist = User::where('id', Input::get('id'))->first();
    if($userExist)
    {
      $buyerExist = Seeker::where('user_id', Input::get('id'))->first();
      if($buyerExist)
      {
        $Response = array('success' => '1', 'message' => 'Seeker already exists');
      }
      else
      {
        $data = new Seeker;
        $data->user_id = Input::get('id');
        $data->referal_code = Input::get('referal_code');
        $data->status = 1;
        $data->save();
        $Response = array('success' => '1', 'message' => 'Upgrade success');
      }
    }
    else
    {
      $Response = array('success' => '0', 'message' => 'User not found');
    }
    return $Response;
  }
  public function upgradeSeller()
  {
    $userExist = User::where('id', Input::get('id'))->first();
    if($userExist)
    {
      $buyerExist = Seller::where('user_id', Input::get('id'))->first();
      if($buyerExist)
      {
        $Response = array('success' => '1', 'message' => 'Seller already exists');
      }
      else
      {
        $data = new Seller;
        $data->user_id = Input::get('id');
        $data->referal_code = Input::get('referal_code');
        $data->status = 1;
        $data->save();
        $Response = array('success' => '1', 'message' => 'Upgrade success');
      }
    }
    else
    {
      $Response = array('success' => '0', 'message' => 'User not found');
    }
    return $Response;
  }

  public function updateBusinessDetails()
  {
    $userExist = User::where('id', Input::get('id'))->first();
    if($userExist)
    {
      $businessExist = Business::where('user_id', Input::get('id'))->first();
      if($businessExist)
      {
        $Response = array('success' => '0', 'message' => 'Already Updated');
      }
      else
      {
        $data = new Business;
        $data->user_id = Input::get('id');
        $data->name = Input::get('name');
        $data->gstin = Input::get('gstin');
        $data->address = Input::get('address');
        $data->lat = Input::get('lat');
        $data->lng = Input::get('lng');
        $data->pan_no = Input::get('pan_no');
        $data->tan_no = Input::get('tan_no');
        $data->status = 1;
        $data->save();
        $updateData['role_seller'] = '1';
        $updateData['business_detail_id'] = $data->id;
        User::where('id', Input::get('id'))->update($updateData);
        $Response = array('success' => '1', 'message' => 'Business Details Updated');
      }
    }
    else
    {
      $Response = array('success' => '0', 'message' => 'User not found');
    }
    return $Response;
  }

  public function updateBankDetails()
  {
    $userExist = User::where('id', Input::get('id'))->first();
    if($userExist)
    {
      $bankExist = Bank::where('user_id', Input::get('id'))->first();
      if($bankExist)
      {
        $Response = array('success' => '0', 'message' => 'Already Updated');
      }
      else
      {
        $data = new Bank;
        $data->user_id = Input::get('id');
        $data->name = Input::get('name');
        $data->number = Input::get('number');
        $data->bank_name = Input::get('bank_name');
        $data->ifsc = Input::get('ifsc');
        $data->address = Input::get('address');
        $data->p_pan = Input::get('p_pan');
        $data->type = Input::get('type');
        $data->status = 1;
        $data->save();
        $updateData['bank_detail_id'] = $data->id;
        $updateData['p_pan_number'] = Input::get('p_pan');
        User::where('id', Input::get('id'))->update($updateData);
        $Response = array('success' => '1', 'message' => 'Bankx Details Updated');
      }
    }
    else
    {
      $Response = array('success' => '0', 'message' => 'User not found');
    }
    return $Response;
  }

  public function postWorkman()
  {
    $data = Workman::where('user_id', Auth::user()->id)->get();
    return view('product.post-workman-equipment')->with('data', $data);;
  }
  public function updateWorkMan()
  {
    $id = Input::get('id');
    $updateData['name'] = Input::get('name');
    $updateData['license'] = Input::get('license');
    $updateData['aadhar'] = Input::get('aadhar');
    Workman::where('id', $id)->update($updateData);
    $Response = array('success' => '1', 'message' => 'Workman detail updated successfully');
    return $Response;
  }

  public function deleteWorkMan($id)
  {
    Workman::where('id', $id)->delete();
    return redirect('post-workman-equipment');
  }


  public function myProduct()
  {
    $id= Auth::user()->id;
    $product = Product::where('user_id',$id)->get();
    $service = Service::where('user_id',$id)->get();
    return view('product.posted')->with('product', $product)->with('service', $service);
  }

  public function myProductData($id)
  {
    $product = Product::get();
    $service = Service::get();
    return view('product.posted')->with('product', $product)->with('service', $service);
  }

  public function deleteproduct($id)
  {
    $updateData['status'] = '1';
    Product::where('id', $id)->update($updateData);

    Product::where('id', $id)->delete();
    return redirect('my-product');
  }

  public function deleteservice()
  {
    Service::where('id', $id)->delete();
    return redirect('my-product');
  }

  public function getSubCategory(){
    $data = SubCategory::where('category_id', Input::get('id'))->get();
    $Response = array('success' => '1', 'sub' => $data);
    return $Response;
  }

  public function addWorkMan(){
    $datass = new Workman;
    $datass->user_id = Input::get('id');
    $datass->name = Input::get('name');
    $datass->aadhar = Input::get('aadhar');
    $datass->license = Input::get('license');
    $datass->save();
    $Response = array('success' => '1', 'message' => 'Workman added succesfully');
    return $Response;
  }

  function roundUpToAny($n,$x=5) {
      return (ceil($n)%$x === 0) ? ceil($n) : round(($n+$x/2)/$x)*$x;
  }

  public function getDistanceByUser($id,$lat,$lng){
    $product = Cart::where('user_id',$id)->first();
    $productId = $product->product_id;
    $user_id = Product::where('id',$productId)->first();
    $userId = $user_id->user_id;
    $businessData = Business::where('user_id',$userId)->first();
    $distance = $this->getDistance($lat,$lng,$businessData->lat,$businessData->lng);
    return $distance;
  }

  public function getDistance($fromlat,$fromlng, $tolat, $tolng){
    $origin = $fromlat.','.$fromlng;
    $destination = $tolat.','.$tolng;
    $details = 'http://maps.googleapis.com/maps/api/distancematrix/json?origins='.$origin.'&destinations='.$destination.'&mode=driving&sensor=false';
    $json = file_get_contents($details);
    $details = json_decode($json, TRUE);
    $dist = $details['rows'][0]['elements'][0]['distance']['text'];
    $dist = explode(" ",$dist);
    return $dist[0];
  }

  public function calculateLogistic($id,$lat,$lng){
    $data = Cart::where('user_id',$id)->get();
    $length = 0;
    $breadth = 0;
    $height = 0;
    $actual_weight = 0;
    $distance = $this->getDistanceByUser($id,$lat,$lng);
    $product_value = 0;
    for ($i=0; $i <count($data); $i++) {
      $product = Product::where('id',$data[$i]->product_id)->first();
      $length = $product->length*$data[$i]->count;
      $breadth = $product->breadth*$data[$i]->count;
      $height = $product->height*$data[$i]->count;
      $actual_weight = $product->weight*$data[$i]->count;
      $distance = 5;
      $product_value = $product->price*$data[$i]->count;
    }

    $volumetric_weight = (($length*$breadth*$height)/1728)*8;
    $volumetric_weight = round($volumetric_weight);
    $volumetric_weight =  $this->roundUpToAny($volumetric_weight);
    //Rounding Weight
    $roundedWeight = $this->roundUpToAny($actual_weight);
    //Comparing Rounded Weight and Volumetric Weight
    $finalWeight = 0;
    if($volumetric_weight>$roundedWeight){
        $finalWeight = $volumetric_weight;
    }else if ($volumetric_weight<$roundedWeight){
        $finalWeight = $roundedWeight;
    }
    else {
        //Both are equal
        $finalWeight = $volumetric_weight;
    }

    //Handling charge
    $handlingCharge = ($finalWeight/100)*20;

    if($handlingCharge<=20){
        $handlingCharge = 20;
    }

    //UFC
    //If distance lesss than 200
    if($distance<=200){
        $fc = 7;
    }elseif($distance>200 && $distance<=1025){
        $fc = 12;
    }elseif($distance>1025 && $distance<=2070){
        $fc = 17;
    }else{
        $fc = 21;
    }
    //Discount on FC
    if($finalWeight<=1000){
        $discount = 0;
    }elseif ($finalWeight >1001 && $finalWeight <=2000){
        $discount = 5;
    }elseif($finalWeight>2001 && $finalWeight<=3000){
        $discount = 7;
    }elseif($finalWeight>3001 && $finalWeight<=5000){
        $discount = 10;
    }elseif($finalWeight>5001 && $finalWeight<=10000){
        $discount = 15;
    }else{
        $discount = 20;
    }
    //Appoying Discount
    $toDiscountValue = $fc*$discount/100;
    $afterDiscount = $fc - $toDiscountValue;
    //Calculate amount based on final weight and final discount
    $amount = $afterDiscount*$finalWeight;
    //Toll Charge
    $tollMin = 20;
    $tollMax = 500;
    $tollAmt = $amount*3/100;
    //Seeing min and max value
    if($tollAmt<=$tollMin){
        $finalToll = $tollMin;
    }elseif($tollAmt>=$tollMax){
        $finalToll = $tollMax;
    }else{
        $finalToll = $tollAmt;
    }
    //Carrier Risk
    $cr = $product_value*0.5/100;
    // //FOV
    // $fov = $product_value*0.125/100;
    //Green tax
    $gtMin = 40;
    $gtCalc = ($finalWeight/100)*16;
    if($gtCalc<=$gtMin){
        $gt = $gtMin;
    }else{
        $gt = $gtCalc;
    }
    //echo $gt;exit;
    //Applying sub charges
    $subCharge = 0;
    //Adding Toll
    $subCharge = $subCharge+$finalToll;
    $docketCharge = 120;
    $subCharge = $subCharge+$docketCharge;
    //$toPayCharge = 150;
    // $subCharge = $subCharge+$toPayCharge;
    $subCharge = $subCharge+$cr;
    //$subCharge = $subCharge+$fov;
    $subCharge = $subCharge+$gt;
    //Adding handling charge
    $subCharge = $subCharge+$handlingCharge;
    //Other Charge
    $subCharge = $subCharge+5;
    $beforeGST = $amount + $subCharge;
    //Final value to GST
    $gst = 12;
    //$gstValue = $beforeGST*$gst/100;
    $gstValue = 0;
    $finalAmount =  $beforeGST+$gstValue;
    $finalAmount = round($finalAmount,2);
    //Handling charge
    return $finalAmount;
  }

  public function migrate()
  {
    return 1;
    $all = Migrate::get();
    foreach ($all as $key) {
      $cat = Category::where('name', $key->cat)->first();
      if($cat){
        //$sub = SubCategory::where('name', $cat->id)->first();
        $datass = new SubCategory;
        $datass->category_id = $cat->id;
        $datass->name = $key['sub'];
        $datass->save();
      }
      else{
        $data = new Category;
        $data->name = $key->cat;
        $data->status = 1;
        $id = $data->save();
        $datas = new SubCategory;
        $datas->category_id = $id;
        $datas->name = $key['sub'];
        $datas->save();
      }
    }
    return 'done';
  }

  public function migrate1()
  {
    return 1;
    $all = MigrateOne::get();
    foreach ($all as $key) {
      $cat = ServiceCategory::where('name', $key->cat)->first();
      if($cat){
        $datass = new ServiceSubCategory;
        $datass->category_id = $cat->id;
        $datass->name = $key['sub'];
        $datass->save();
      }
      else{
        $data = new ServiceCategory;
        $data->name = $key->cat;
        $data->status = 1;
        $id = $data->save();
        $datas = new ServiceSubCategory;
        $datas->category_id = $id;
        $datas->name = $key['sub'];
        $datas->save();
      }
    }
    return 'done';
  }

  public function logout()
  {
    Session::flush();
    return Redirect::to('/');
  }
}
