<?php



/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
#App Specific Call
// Route::get('checkConnection', 'Controller@checkConnection');
// Route::get('getToken', 'Controller@getToken');
#Web Calls
Route::get('/', 'AppController@test');
Route::get('/gen', 'AppController@index');
// Route::get('/', 'PreController@index');
// Route::post('getOTP_P', 'PreController@OTP');
// Route::post('preRegister', 'PreController@preRegister');
// Route::get('referal/{code}', 'PreController@referal');
// Route::get('sendMail/{id}', 'PreController@sendMail');
// Route::get('verify/{code}', 'PreController@verify');

Route::get('about', 'AppController@about');
Route::get('product', 'AppController@products');
Route::get('product/{id}', 'AppController@productDetail');
Route::get('contact', 'AppController@contact');
Route::post('contactUs', 'AppController@contactUs');
Route::get('login', 'AppController@login');
Route::post('loginUsingFB', 'AppController@loginUsingFB');
Route::post('loginUser', 'AppController@loginUser');
Route::post('registerUser', 'AppController@registerUser');
Route::post('resetPassword', 'AppController@resetPassword');
Route::post('otpProcess', 'AppController@otpProcess');
Route::post('otpVerify', 'AppController@otpUser');
Route::post('otpUser', 'AppController@otpUser');
Route::get('profile', 'AppController@profile');
Route::get('my-orders', 'AppController@myOrders');
Route::post('viewOrderDetail', 'AppController@viewOrderDetail');
Route::get('product', 'AppController@product');
Route::get('upgrade', 'AppController@upgrade');
Route::get('upload', 'AppController@upload');
Route::get('post', 'AppController@post');
Route::get('cart', 'AppController@cart');
Route::get('checkout', 'AppController@checkout');
Route::get('viewproduct/{id}', 'AppController@viewproduct');
Route::get('viewservice/{id}', 'AppController@viewservice');
#Cart Section
Route::get('addCart/{user_id}/{product_id}/{count}', 'AppController@addCart');
Route::get('deleteCart/{user_id}/{product_id}', 'AppController@deleteCart');
Route::get('deleteCartAll/{user_id}', 'AppController@deleteCartAll');
Route::get('syncCart/{user_id}', 'AppController@syncCart');
Route::post('getLogistic', 'AppController@getLogistic');
Route::get('calculateLogistic/{user_id}/{lat}/{lng}', 'AppController@calculateLogistic');
Route::get('getDistance/{fromlat}/{fromlng}/{tolat}/{tolng}', 'AppController@getDistance');
#Order
Route::get('placeOrder/{user_id}/{id}', 'AppController@placeOrder');
#
Route::post('uploadIDProof', 'AppController@uploadIDProof');
Route::post('updateIDProof', 'AppController@updateIDProof');
Route::post('uploadAadhar', 'AppController@uploadAadhar');
Route::post('updateAadhar', 'AppController@updateAadhar');
Route::post('uploadPan', 'AppController@uploadPan');
Route::post('uploadPPan', 'AppController@uploadPPan');
Route::post('uploadDig', 'AppController@uploadDig');
Route::post('uploadImg', 'AppController@uploadImg');
Route::post('uploadAddr', 'AppController@uploadAddr');
Route::post('uploadCheque', 'AppController@uploadCheque');
Route::post('uploadPoi', 'AppController@uploadPoi');
#
Route::post('linkAadharNumber', 'AppController@linkAadharNumber');
Route::post('uploadAadharImg', 'AppController@uploadAadharImg');
Route::post('uploadPanImg', 'AppController@uploadPanImg');
Route::post('uploadPPanImg', 'AppController@uploadPPanImg');
Route::post('uploadDigImg', 'AppController@uploadDigImg');
Route::post('uploadAddrImg', 'AppController@uploadAddrImg');
Route::post('uploadChequeImg', 'AppController@uploadChequeImg');
Route::post('uploadPoiImg', 'AppController@uploadPoiImg');
#
Route::post('userImage', 'AppController@userImage');
Route::post('updateImage', 'AppController@updateImage');

Route::post('productImage', 'AppController@productImage');

Route::post('googleplace', 'AppController@googleplace');
Route::post('updateProfile', 'AppController@updateProfile');
Route::post('updatePassword', 'AppController@updatePassword');
#Upgrade
Route::post('upgradeBuyer', 'AppController@upgradeBuyer');
Route::post('upgradeBroker', 'AppController@upgradeBroker');
Route::post('upgradeLessee', 'AppController@upgradeLessee');
Route::post('upgradeLessor', 'AppController@upgradeLessor');
Route::post('upgradeSeeker', 'AppController@upgradeSeeker');
Route::post('upgradeSeller', 'AppController@upgradeSeller');
#Product
Route::post('postProduct', 'AppController@postProduct');
Route::post('updateProduct', 'AppController@updateProduct');
Route::post('postServiceDetail', 'AppController@postServiceDetail');
Route::post('postService', 'AppController@postService');
Route::post('updateService', 'AppController@updateService');
Route::get('my-product', 'AppController@myProduct');
Route::get('my-product/{id}', 'AppController@myProductData');
Route::get('post-workman-equipment', 'AppController@postWorkman');
Route::get('deleteproduct/{id}', 'AppController@deleteproduct');
Route::get('deleteservice/{id}', 'AppController@deleteservice');
#Workman
Route::post('addWorkMan', 'AppController@addWorkMan');
Route::post('updateWorkMan', 'AppController@updateWorkMan');
Route::get('deleteWorkMan/{id}', 'AppController@deleteWorkMan');
#Update Bank Details
Route::post('updateBusinessDetails', 'AppController@updateBusinessDetails');
Route::post('updateBankDetails', 'AppController@updateBankDetails');
#Get sub category
Route::post('getSubCategory', 'AppController@getSubCategory');
#Admin Panel
Route::get('master', 'AdminController@index');
Route::post('authAdminLogin', 'AdminController@authAdminLogin');
Route::get('contacts', 'AdminController@contacts');
Route::get('dashboard', 'AdminController@dashboard');
Route::get('users-all', 'AdminController@usersAll');
Route::get('users/delete/{id}', 'AdminController@usersDelete');
Route::get('users-activated', 'AdminController@usersActivated');
Route::get('users-not-activated', 'AdminController@usersNotActivated');
Route::get('users-blocked', 'AdminController@usersBlocked');
Route::get('products-all', 'AdminController@productsAll');
Route::get('products-approve', 'AdminController@productsApprove');
Route::get('products/{id}', 'AdminController@productGet');
Route::get('category-create', 'AdminController@categoryCreate');
Route::post('createCategory', 'AdminController@categoryCreateData');
Route::get('category-edit/{id}', 'AdminController@categoryEdit');
Route::post('category-edit/editCategory', 'AdminController@editCategoryData');
Route::get('category-status/{id}', 'AdminController@categoryStatus');
Route::get('product-status/{id}', 'AdminController@productStatus');
Route::get('category-all', 'AdminController@categoryAll');
Route::get('approval-product', 'AdminController@approval_product');
Route::get('approval-product/{id}', 'AdminController@approval_product_view');
Route::get('approval-service', 'AdminController@approval_service');
Route::get('approval-service-document', 'AdminController@service_document');
Route::get('action/{action}/{item}/{id}', 'AdminController@approve');

Route::get('logoutAdmin', 'AdminController@logout');

Route::get('logout', 'AppController@logout');
// ##Testing Page
// Route::get('migrate', 'AppController@migrate');
// Route::get('migrate1', 'AppController@migrate1');

##End of Testing Page
#User Controller
// Route::post('registerUser', 'Controller@registerUser');
