<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Migrate extends Model
{
    protected $table = 'migrate';
}
