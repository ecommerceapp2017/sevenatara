<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    protected $table = 'sub_category';

    public static $add = array(
        'name' => 'required',
        'description' => 'required',
        'tax_type' => 'required',
        'tax_value' => 'required'
		);
}
