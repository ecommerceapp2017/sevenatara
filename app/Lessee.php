<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lessee extends Model
{
    protected $table = 'lessee';
}
