<?php
return [
    'color' => [
        '1' => 'WHITE',
        '2' => 'SILVER',
        '3' => 'GRAY',
        '4' => 'BLACK',
        '5' => 'RED',
        '6' => 'MAROON',
        '7' => 'YELLOW',
        '8'=> 'OLIVE',
        '9' => 'LIME',
        '10' => 'GREEN',
        '11' => 'AQUA',
        '12' => 'TEAL',
        '13' => 'BLUE',
        '14' => 'NAVY',
        '15' => 'FUCHSIA',
        '16' => 'PURPLE'
    ]   
];